//
//  SafeMate-Bridging-Header.h
//

#ifndef SafeMate_Bridging_Header_h
#define SafeMate_Bridging_Header_h

#import <GoogleSignIn/GoogleSignIn.h>
#import <GoogleMaps/GoogleMaps.h>
#import <LinkedinSwift/LSHeader.h>

#import "Alert.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "PayPalMobile.h"
#import "FMDB.h"

#define APP_NAME "Safe Mate"
#define DATABASE_NAME "Safemate.sqlite"

// For APIs
#define BASE_URL "http://www.auxanoglobalservices.com/safemate/api.php?request="
#define USER_PIC_URL "http://www.auxanoglobalservices.com/safemate/"

#define REGISTER_USER "registerUser"
#define GET_ROLES "getRoles"
#define GET_COMPANY "getCompany"
#define LOGIN_USER "login"
#define FORGOT_PASSWORD "forgotPassword"
#define LOGIN_WITH_SOCIAL "socialLogin"
#define CREATE_NEW_SITE "createNewSite"
#define ADD_QUESTION "addQuestion"
#define REMOVE_QUESTION "removeQuestion"
#define GET_SITE_LIST "getSiteList"
#define CHECK_INTO_SITE "checkIntoSite"
#define GET_PENDING_USERS "getPendingUserList"
#define REMOVE_USERS "removeUsers"
#define APPROVE_USERS "approveUser"
#define GET_SITE_INFO "getSiteInfo"
#define GET_MYTEAM_LIST "getMyTeams"
#define GET_SITE_PEOPLE "getSitePeople"

#define Validate_Access_Code "validateAccessCode"
#define Validate_Site_Key "validateSiteKey"

#define DEVICE_TYPE "2" // 1 -> Android, 2 -> iOS

#define MANUAL_REGISTER "1"
#define FACEBOOK_REGISTER "2"
#define GOOGLE_REGISTER "3"
#define LINKEDIN_REGISTER "4"

#define FACEBOOK_REGISTER_PASSWORD "facebook@123"
#define GOOGLE_REGISTER_PASSWORD "google@123"
#define LINKEDIN_REGISTER_PASSWORD "linkedin@123"

// For Google+ SignIn
#define GOOGLE_CLIENT_ID "1007585665931-i149dlfod3bij598l52hlphgutsecstk.apps.googleusercontent.com"

// For Google Map
#define GOOGLE_MAP_API_KEY "AIzaSyARwNJByCMZoFBLTNO3MQOlsNDL805xBPE"
#define GOOGLE_AUTO_FILTER_API_KEY "AIzaSyA2Mhu4_WNYDZq5HZMtF86kII_hmEDnefA"
#define GOOGLE_AUTO_FILTER_BASE_URL "https://maps.googleapis.com/maps/api/place/autocomplete/json"
#define SEARCH_LOCATION_ERROR "Location Could Not Be Found"

// For LinkedIn
#define clientId "81uk4edsfa64fe"
#define clientSecret "I2VJwSY6UDSHxUm5"
#define state "DLKDJF46ikMMZADfdfds"
#define redirectUrl "http://www.auxanoglobalservices.com/"

// For PayPal
#define CLIENT_ID_FOR_PRODUCTION ""
#define CLIENT_ID_FOR_SANDBOX ""
#define Payment_Success "PayPal Payment Success"
#define Payment_Cancelled "PayPal Payment Cancelled"

// For Checkbox Validation
#define Empty_Checkbox "Please Select Only One Checkbox"

// For Validation Message
#define Valid_Email "Please Enter A Valid Email Id"
#define Valid_Password "Please Enter A Alphanumeric Password"

// For Alreday Exists
#define Email_Alreday_Exists "Entered Email Alreday Added"

// For Allow Permission
#define Email_Permission "Select Email Permission First"

// For Empty Field Message
#define Empty_First_Name "Please Enter Your First Name"
#define Empty_Last_Name "Please Enter Your Last Name"
#define Empty_Email "Please Enter Your Email Id"
#define Empty_Password "Please Enter Your Password"
#define Empty_Contact_Number "Please Enter Your Contact Number"
#define Empty_Company_Name "Please Enter Your Company Name"
#define Empty_Role "Please Enter Your Role"
#define Empty_Site_Name "Please Enter Site"
#define Empty_Address "Please Enter Address"
#define Empty_Tracking_Rang "Please Enter Geofence Range"
#define Empty_Supervisor "Please Enter Supervisor"
#define Empty_Organization "Please Enter Organization"
#define Empty_Question "Please Enter Question"
#define Empty_Copy_Site "Please Select Site"
#define Empty_Time "Please Select Time"
#define Empty_Query_Type "Please Select Query"
#define Empty_Options "Please Fill All Option"
#define Empty_Group "Please Select Group"
#define Empty_Reoprt_Role "Please Select Role"
#define Empty_People "Please Select People"

#define Mini_Tracking_Rang "Range Should Be Between 50 to 2000meters"

// For  Only Characters Field Message

#define Only_Characters_First_Name "Please Enter Only Characters In First Name"
#define Only_Characters_Last_Name "Please Enter Only Characters In Last Name"

// For Max & Mini Character Field Message
#define Mini_Characters_First_Name "Please Enter Atleast 3 Characters In First Name"
#define Mini_Characters_Last_Name "Please Enter Atleast 3 Characters In Last Name"
#define Mini_Characters_Password "Please Enter Atleast 6 Characters In Password"
#define Mini_Characters_Contact "Please Enter Atleast 10 Characters In Contact"

// For  Check Into Site Field Message
#define Empty_Site_AccessCode "Please Enter Site Access Code"
#define Empty_Site_Key "Please Enter Site Key"
#define Empty_Name "Please Enter Name"
#define Only_Characters_Name "Please Enter Only Characters In Name"
#define Empty_Organization "Please Enter Organization"
#define Empty_Phone_Number "Please Enter Phone Number"
#define Empty_CheckBox "Please Select Read & Accept Information Checkbox"

#define Select_Category_Upgrade_Site "Please Select Only One Category To Upgrade Site"

#define Network_Error "The Internet Connection Appears To Be Offline"
#define Send_Email_Error "Your device could not send e-mail. Please check e-mail configuration and try again."
#define Send_SMS_Error "Your device could not send message. Please check message configuration and try again."

#define No_Camera "Sorry, this device has no camera."

#define Delete_Query "Are You Sure You Want To Remove This Query?"

//For My Info
#define Select_Category "Please Select Only One Category To Cancel Account"

#endif /* SafeMate_Bridging_Header_h */
