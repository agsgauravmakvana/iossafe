//
//  SignIn.swift
//

import UIKit
import FBSDKLoginKit
import LinkedinSwift

class SignIn: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITextFieldDelegate {
    
    // MARK: - Variables
    var dictFBData: [String : AnyObject]!
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: clientId, clientSecret: clientSecret, state: state, permissions: ["r_basicprofile","r_emailaddress"], redirectUrl: redirectUrl))
    
    // MARK: - UI Controls
    @IBOutlet var btnLogIn: UIButton!
    @IBOutlet var btnNewMember: UIButton!
    @IBOutlet var btnForgotPass: UIButton!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnGoogle: UIButton!
    @IBOutlet var btnLinkedIn: UIButton!
    
    @IBOutlet var txtEmail: ACFloatingTextfield!
    @IBOutlet var txtPassword: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For google signin
        GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENT_ID
        GIDSignIn.sharedInstance().scopes = ["profile"]
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self

        btnLogIn.layer.cornerRadius = DELEGATE.setCorner()
        btnLogIn.layer.borderColor = UIColor.white.cgColor
        btnLogIn.layer.borderWidth = 1.0

        if UserDefaults.standard.bool(forKey: "isLogin") {
            let dictUserDetails = (UserDefaults.standard.value(forKey: "UserDetails")) as! NSDictionary
            
            let isNewUser = dictUserDetails["isNewUser"] as! String
            if (isNewUser == "1") {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
                self.navigationController?.pushViewController(objVC, animated: false)
            }else {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: DeshboardVC = storyboard.instantiateViewController(withIdentifier: "DeshboardVC") as! DeshboardVC
                self.navigationController?.pushViewController(objVC, animated: false)
            }
        }
        
        if DELEGATE.checkInternetConnection() {
            
        }
    }
    
    // MARK: - UI Methods
    func signIn(email: String, password: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()

        let url = String(format: "%@%@", BASE_URL, LOGIN_USER)
        let parameters: [AnyHashable: Any] = [
            "email": email,
            "password": password,
            "deviceType": DEVICE_TYPE,
            "deviceToken": UserDefaults.standard.value(forKey: "DeviceToken")!
        ]

        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSignInData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageSignInData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            DELEGATE.dictUserDetails = dictData["userDetail"] as! NSDictionary
//            print("DELEGATE.dictUserDetails: \(DELEGATE.dictUserDetails)")
            
            UserDefaults.standard.setValue(DELEGATE.dictUserDetails, forKey: "UserDetails")
//            print("UserDetails: \(String(describing: UserDefaults.standard.value(forKey: "UserDetails")))")
            
            DELEGATE.strUserID = DELEGATE.dictUserDetails["userID"] as! String
//            print("DELEGATE.strUserID: \(DELEGATE.strUserID)")
            
            UserDefaults.standard.setValue(DELEGATE.strUserID, forKey: "UserID")
//            print("UserID: \(String(describing: UserDefaults.standard.value(forKey: "UserID")))")
            
            UserDefaults.standard.set(true, forKey: "isLogin")
            
            let isNewUser = DELEGATE.dictUserDetails["isNewUser"] as! String
            if (isNewUser == "1") {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
                self.navigationController?.pushViewController(objVC, animated: true)
            }else {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: DeshboardVC = storyboard.instantiateViewController(withIdentifier: "DeshboardVC") as! DeshboardVC
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func signInWithSocial(parameters: [AnyHashable: Any]) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, LOGIN_WITH_SOCIAL)
//        print("Parameters: \(parameters)")
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSignInWithSocialData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageSignInWithSocialData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            DELEGATE.dictUserDetails = dictData["userDetail"] as! NSDictionary
//            print("DELEGATE.dictUserDetails: \(DELEGATE.dictUserDetails)")
            
            UserDefaults.standard.setValue(DELEGATE.dictUserDetails, forKey: "UserDetails")
//            print("UserDetails: \(String(describing: UserDefaults.standard.value(forKey: "UserDetails")))")
            
            DELEGATE.strUserID = DELEGATE.dictUserDetails["userID"] as! String
//            print("DELEGATE.strUserID: \(DELEGATE.strUserID)")
            
            UserDefaults.standard.setValue(DELEGATE.strUserID, forKey: "UserID")
//            print("UserID: \(String(describing: UserDefaults.standard.value(forKey: "UserID")))")
            
            UserDefaults.standard.set(true, forKey: "isLogin")
            
            let isNewUser = DELEGATE.dictUserDetails["isNewUser"] as! String
            if (isNewUser == "1") {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
                self.navigationController?.pushViewController(objVC, animated: true)
            }else {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: DeshboardVC = storyboard.instantiateViewController(withIdentifier: "DeshboardVC") as! DeshboardVC
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func getFBUserData() {
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    self.dictFBData = result as! [String : AnyObject]
                    UserDefaults.standard.set(self.dictFBData, forKey: "UserData")
                    print("dictFBData : \(self.dictFBData)")
                    
                    let accessToken = FBSDKAccessToken.current()
                    if(accessToken != nil) {
                        print("Access Token: \(String(describing: accessToken?.tokenString))")
                        UserDefaults.standard.set(accessToken?.tokenString, forKey: "AccessToken")
                        UserDefaults.standard.synchronize()
                        
                        let parameters: [AnyHashable: Any] = [
                            "firstName": self.dictFBData["first_name"] as! String,
                            "lastName": self.dictFBData["last_name"] as! String,
                            "email": self.dictFBData["email"] as! String,
                            "contactNumber": "",
                            "companyID": "",
                            "companyName": "",
                            "roleID": "",
                            "deviceType": DEVICE_TYPE,
                            "deviceToken": UserDefaults.standard.value(forKey: "DeviceToken")!,
                            "registerType": FACEBOOK_REGISTER
                        ]

                        self.signInWithSocial(parameters: parameters)
                    }
                }else {
                    DELEGATE.showPopup(title: "\(String(describing: error?.localizedDescription))", type: .error)
                }
            })
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickLogIn(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }
        
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: .whitespaces)
        if txtPassword.text == "" {
            DELEGATE.showPopup(title: Empty_Password, type: .error)
            return
        }else {
            if DELEGATE.isValidPassword(strPassword: txtPassword.text!) == false {
                DELEGATE.showPopup(title: Valid_Password, type: .error)
                return
            }
        }
        
        self.signIn(email: txtEmail.text!, password: txtPassword.text!)
    }
    
    @IBAction func onClickNewMember(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SignUp = storyboard.instantiateViewController(withIdentifier: "SignUp") as! SignUp
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickForgotPass(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: ForgotPassword = storyboard.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickFacebook(sender: UIButton) {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email","user_friends"], from: self) { (result, error) in
            if (error == nil) {
                let fbloginresult: FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if (fbloginresult.grantedPermissions.contains("email")) {
                        self.getFBUserData()
                    }
                }
            }else {
                DELEGATE.showPopup(title: "\(String(describing: error?.localizedDescription))", type: .error)
            }
        }
    }
    
    @IBAction func onClickGoogle(sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func onClickLinkedIn(sender: UIButton) {
        self.linkedinHelper.authorizeSuccess({ (lsToken) -> Void in
            print("Token: \(lsToken.accessToken)")
            
            self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                
                let dictData = response.jsonObject!
                print("dictData : \(dictData)")
                
                let parameters: [AnyHashable: Any] = [
                    "firstName": dictData["firstName"] as! String,
                    "lastName": dictData["lastName"] as! String,
                    "email": dictData["emailAddress"] as! String,
                    "contactNumber": "",
                    "companyID": "",
                    "companyName": "",
                    "roleID": "",
                    "deviceType": DEVICE_TYPE,
                    "deviceToken": UserDefaults.standard.value(forKey: "DeviceToken")!,
                    "registerType": LINKEDIN_REGISTER
                ]
                
                self.signInWithSocial(parameters: parameters)
            }) { [unowned self] (error) -> Void in
                DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
            }
        }, error: { (error) -> Void in
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        }, cancel: { () -> Void in
            print("Cancelled by user")
        })
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPassword {
            let currentString: NSString = txtPassword.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= Constant.maxLength
        }

        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.txtEmail) {
            self.txtPassword.becomeFirstResponder()
        }else {
            self.txtPassword.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }

    // MARK: GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user:GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID
            print("userId : \(String(describing: userId))")
            let idToken = user.authentication.idToken
            print("idToken : \(String(describing: idToken))")
            let fullName = user.profile.name
            print("fullName : \(String(describing: fullName))")
            let givenName = user.profile.givenName
            print("givenName : \(String(describing: givenName))")
            let familyName = user.profile.familyName
            print("familyName : \(String(describing: familyName))")
            let email = user.profile.email
            print("email : \(String(describing: email))")
            
            let parameters: [AnyHashable: Any] = [
                "firstName": user.profile.givenName,
                "lastName": user.profile.familyName,
                "email": user.profile.email,
                "contactNumber": "",
                "companyID": "",
                "companyName": "",
                "roleID": "",
                "deviceType": DEVICE_TYPE,
                "deviceToken": UserDefaults.standard.value(forKey: "DeviceToken")!,
                "registerType": GOOGLE_REGISTER
            ]
            
            self.signInWithSocial(parameters: parameters)
        }else {
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!, withError error: Error!) {
        DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

