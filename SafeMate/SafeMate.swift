//
//  SafeMate.swift
//

import UIKit

class SafeMate: UIViewController {
    
    // MARK: - Variables
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 0
    
    // MARK: - UI Controls
    @IBOutlet var contentView: UIView!
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet var imgHome: UIImageView!
    @IBOutlet var imgTeam: UIImageView!
    @IBOutlet var imgReports: UIImageView!
    @IBOutlet var imgMore: UIImageView!
    
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblTeam: UILabel!
    @IBOutlet var lblReports: UILabel!
    @IBOutlet var lblMore: UILabel!
    
    var objHomeVC: HomeVC!
    var objTeamVC: TeamVC!
    var objReportsVC: ReportsVC!
    var objMoreVC: MoreVC!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("UserID: \(String(describing: UserDefaults.standard.value(forKey: "UserID")))")
        print("UserDetails: \(String(describing: UserDefaults.standard.value(forKey: "UserDetails")))")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        objHomeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        objTeamVC = storyboard.instantiateViewController(withIdentifier: "TeamVC") as! TeamVC
        objReportsVC = storyboard.instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
        objMoreVC = storyboard.instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
        
        viewControllers = [objHomeVC, objTeamVC, objReportsVC, objMoreVC]
        
        buttons[selectedIndex].isSelected = true
        didPressTab(buttons[selectedIndex])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - UI Methods
    func setDefaults() {
        imgHome.image = UIImage(named: "home_normal.png")
        imgTeam.image = UIImage(named: "team_normal.png")
        imgReports.image = UIImage(named: "reports_normal.png")
        imgMore.image = UIImage(named: "more_normal.png")
        
        lblHome.textColor = UIColor(red: 159.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        lblTeam.textColor = UIColor(red: 159.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        lblReports.textColor = UIColor(red: 159.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        lblMore.textColor = UIColor(red: 159.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0)
    }
    
    // MARK: - IBAction Methods
    @IBAction func didPressTab(_ sender: UIButton) {
        let button: UIButton! = sender

        self.setDefaults()
        
        if (button.tag == 0) {
            imgHome.image = UIImage(named: "home_active.png")
            lblHome.textColor = UIColor(red: 30.0/255.0, green: 187.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        }else if (button.tag == 1) {
            imgTeam.image = UIImage(named: "team_active.png")
            lblTeam.textColor = UIColor(red: 30.0/255.0, green: 187.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        }else if (button.tag == 2) {
            imgReports.image = UIImage(named: "reports_active.png")
            lblReports.textColor = UIColor(red: 30.0/255.0, green: 187.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        }else {
            imgMore.image = UIImage(named: "more_active.png")
            lblMore.textColor = UIColor(red: 30.0/255.0, green: 187.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        }
        
        let previousIndex = selectedIndex
        
        selectedIndex = sender.tag
        buttons[previousIndex].isSelected = false
        
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParentViewController: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParentViewController()
        
        sender.isSelected = true

        let vc = viewControllers[selectedIndex]
        addChildViewController(vc)
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

