//
//  SiteAccessCodeAuthoriseVC.swift
//

import UIKit

class SiteAccessCodeAuthoriseVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnCheckbox: UIButton!
    
    @IBOutlet var lblWelcome: UILabel!
    
    @IBOutlet var viewNavigation: UIView!
    @IBOutlet var viewProfile: UIView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblWelcome.layer.cornerRadius = 10.0
        lblWelcome.layer.borderColor = UIColor.black.cgColor
        lblWelcome.layer.borderWidth = 1.0
        
        viewProfile.layer.cornerRadius = viewProfile.frame.size.width / 2.0
        viewProfile.layer.borderColor = UIColor.black.cgColor
        viewProfile.layer.borderWidth = 1.0
        
        btnCheckbox.layer.cornerRadius = 2.0
        btnCheckbox.layer.borderColor = UIColor.darkGray.cgColor
        btnCheckbox.layer.borderWidth = 1.0
        
        btnCheckbox.tag = 0
        btnCheckbox.setBackgroundImage(UIImage(named: ""), for: .normal)
        
        DELEGATE.setNavigationShadow(view: viewNavigation)
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        
    }
    
    @IBAction func onClickCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checkmark.png"), for: .normal)
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

