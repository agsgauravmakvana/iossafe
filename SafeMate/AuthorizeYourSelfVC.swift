//
//  AuthorizeYourSelfVC.swift
//

import UIKit

class AuthorizeYourSelfVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var strExit: String = ""
    var strEntry: String = ""
    var strAutoCheckIn: String = ""
    var strFullTraking: String = ""
    
    // MARK: - UI Controls
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet weak var switchExit: UISwitch!
    @IBOutlet weak var switchEntry: UISwitch!
    @IBOutlet weak var switchAuto: UISwitch!
    @IBOutlet weak var switchFullTime: UISwitch!
    
    @IBOutlet var txtNotifyEmail: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0

        if DELEGATE.isIPad() == false {
            switchExit.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchEntry.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchAuto.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchFullTime.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
        }
        
        switchEntry.isOn = true
        strEntry = "0"
        
        switchExit.isOn = true
        strExit = "0"
        
        switchAuto.isOn = true
        strAutoCheckIn = "0"
        
        switchFullTime.isOn = false
        strFullTraking = "1"
    }

    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func isSwitchEntryOn(sender: UISwitch) {
        if sender.isOn {
            strEntry = "0"
        }else {
            strEntry = "1"
        }
    }
    
    @IBAction func isSwitchExitOn(sender: UISwitch) {
        if sender.isOn {
            strExit = "0"
        }else {
            strExit = "1"
        }
    }
    
    @IBAction func isSwitchAutoCheckInOn(sender: UISwitch) {
        if sender.isOn {
            strAutoCheckIn = "0"
        }else {
            strAutoCheckIn = "1"
        }
    }
    
    @IBAction func isSwitchFullTimeOn(sender: UISwitch) {
        if sender.isOn {
            strFullTraking = "0"
        }else {
            strFullTraking = "1"
        }
    }
    
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtNotifyEmail.text = txtNotifyEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtNotifyEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtNotifyEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }

        DELEGATE.dictCheckIntoSite.setValue(txtNotifyEmail.text!, forKey: "notifyEmail")
        DELEGATE.dictCheckIntoSite.setValue(strEntry, forKey: "isEmailOnEntry")
        DELEGATE.dictCheckIntoSite.setValue(strExit, forKey: "isEmailOnExit")
        DELEGATE.dictCheckIntoSite.setValue(strAutoCheckIn, forKey: "isAutoCheckIn")
        DELEGATE.dictCheckIntoSite.setValue("1", forKey: "isSingleCheckIn")
        DELEGATE.dictCheckIntoSite.setValue(strFullTraking, forKey: "isFullTraking")
        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SafemateOrganizationVC = storyboard.instantiateViewController(withIdentifier: "SafemateOrganizationVC") as! SafemateOrganizationVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
