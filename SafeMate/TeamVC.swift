//
//  TeamVC.swift
//
//
//
//
import UIKit

class TeamVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MyTeamCellProtocol, MySitesCellProtocol {
    
    // MARK: - Variables
    var strFlag: String!
    var arrSelTeam: NSMutableArray = []
    var arrSiteList: Array<Dictionary<String,Any>> = []
    var arrPermissionUsers: NSMutableArray = []
    var arrTeamList: NSMutableArray = []
    var currentTeamHeight: CGFloat!
    var currentSiteHeight: CGFloat!
    var diff: CGFloat!
    var widthTitle: CGFloat!
    var widthAddress: CGFloat!
    
    // MARK: - UI Controls
    @IBOutlet var viewMySitesSep: UIView!
    @IBOutlet var viewMyTeamSep: UIView!
    @IBOutlet var viewSites: UIView!
    @IBOutlet var viewTeam: UIView!
    @IBOutlet var btnMySites: UIButton!
    @IBOutlet var btnMyTeam: UIButton!
    @IBOutlet var btnAddUpgrade: UIButton!
    @IBOutlet var btnReview: UIButton!
    @IBOutlet var tblSites: UITableView!
    @IBOutlet var tblTeam: UITableView!
    @IBOutlet weak var lblTotalActiveUsers: UILabel!
    @IBOutlet weak var lblTotalActiveSites: UILabel!
    @IBOutlet weak var lblSiteNotFound: UILabel!
    @IBOutlet weak var lblTeamNotFound: UILabel!
    @IBOutlet var lblSelectAll: UILabel!
    @IBOutlet var btnSelectAll: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DELEGATE.isIPad() {
            diff = 3.0
            currentTeamHeight = 100.0
            currentSiteHeight = 115.0
            widthAddress = 490.0
        }else{
            diff = 2.0
            currentTeamHeight = 180.0
            currentSiteHeight = 68.0
            widthAddress = 220.0
        }
        
        lblSiteNotFound.isHidden = true
        lblTeamNotFound.isHidden = true
        lblSelectAll.isHidden = true
        
        btnAddUpgrade.layer.cornerRadius = DELEGATE.setCorner()
        btnAddUpgrade.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnAddUpgrade.layer.borderWidth = 1.0
        
        btnReview.layer.cornerRadius = DELEGATE.setCorner()
        btnReview.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnReview.layer.borderWidth = 1.0
        
        self.setDefault()
        
        strFlag = "MySites"
        lblTitle.text = "Monitored Sites"
        btnMySites.setTitleColor(UIColor.white, for: .normal)
        viewMySitesSep.isHidden = false
        viewSites.isHidden = false
        viewTeam.isHidden = true
        
        btnSelectAll.isHidden = true
        btnSelectAll.tag = 0
        btnSelectAll.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        self.getMySites()
    }
    
    // MARK: - UI Methods
    func setDefault() {
        viewMySitesSep.isHidden = true
        viewMyTeamSep.isHidden = true
        
        btnMySites.setTitleColor(UIColor(red: 141.0/255.0, green: 221.0/255.0, blue: 234.0/255.0, alpha: 1.0), for: .normal)
        btnMyTeam.setTitleColor(UIColor(red: 141.0/255.0, green: 221.0/255.0, blue: 234.0/255.0, alpha: 1.0), for: .normal)
    }
    
    func onClickSelectedSite(_ sender: UIButton!) {
        let value = sender.tag;
       
        if self.arrSelTeam.contains(self.arrTeamList.object(at: value)) {
            self.arrSelTeam.remove(self.arrTeamList.object(at: value))
        }else {
            self.arrSelTeam.add(self.arrTeamList.object(at: value))
        }
        
        if (self.arrSelTeam.count == self.arrPermissionUsers.count) {
            btnSelectAll.tag = 5
            btnSelectAll.setBackgroundImage(UIImage(named: "checked_filled.png"), for: .normal)
        }else {
            btnSelectAll.tag = 0
            btnSelectAll.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        
//        if (self.arrSelTeam.count > 0) {
//            self.open(view: viewBottom)
//        }else {
//            self.close(view: viewBottom)
//        }
        
        self.tblTeam.reloadData()
        
        print("self.arrSelTeam: \(self.arrSelTeam)")
    }
    
    func getMySites() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_SITE_LIST)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageMySitesData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageMySitesData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
       
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let keyExists = dictData["siteListInfo"] != nil
            if keyExists == false {
                lblSiteNotFound.isHidden = false
                lblSiteNotFound.text = "Look like you don't have any site yet"
            }else {
                let siteListInfo = dictData["siteListInfo"] as! NSDictionary
                self.arrSiteList = siteListInfo["siteList"] as! Array<Dictionary<String,String>>
                self.tblSites.reloadData()
                
                btnMySites.setTitle("MY SITES (\(self.arrSiteList.count))", for: .normal)
                
                let totalActiveUsers = siteListInfo["totalActiveUsers"] as! String
                lblTotalActiveSites.text = totalActiveUsers
                
                let totalUsers = siteListInfo["totalUsers"] as! String
                lblTotalActiveUsers.text = String(format: "%@/%@", totalActiveUsers,totalUsers)
                
                if (self.arrSiteList.count > 0) {
                    lblSiteNotFound.isHidden = true
                    lblSiteNotFound.text = ""
                }else {
                    lblSiteNotFound.isHidden = false
                    lblSiteNotFound.text = "Look like you don't have any site yet"
                }
            }
        }
    }
    
    func getMyTeam() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_MYTEAM_LIST)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageMyTeamData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageMyTeamData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let keyExists = dictData["teamList"] != nil
            if keyExists == false {
                lblTeamNotFound.isHidden = false
                lblTeamNotFound.text = "Look like you don't have any team yet"
                
                lblSelectAll.isHidden = true
                btnSelectAll.isHidden = true
            }else {
                self.arrTeamList = NSMutableArray(array: dictData["teamList"] as!Array<Dictionary<String,AnyObject>>)
                btnMyTeam.setTitle("MY TEAMS (\(self.arrTeamList.count))", for: .normal)
                
                if (self.arrTeamList.count > 0) {
                    self.arrPermissionUsers.removeAllObjects()
                    for i in 0...self.arrTeamList.count - 1 {
                        let dict = self.arrTeamList[i] as! NSDictionary
                        let dictSiteDetail = dict["siteDetail"] as! NSDictionary
                        if (dictSiteDetail["permission"] as! String) == "1" {
                            self.arrPermissionUsers.add(dict)
                        }
                    }
                }
                
                if (self.arrTeamList.count > 0) {
                    lblTeamNotFound.isHidden = true
                    lblTeamNotFound.text = ""
                    
                    lblSelectAll.isHidden = false
                    btnSelectAll.isHidden = false
                }else {
                    lblTeamNotFound.isHidden = false
                    lblTeamNotFound.text = "Look like you don't have any team yet"
                    
                    lblSelectAll.isHidden = true
                    btnSelectAll.isHidden = true
                }
                
                self.tblTeam.reloadData()
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickSitesTeam(sender: UIButton) {
        let button: UIButton! = sender
        
        self.setDefault()
        button.setTitleColor(UIColor.white, for: .normal)
        
        if (button == btnMySites) {
            strFlag = "MySites"
            lblTitle.text = "Monitored Sites"
            viewMySitesSep.isHidden = false
            viewSites.isHidden = false
            viewTeam.isHidden = true
            
            if !(self.arrSiteList.count > 0) {
                self.getMySites()
            }
        }else {
            strFlag = "MyTeam"
            lblTitle.text = "Monitored Teams"
            viewMyTeamSep.isHidden = false
            viewSites.isHidden = true
            viewTeam.isHidden = false
            
            if !(self.arrTeamList.count > 0) {
                self.getMyTeam()
            }
        }
    }
    
    @IBAction func onClickAddUpgradeSite(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: AddNewSiteVC = storyboard.instantiateViewController(withIdentifier: "AddNewSiteVC") as! AddNewSiteVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickReview(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: PendingUsersVC = storyboard.instantiateViewController(withIdentifier: "PendingUsersVC") as! PendingUsersVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickSelectAllCheckbox(sender: UIButton) {
        self.arrSelTeam.removeAllObjects()
       
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled.png"), for: .normal)
            
            if (self.arrPermissionUsers.count > 0) {
                for i in 0...self.arrPermissionUsers.count - 1 {
                    let dict = self.arrPermissionUsers[i] as! NSDictionary
                    self.arrSelTeam.add(dict)
                }
            }
            
//            self.arrSelPendingUsers = self.arrPendingUsers.mutableCopy() as! NSMutableArray
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        
//        if (self.arrSelPendingUsers.count > 0) {
//            self.open(view: viewBottom)
//        }else {
//            self.close(view: viewBottom)
//        }
        
        self.tblTeam.reloadData()
        
        print("self.arrSelTeam: \(self.arrSelTeam)")
    }
    
    // MARK: - Delegate Methods
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == tblSites) {
            return self.arrSiteList.count
        }else {
            return self.arrTeamList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == tblSites) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MySitesCell", for: indexPath) as! MySitesCell
            cell.delegate = self
            cell.idx = indexPath.row
         
            let dict = self.arrSiteList[indexPath.row] as AnyObject
            cell.lblSiteName.text = "\(dict["siteName"] as! String)"
            cell.lblSiteAddress.text =  "\(dict["siteAddress"] as! String)"

            let siteUsers = dict["siteUsers"] as! String
            let siteActiveUsers = dict["siteActiveUsers"] as! String
            cell.lblSiteUsers.text = String(format: "Users:\n%@/%@",siteActiveUsers,siteUsers) as String

            cell.lblSiteAddress.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblSiteAddress.sizeToFit()
            cell.lblSiteAddress.frame.size.width = widthAddress
            
            cell.viewBack.frame.size.height = cell.lblSiteAddress.frame.origin.y + cell.lblSiteAddress.frame.size.height + diff
            cell.imgbackground.frame.size.height = cell.viewBack.frame.origin.y + cell.viewBack.frame.size.height
            
            if DELEGATE.isIPad() {
                cell.lblSiteUsers.frame.size.height = cell.viewBack.frame.size.height + 5.0
            }else {
                cell.lblSiteUsers.frame.size.height = cell.viewBack.frame.size.height + cell.lblSiteName.frame.origin.y
            }
            
            cell.btnBackView.frame.size.height = cell.imgbackground.frame.size.height
            
            currentSiteHeight = cell.imgbackground.frame.origin.y +  cell.imgbackground.frame.size.height
           
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyTeamCell", for: indexPath) as! MyTeamCell
            cell.delegate = self
            cell.idx = indexPath.row
            
            cell.imgbackground.isHidden = true
            cell.viewBackground.layer.shadowOpacity = 0.3;
            cell.viewBackground.layer.shadowOffset = CGSize(width: 1, height: 1);
            cell.viewBackground.layer.shadowRadius = 3;
            cell.viewBackground.layer.cornerRadius = 0
            cell.viewBackground.layer.masksToBounds = false
            cell.viewBackground.clipsToBounds = false
            
            cell.btnTeamCheckbox.addTarget(self, action:#selector(self.onClickSelectedSite(_:)), for: .touchUpInside)
            cell.btnTeamCheckbox.tag = indexPath.row
            
            let dict = self.arrTeamList[indexPath.row] as AnyObject
            cell.lblTeamName.text = "\(dict["firstName"] as! String) \(dict["lastName"] as! String) (\(dict["companyName"] as! String))"
            
            let sitedetails = dict["siteDetail"] as! NSDictionary
            cell.lblTeamAddress.text = "\(sitedetails["siteAddress"] as! String)"
            cell.lblTeamSIteName.text = "\(sitedetails["siteName"] as! String)"
            
            let status = dict["userstatus"] as! String
            if status == "1" {
                let main_string = "\(dict["roleName"] as! String) \("Online")"
                let range = (main_string as NSString).range(of: "Online")
               
                let attribute = NSMutableAttributedString.init(string: main_string)
                attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 27.0/255.0, green: 177.0/255.0, blue: 59.0/255.0, alpha: 1.0) , range: range)
                
                cell.lblTeamPosition.attributedText = attribute
            }else {
                let main_string = "\(dict["roleName"] as! String) \("Offline")"
                let range = (main_string as NSString).range(of: "Offline")
                
                let attribute = NSMutableAttributedString.init(string: main_string)
                attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray , range: range)
                
                cell.lblTeamPosition.attributedText = attribute
            }

            if self.arrSelTeam.contains(self.arrTeamList.object(at: indexPath.row)) {
                cell.btnTeamCheckbox.setBackgroundImage(UIImage(named:"checked_filled.png"), for: UIControlState())
                cell.viewBackground.backgroundColor = UIColor(red: 228.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            }else {
                cell.btnTeamCheckbox.setBackgroundImage(UIImage(named:"unchecked.png"), for: UIControlState())
                cell.viewBackground.backgroundColor = UIColor.white
            }
            
            if (sitedetails["permission"] as! String) == "1" {
                if DELEGATE.isIPad() {
                    widthTitle = 545.0
                }else {
                    widthTitle = 245.0
                }
                
                cell.btnTeamCheckbox.isHidden = false
            }else {
                if DELEGATE.isIPad() {
                    widthTitle = 588.0
                }else {
                    widthTitle = 270.0
                }
                
                cell.btnTeamCheckbox.isHidden = true
            }
            
            cell.lblTeamName.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblTeamName.sizeToFit()
            cell.lblTeamName.frame.size.width = widthTitle
            
            cell.lblTeamAddress.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.lblTeamAddress.sizeToFit()
            cell.lblTeamAddress.frame.size.width = widthTitle
     
            cell.lblTeamPosition.frame.origin.y = cell.lblTeamName.frame.origin.y + cell.lblTeamName.frame.size.height + diff
            cell.lblTeamSIteName.frame.origin.y = cell.lblTeamPosition.frame.origin.y + cell.lblTeamPosition.frame.height + diff
            cell.lblTeamAddress.frame.origin.y = cell.lblTeamSIteName.frame.origin.y + cell.lblTeamSIteName.frame.height + diff
            cell.viewBackground.frame.size.height = cell.lblTeamAddress.frame.origin.y + cell.lblTeamAddress.frame.size.height + diff
            
            currentTeamHeight = cell.viewBackground.frame.origin.y + cell.viewBackground.frame.size.height

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableView == tblTeam) {
            return currentTeamHeight + 10.0
        }else {
            return currentSiteHeight + 5.0
        }
    }
    
    // MARK: MySitesCellProtocol
    func getSitesDetails(index: NSInteger) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: ExistingSiteVC = storyboard.instantiateViewController(withIdentifier: "ExistingSiteVC") as! ExistingSiteVC
        objVC.dictPrev = self.arrSiteList[index] as NSDictionary
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: MyTeamCellProtocol
    func getTeamDetails(index: NSInteger) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: YourHistoryVC = storyboard.instantiateViewController(withIdentifier: "YourHistoryVC") as! YourHistoryVC
        objVC.dictPrev = self.arrTeamList[index] as! NSDictionary
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

