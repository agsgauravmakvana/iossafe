//
//  AppDelegate.swift
//

import UIKit
import FBSDKLoginKit
import LinkedinSwift
import IQKeyboardManagerSwift
import Contacts
import CoreLocation

let DELEGATE = UIApplication.shared.delegate as! AppDelegate

extension String {
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    // MARK: - Variables
    let arrContacts = NSMutableArray()
    var arrQuestions = NSMutableArray()
    var objSignIn: SignIn!
    var objSafeMate: SafeMate!
    var dictUserDetails: NSDictionary!
    var dictNewSite = NSMutableDictionary()
    var dictCheckIntoSite = NSMutableDictionary()
    var strUserID: String = ""
    var locationManager = CLLocationManager()
    var currentlocation = CLLocation()
    var locationStatus: NSString = "Not Started"
    var isNewSite: Bool = false
    
    // MARK: - UIControls
    var window: UIWindow?

    // MARK: - Delegate Methods
    // MARK: UIApplication
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
        IQKeyboardManager.sharedManager().toolbarTintColor = UIColor(red: 0.0/255.0, green: 82.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        
        GMSServices.provideAPIKey(GOOGLE_MAP_API_KEY)
        
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: CLIENT_ID_FOR_PRODUCTION, PayPalEnvironmentSandbox: CLIENT_ID_FOR_SANDBOX])
        
        if !UserDefaults.standard.bool(forKey: "isFirstTimeOnly") {
            UserDefaults.standard.set(true, forKey: "isFirstTimeOnly")
            UserDefaults.standard.set(false, forKey: "isLogin")
            UserDefaults.standard.set(true, forKey: "isFirstTime")
        }
        
        // For get device token
        let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
        
        // For testing
        let deviceToken = UserDefaults.standard.object(forKey: "DeviceToken") as? String
        if (deviceToken == nil) {
            UserDefaults.standard.set("0C1DE57E4A9342768268WG92E398D7E76C09E6801679F651D030691B07CA90A9", forKey: "DeviceToken")
            UserDefaults.standard.synchronize()
        }
        
        // For get current location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        self.getAllContacts()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let strDeviceToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("strDeviceToken: \(strDeviceToken)")
        
        UserDefaults.standard.set(strDeviceToken, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Couldn’t register (error) : \(error.localizedDescription)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
      
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
       
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if (url.scheme?.hasPrefix("fb"))! {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }else if (url.scheme?.hasPrefix("li"))! {
            return LinkedinSwiftHelper.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }else {
            return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication!, annotation: annotation)
        }
    }
    
    // MARK: CLLocationManager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationCoordinat: CLLocationCoordinate2D = manager.location!.coordinate
        
        let latitude = locationCoordinat.latitude;
        let longitude = locationCoordinat.longitude;
        print("latitude: \(latitude) longitude: \(longitude)")
        
        currentlocation = CLLocation(latitude: latitude, longitude: longitude)
        locationManager.stopUpdatingLocation()
    }

    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false

        switch status {
            case CLAuthorizationStatus.restricted:
                locationStatus = "Restricted Access to location"
            case CLAuthorizationStatus.denied:
                locationStatus = "User denied access to location"
            case CLAuthorizationStatus.notDetermined:
                locationStatus = "Status not determined"
            default:
                locationStatus = "Allowed to location Access"
                
            shouldIAllow = true
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LabelHasbeenUpdated"), object: nil)
        if (shouldIAllow == true) {
            // Start location services
            locationManager.startUpdatingLocation()
            
            print("Location to Allowed")
        }else {
            print("Denied access: \(locationStatus)")
        }
    }
    
    // MARK:- UI Methods
    func checkDevice() -> String {
        var strDevice: String = ""
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            strDevice = "Main_iPad"
        }else {
            strDevice = "Main"
        }
        return strDevice
    }
    
    func isIPad() -> Bool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            return true
        }else {
            return false
        }
    }
    
    func showPopup(title: String, type: AlertType) {
        let alert = Alert(title: title, duration: CGFloat(Float(CFloat("2.0")!)), completion: {() -> Void in
            
        })
        alert?.showStatusBar = false
        alert?.alertType = type
        alert?.incomingTransition = .slideFromTop
        alert?.outgoingTransition = .slideToTop
        alert?.bounces = true
        alert?.show()
    }
    
    func showToast(title: String, view: UIView) {
        let toast = HJToast()
        toast.backgroundColor = UIColor.red
        toast.bakeToast(withMessage: title, inView: view)
    }
    
    func isValidEmail(strEmailId: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: strEmailId)
    }
    
    func isValidPassword(strPassword: String) -> Bool {
        let regularExpression = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{6,15}$"
        let passwordValidation = NSPredicate(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: strPassword)
    }
    
    func isValidCharecter(strchare: String) -> Bool {
        let regularExpression = "^[a-zA-Z]+$"
        let passwordValidation = NSPredicate(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: strchare)
    }
    
    func setCorner() -> CGFloat {
        if self.isIPad() {
            return 8.0
        }else {
            return 5.0
        }
    }
    
    func showLoading(view: UIView) {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    func hideLoading(view: UIView) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
    
    func showAlert(title: String, message:String) {
        let alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
        alertView.show()
    }
    
    func getAllContacts() {
        let status = CNContactStore.authorizationStatus(for: CNEntityType.contacts) as CNAuthorizationStatus
        if status == CNAuthorizationStatus.denied {
            self.showAlert(title: APP_NAME, message: "This app previously was refused permissions to contacts; Please go to settings and grant permission to this app so it can use contacts")
            return
        }
        
        let store = CNContactStore()
        store.requestAccess(for: CNEntityType.contacts) {_,_ in
            
            let request = CNContactFetchRequest(keysToFetch:[
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                CNContactIdentifierKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactMiddleNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor])
            
            do {
                try store.enumerateContacts(with: request, usingBlock: { (contact: CNContact, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                    
                    let emailAddresses = contact.emailAddresses as NSArray
                    if emailAddresses.count > 0 {
                        for index in 0...emailAddresses.count - 1 {
                            let dict = NSMutableDictionary()
                            dict.setValue((contact.identifier), forKey: "identifier")
                            dict.setValue((contact.familyName), forKey: "familyName")
                            dict.setValue((contact.givenName), forKey: "givenName")
                            dict.setValue((contact.middleName), forKey: "middleName")
                            dict.setValue(contact.givenName + " " + contact.familyName, forKey: "displayName")
                            dict.setValue(((contact.emailAddresses[index].value) as String), forKey: "emailAddress")
                            self.arrContacts.add(dict)
                        }
                    }
                })
            }catch {
                return;
            }
        }
    }
    
    func checkInternetConnection() -> Bool {
        if Reachability.isConnectedToNetwork() == false {
            self.showPopup(title: Network_Error, type: .error)
            return false
        }else {
            return true
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

