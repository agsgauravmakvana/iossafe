//
//  SuccessVC.swift
//

import UIKit
import MessageUI

class SuccessVC: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    // MARK: - Variables
    var dictPrev: NSDictionary!
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblTeamCode: UILabel!
    @IBOutlet var lblSiteCode: UILabel!
    @IBOutlet var btnEmailCode: UIButton!
    @IBOutlet var btnSMSCode: UIButton!
    @IBOutlet var btnDone: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        lblSiteCode.text = PartialFormatter().formatPartial("\(dictPrev.value(forKey: "siteKey") as! String)")
        lblTeamCode.text = PartialFormatter().formatPartial("\(dictPrev.value(forKey: "accessCode") as! String)")
        
        btnEmailCode.layer.cornerRadius = DELEGATE.setCorner()
        btnEmailCode.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnEmailCode.layer.borderWidth = 1.0
        
        btnSMSCode.layer.cornerRadius = DELEGATE.setCorner()
        btnSMSCode.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnSMSCode.layer.borderWidth = 1.0
        
        btnDone.layer.cornerRadius = DELEGATE.setCorner()
        btnDone.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnDone.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickEmailCode(sender: UIButton) {
        if (MFMailComposeViewController.canSendMail()) {
            let mailComposeViewController = MFMailComposeViewController()
            mailComposeViewController.mailComposeDelegate = self
            mailComposeViewController.setToRecipients([])
            mailComposeViewController.setSubject("")
            mailComposeViewController.setMessageBody("You are invited to \((DELEGATE.dictNewSite.value(forKey: "siteName") as! String)) site. Please use \((dictPrev.value(forKey: "accessCode") as! String)) as access code and \((dictPrev.value(forKey: "siteKey") as! String)) as site key.", isHTML: false)
            
            self.present(mailComposeViewController, animated: true, completion: nil)
        }else {
            DELEGATE.showPopup(title: Send_Email_Error, type: .error)
        }
    }
    
    @IBAction func onClickSMSCode(sender: UIButton) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "You are invited to \((DELEGATE.dictNewSite.value(forKey: "siteName") as! String)) site. Please use \((dictPrev.value(forKey: "accessCode") as! String)) as access code and \((dictPrev.value(forKey: "siteKey") as! String)) as site key."
            controller.recipients = []
            controller.messageComposeDelegate = self
            
            self.present(controller, animated: true, completion: nil)
        }else {
            DELEGATE.showPopup(title: Send_SMS_Error, type: .error)
        }
    }
    
    @IBAction func onClickDone(sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isLogin")
        
        let dict = UserDefaults.standard.value(forKey: "UserDetails") as! NSDictionary
        let dictUserDetails: NSMutableDictionary = NSMutableDictionary(dictionary: dict)
        dictUserDetails.setValue("1", forKey: "isNewUser")
        
        DELEGATE.dictUserDetails = dictUserDetails as NSDictionary
        UserDefaults.standard.setValue(DELEGATE.dictUserDetails, forKey: "UserDetails")
        
        print("UserID: \(String(describing: UserDefaults.standard.value(forKey: "UserID")))")
        print("UserDetails: \(String(describing: UserDefaults.standard.value(forKey: "UserDetails")))")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
        objVC.selectedIndex = 1
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: MFMailComposeViewController
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail was cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail was saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail was sent")
        case MFMailComposeResult.failed.rawValue:
            print("Mail sent failed: %@", [error!.localizedDescription])
        default:
            break
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: MFMessageComposeViewController
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result.rawValue {
        case MessageComposeResult.cancelled.rawValue:
            print("Message was cancelled")
        case MessageComposeResult.sent.rawValue:
            print("Message was sent")
        case MessageComposeResult.failed.rawValue:
            print("Message failed")
        default:
            break
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

