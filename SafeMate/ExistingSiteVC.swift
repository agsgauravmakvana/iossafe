//
//  ExistingSiteVC.swift
//

import UIKit

class ExistingSiteVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, GMSMapViewDelegate, ExistingSiteCellProtocol {
    
    // MARK: - Variables
    var locationManager = CLLocationManager()
    var mapTasks = MapTasks()
    var locationMarker: GMSMarker!
    let geoFenceCircle = GMSCircle()
    var radius: Double = 0.0
    var currentCoradinate: CLLocationCoordinate2D!
    var dictPrev: NSDictionary!
    var teamList: Array<Dictionary<String,Any>> = []
    
    // MARK: - UI Controls
    @IBOutlet var tblExistingSite: UITableView!
    @IBOutlet var viewMap: GMSMapView!
    @IBOutlet weak var lblNotFound: UILabel!
    @IBOutlet weak var lblSiteName: UILabel!
    @IBOutlet weak var lblAccessCode: UILabel!
    @IBOutlet weak var lblSiteKey: UILabel!
    @IBOutlet weak var lblMyTeam: UILabel!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        lblNotFound.isHidden = true
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

        viewMap.delegate = self
        
        self.getSiteInfo()
    }
    
    // MARK: - UI Methods
    func getSiteInfo() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_SITE_INFO)
         let parameters: [AnyHashable: Any] = [
            "userID": (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "siteID": (dictPrev.value(forKey: "siteID") as! String)
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSiteInfoData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageSiteInfoData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let keyExists = dictData["siteDetail"] != nil
            if keyExists == false {
                lblNotFound.isHidden = false
                lblNotFound.text = "Look like you don't have any team yet"
            }else {
                let siteDetails = dictData["siteDetail"] as! NSDictionary

                lblSiteName.text = "\(siteDetails.value(forKey: "siteName") as! String)"
                lblSiteKey.text = PartialFormatter().formatPartial("\(siteDetails.value(forKey: "siteKey") as! String)")
                lblAccessCode.text = PartialFormatter().formatPartial("\(siteDetails.value(forKey: "accessCode") as! String)")

                radius = NSString(string: siteDetails["siteRange"] as! String).doubleValue
                
                let latitude = "\(siteDetails["siteLatitude"] as! String)"
                let longitude = "\(siteDetails["siteLogitude"] as! String)"
                let coordinate = CLLocationCoordinate2D(latitude:Double(latitude)!, longitude:Double(longitude)!)
                self.viewMap.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 14.0)
                self.setupLocationMarker(coordinate)
                
                teamList = siteDetails["teamList"] as! Array<Dictionary<String,String>>
                lblMyTeam.text = String(format:"My Team (%i)",teamList.count)

                if (self.teamList.count > 0) {
                    lblNotFound.isHidden = true
                    lblNotFound.text = ""
                }else {
                    lblNotFound.isHidden = false
                    lblNotFound.text = "Look like you don't have any team yet"
                }
                
                self.tblExistingSite.reloadData()
            }
        }
    }
    
    func setupLocationMarker(_ coordinate: CLLocationCoordinate2D) {
        if locationMarker != nil {
            locationMarker.map = nil
        }
        
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = viewMap
        
        locationMarker.title = mapTasks.fetchedFormattedAddress
        locationMarker.appearAnimation = kGMSMarkerAnimationPop
        locationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        locationMarker.opacity = 1.0
        locationMarker.isFlat = true
        
        geoFenceCircle.radius = radius
        geoFenceCircle.position = coordinate
        geoFenceCircle.fillColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 0.2)
        geoFenceCircle.strokeWidth = 1.0
        geoFenceCircle.strokeColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        geoFenceCircle.map = viewMap
    }

    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickEditSite(sender: UIButton) {
        DELEGATE.isNewSite = false
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: NewSite = storyboard.instantiateViewController(withIdentifier: "NewSite") as! NewSite
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teamList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExistingSiteCell", for: indexPath) as! ExistingSiteCell
        cell.delegate = self
        cell.idx = indexPath.row
        
        let dict = self.teamList[indexPath.row] as AnyObject
        cell.lblFirstAndLastName.text = "\(dict["firstName"] as! String) \(dict["lastName"] as! String)"
        cell.lblRoleName.text = "\(dict["roleName"] as! String)"
        cell.lblCompanyName.text = "\(dict["companyName"] as! String)"
        
        return cell
    }
    
    // MARK: ExistingSiteCellProtocol
    func getExistingSiteDetails(index: NSInteger) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: YourInformationVC = storyboard.instantiateViewController(withIdentifier: "YourInformationVC") as! YourInformationVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: CLLocationManager
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    // MARK: GMSMapView
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

