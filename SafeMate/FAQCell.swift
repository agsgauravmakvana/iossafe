//
//  FAQCell.swift
//

import UIKit

protocol FAQCellProtocol : class {
    func getFAQDetails(indexPath: IndexPath)
}

class FAQCell: UITableViewCell {

    // MARK: - Variables
    var delegate : FAQCellProtocol?
    var idxPath = IndexPath(row: 0, section: 0)
    
    // MARK: - UIControls
    @IBOutlet weak var lblQuestion : UILabel!
    @IBOutlet weak var lblAnswer : UILabel!
    @IBOutlet weak var btnDetails : UIButton!
    @IBOutlet weak var imgArrow : UIImageView!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - UI Methods
    func setValues(_ faqData: FAQData) {
        print("\(String(describing: faqData.faqQuestion))")
        print("\(String(describing: faqData.faqAnswer))")

        let paragraphStyle = NSMutableParagraphStyle()
        
        if DELEGATE.isIPad() {
            paragraphStyle.lineSpacing = 5.0
        }else {
            paragraphStyle.lineSpacing = 3.0
        }
        
        let attribute = [
            NSFontAttributeName: UIFont(name: lblAnswer.font.fontName, size: lblAnswer.font.pointSize)!,
            NSForegroundColorAttributeName: lblAnswer.textColor] as [String : Any]
        
        let attributedString = NSMutableAttributedString(string: faqData.faqAnswer!, attributes: attribute )
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.lblQuestion.text = faqData.faqQuestion
        self.lblAnswer.attributedText = attributedString

        let faqShown = faqData.faqShown
        self.lblAnswer.isHidden = !faqShown
        
         self.imgArrow.image = faqShown ? UIImage(named: "faq_expand.png") : UIImage(named: "faq_collapse.png")
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickDetails(_ sender: Any) {
        delegate?.getFAQDetails(indexPath: idxPath)
    }
    
}
