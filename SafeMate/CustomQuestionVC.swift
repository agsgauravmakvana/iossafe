//
//  CustomQuestionVC.swift
//

import UIKit

protocol CustomQuestionVCProtocol : class {
    func reloadCustomQuestionList()
}

class CustomQuestionVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CustomQuestionCellProtocol, UITextFieldDelegate {
    
    // MARK: - Variables
    var strInputRequired: String = ""
    var strFieldType: String = ""
    weak var delegate : CustomQuestionVCProtocol?
    var dictPrev: NSDictionary!
    var arrOptions = NSMutableArray()
    var indexAdd: NSInteger = 0
    var strOption: String = ""
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnInputRequired: UIButton!
    @IBOutlet var btnRadioButton: UIButton!
    @IBOutlet var btnCheckBox: UIButton!
    @IBOutlet var btnDropDown: UIButton!
    @IBOutlet var btnAddMore: UIButton!
    @IBOutlet var tblOptions: UITableView!
    @IBOutlet var txtQuestion: ACFloatingTextfield!
    @IBOutlet var txtHelpText: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        btnCancel.layer.cornerRadius = DELEGATE.setCorner()
        btnCancel.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnCancel.layer.borderWidth = 1.0
        
        btnSave.layer.cornerRadius = DELEGATE.setCorner()
        btnSave.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnSave.layer.borderWidth = 1.0
        
        strInputRequired = "1"
        btnInputRequired.tag = 0
        btnInputRequired.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        strFieldType = "3"
        self.onClickAnswerRadio(sender: btnRadioButton)
        
        self.arrOptions.add("")
    }
    
    // MARK: - UI Methods
    func addQuestion() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, ADD_QUESTION)
        
        let parameters: [AnyHashable: Any] = [
            "fieldType" : strFieldType,
            "siteID" : (dictPrev.value(forKey: "siteID") as! String),
            "question" : txtQuestion.text!,
            "helpText" : txtHelpText.text!,
            "isRequired" : strInputRequired,
            "isAllowMultiline" : "1",
            "minChar" : "0",
            "maxChar" : "0",
            "inputType" : "1",
            "options" : strOption
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageAddQuestionData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageAddQuestionData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let dict = dictData["questionDetail"] as! NSDictionary
            DELEGATE.arrQuestions.add(dict)
            
            delegate?.reloadCustomQuestionList()
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCancel(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSave(sender: UIButton) {
        txtQuestion.text = txtQuestion.text?.trimmingCharacters(in: .whitespaces)
        if txtQuestion.text == "" {
            DELEGATE.showPopup(title: Empty_Question, type: .error)
            return
        }
        
        self.strOption = ""
        if (self.arrOptions.count > 0) {
            for row in 0...self.arrOptions.count - 1 {
                let indexPath = NSIndexPath(row: row, section: 0) as IndexPath
                let cell: CustomQuestionCell = self.tblOptions.cellForRow(at: indexPath) as! CustomQuestionCell
                
                cell.txtOption.text = cell.txtOption.text?.trimmingCharacters(in: .whitespaces)
                if cell.txtOption.text == "" {
                    DELEGATE.showPopup(title: Empty_Options, type: .error)
                    return
                }else {
                    self.strOption = self.strOption.appending(NSString(format:"#;%@", cell.txtOption.text!) as String)
                    continue
                }
            }
            
            self.strOption = String(self.strOption.characters.dropFirst())
            self.strOption = String(self.strOption.characters.dropFirst())
        }
        
        print("strOption: \(self.strOption)")
        
        if self.strOption == "" {
            DELEGATE.showPopup(title: Empty_Options, type: .error)
            return
        }
        
        self.addQuestion()
    }
    
    @IBAction func onClickInputRequiredCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
            
            strInputRequired = "0"
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
            
            strInputRequired = "1"
        }
    }
    
    @IBAction func onClickAnswerRadio(sender: UIButton) {
        btnRadioButton.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        btnCheckBox.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        btnDropDown.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        
        sender.setBackgroundImage(UIImage(named: "radio_selected.png"), for: .normal)
        
        if (sender == btnRadioButton) {
            strFieldType = "3"
        }else if (sender == btnCheckBox) {
            strFieldType = "2"
        }else {
            strFieldType = "1"
        }
    }
    
    @IBAction func onClickAddMore(sender: UIButton) {
        indexAdd = indexAdd + 1
        self.arrOptions.add("")
        self.tblOptions.reloadData()
    }
    
    // MARK: - Delegate Methods
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomQuestionCell", for: indexPath) as! CustomQuestionCell
        cell.delegate = self
        cell.idx = indexPath.row

        cell.txtOption.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        cell.txtOption.tag = indexPath.row
        cell.txtOption.text = (self.arrOptions[indexPath.row] as! String)
        
        if (indexPath.row == 0) {
            cell.btnDelete.isHidden = true
        }else {
            cell.btnDelete.isHidden = false
        }
        
        return cell
    }
    
    func textFieldDidChange() {
        
    }
    
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (txtTemp == txtQuestion || txtTemp == txtHelpText) {
            
        }else {
            txtTemp.text = txtTemp.text?.trimmingCharacters(in: .whitespaces)
            self.arrOptions.replaceObject(at: txtTemp.tag, with: txtTemp.text!)
        }
    }
    
    // MARK: CustomQuestionCell
    func deleteRow(index: NSInteger) {
        print("index: \(index)")
        
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.arrOptions.removeObject(at: index)
        self.tblOptions.reloadData()
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

