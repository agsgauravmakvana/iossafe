//
//  AdditionalAdministratorsVC.swift
//

import UIKit

class AdditionalAdministratorsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ContactsVCProtocol {
    
    // MARK: - Variables
    var strAuthUsers: String = ""
    var arrEmails: NSMutableArray = []
    var strPermission: String = ""
    
    // MARK: - UI Controls
    @IBOutlet var btnAddEmail: UIButton!
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnView: UIButton!
    
    @IBOutlet var tblEmails: UITableView!
    @IBOutlet var txtEmail: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAddEmail.layer.cornerRadius = btnAddEmail.frame.size.width / 2.0
        btnAddEmail.layer.borderColor = UIColor(red: 245.0/255.0, green: 107.0/255.0, blue: 20.0/255.0, alpha: 1.0).cgColor
        btnAddEmail.layer.borderWidth = 1.0
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
        
        btnSelect.layer.cornerRadius = DELEGATE.setCorner()
        btnSelect.layer.borderColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0).cgColor
        btnSelect.layer.borderWidth = 1.0
        
        btnEdit.tag = 0
        btnEdit.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btnView.tag = 0
        btnView.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
    }
    
    // MARK: - UI Methods
    func checkEmailExists() -> Bool {
        var flag: Bool = false
        
        for index in 0...arrEmails.count - 1 {
            let dict: NSDictionary = self.arrEmails[index] as! NSDictionary
            if ((dict["email"] as! String) == txtEmail.text) {
                DELEGATE.showPopup(title: Email_Alreday_Exists, type: .error)
                flag = true
            }
        }

        return flag
    }
    
    func checkPermission() {
        var edit: String = "false"
        var view: String = "false"
        
        if (btnEdit.tag == 0) {
            edit = "false"
        }else {
            edit = "true"
        }
        
        if (btnView.tag == 0) {
            view = "false"
        }else {
            view = "true"
        }
        
        strPermission = ""
        if (edit == "false" && view == "true") {
            strPermission = "1"
        }else if (edit == "true" && view == "false") {
            strPermission = "2"
        }else if (edit == "true" && view == "true") {
            strPermission = "3"
        }else {
            strPermission = ""
        }
    }
    
    func addNewEmail() {
        let dict : NSDictionary = [
            "email" : txtEmail.text!,
            "permission" : strPermission
        ]
        arrEmails.add(dict)
        
        btnEdit.tag = 0
        btnEdit.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btnView.tag = 0
        btnView.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        txtEmail.text = ""
        
        tblEmails.reloadData()
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddEmail(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        /*let peoples: Int = Int(DELEGATE.dictNewSite.value(forKey: "peoples") as! String)!
        print("\(peoples)")
        
        if (self.arrEmails.count == peoples) {
            DELEGATE.showPopup(title: "You Can Add Only \(peoples) Peoples", type: .error)
            return
        }*/
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
            DELEGATE.showPopup(title: Valid_Email, type: .error)
            return
        }else {
            if (self.arrEmails.count > 0) {
                if self.checkEmailExists() {
                    DELEGATE.showPopup(title: Email_Alreday_Exists, type: .error)
                    return
                }else {
                    self.checkPermission()
                    
                    if (strPermission == "") {
                        DELEGATE.showPopup(title: Email_Permission, type: .error)
                        return
                    }else {
                        self.addNewEmail()
                    }
                }
            }else {
                self.checkPermission()
                
                if (strPermission == "") {
                    DELEGATE.showPopup(title: Email_Permission, type: .error)
                    return
                }else {
                    self.addNewEmail()
                }
            }
        }
    }
    
    @IBAction func onClickSelect(sender: UIButton) {
        /*let peoples: Int = Int(DELEGATE.dictNewSite.value(forKey: "peoples") as! String)!
        print("\(peoples)")
        
        if (self.arrEmails.count == peoples) {
            DELEGATE.showPopup(title: "You Can Add Only \(peoples) Peoples", type: .error)
        }else {
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            let objVC: ContactsVC = storyboard.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
            objVC.delegate = self
            self.navigationController?.pushViewController(objVC, animated: true)
        }*/
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: ContactsVC = storyboard.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        objVC.delegate = self
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        self.strAuthUsers = ""
        
        if (self.arrEmails.count > 0) {
            for index in 0...self.arrEmails.count - 1 {
                let dict: NSDictionary = self.arrEmails[index] as! NSDictionary
                let email = dict["email"] as! String
                let permission = dict["permission"] as! String
                
                self.strAuthUsers = self.strAuthUsers.appending(NSString(format:",%@#;%@", email, permission) as String)
            }
            self.strAuthUsers = String(self.strAuthUsers.characters.dropFirst())
        }
        
        DELEGATE.dictNewSite.setValue(self.strAuthUsers, forKey: "authUsers")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: AuthorizeVC = storyboard.instantiateViewController(withIdentifier: "AuthorizeVC") as! AuthorizeVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }

    @IBAction func onClickEditViewCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalAdministratorsCell", for: indexPath) as! AdditionalAdministratorsCell
        
        let dict: NSDictionary = self.arrEmails[indexPath.row] as! NSDictionary
        cell.lblEmail.text = (dict["email"] as! String)
        
        let permission = dict["permission"] as! String
        if permission == "1" {
            cell.btnPermission.setTitle("View", for: .normal)
        }else if permission == "2" {
            cell.btnPermission.setTitle("Edit", for: .normal)
        }else {
            cell.btnPermission.setTitle("Edit & View", for: .normal)
        }
        
        return cell
    }
    
    // MARK: ContactsVC
    func getContactEmail(email: String) {
        txtEmail.text = email
        txtEmail.becomeFirstResponder()
        txtEmail.resignFirstResponder()
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

