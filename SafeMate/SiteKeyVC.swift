//
//  SiteKeyVC.swift
//

import UIKit

class SiteKeyVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var dictPrev = NSDictionary()
    
    // MARK: - UI Controls
    @IBOutlet var txtSiteKey: ACFloatingTextfield!
    @IBOutlet var btnCheckIn: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var viewProfile: UIView!
    @IBOutlet var btnCall: UIButton!
    @IBOutlet var lblSupervisorName: UILabel!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        lblSupervisorName.text = (dictPrev["siteSupervisorName"] as! String)
        
        viewProfile.layer.cornerRadius = viewProfile.frame.size.width / 2.0
        viewProfile.layer.borderColor = UIColor.white.cgColor
        viewProfile.layer.borderWidth = 1.5
        
        btnCheckIn.layer.cornerRadius = DELEGATE.setCorner()
        btnCheckIn.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnCheckIn.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    func checkValidateSiteKey(strSiteKey: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, Validate_Site_Key)

        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "accessCode": (DELEGATE.dictCheckIntoSite.value(forKey: "accessCode") as! String),
            "siteKey": strSiteKey
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageValidateSiteKeyData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageValidateSiteKeyData(dictData: NSDictionary) {
        print("dictData: \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let dictSiteDetail = dictData["siteDetail"] as! NSDictionary
            
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            let objVC: FillYourDetailsVC = storyboard.instantiateViewController(withIdentifier: "FillYourDetailsVC") as! FillYourDetailsVC
            objVC.dictPrev = dictSiteDetail
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCall(sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "tel://\(dictPrev["siteContactNumber"] as! String)")! as URL)
    }
    
    @IBAction func onClickCheckIn(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtSiteKey.text = txtSiteKey.text?.trimmingCharacters(in: .whitespaces)
        if txtSiteKey.text == "" {
            DELEGATE.showPopup(title: Empty_Site_Key, type: .error)
            return
        }
        
        self.checkValidateSiteKey(strSiteKey: txtSiteKey.text!)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtSiteKey) {
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
        }else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

