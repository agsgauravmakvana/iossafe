//
//  Team.swift
//

import UIKit

class Team: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

