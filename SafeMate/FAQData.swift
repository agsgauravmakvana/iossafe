//
//  FAQData.swift
//

import Foundation

class FAQData {
    var faqQuestion: String?
    var faqAnswer: String?
    var faqShown = false
    
    init(question: String, answer: String) {
        self.faqQuestion = question
        self.faqAnswer = answer
    }
}
