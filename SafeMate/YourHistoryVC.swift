//
//  YourHistoryVC.swift
//

import UIKit

class YourHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Variables
    var dictPrev: NSDictionary!
    
    // MARK: - UI Controls
    @IBOutlet var viewToday: UIView!
    @IBOutlet var viewWeek: UIView!
    @IBOutlet var viewMonth: UIView!
    @IBOutlet var viewAll: UIView!
    @IBOutlet var viewProfile: UIView!
    
    @IBOutlet var btnToday: UIButton!
    @IBOutlet var btnWeek: UIButton!
    @IBOutlet var btnMonth: UIButton!
    @IBOutlet var btnAll: UIButton!
    
    @IBOutlet var tblHistory: UITableView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        print("dictPrev: \(dictPrev)")
        
        viewProfile.layer.cornerRadius = viewProfile.frame.size.width / 2.0
        viewProfile.layer.borderColor = UIColor.white.cgColor
        viewProfile.layer.borderWidth = 1.5
        
        self.setDefault()
        btnToday.setTitleColor(UIColor.white, for: .normal)
        viewToday.isHidden = false
    }
    
    // MARK: - UI Methods
    func setDefault() {
        viewToday.isHidden = true
        viewWeek.isHidden = true
        viewMonth.isHidden = true
        viewAll.isHidden = true
        
        btnToday.setTitleColor(UIColor(red: 141.0/255.0, green: 221.0/255.0, blue: 234.0/255.0, alpha: 1.0), for: .normal)
        btnWeek.setTitleColor(UIColor(red: 141.0/255.0, green: 221.0/255.0, blue: 234.0/255.0, alpha: 1.0), for: .normal)
        btnMonth.setTitleColor(UIColor(red: 141.0/255.0, green: 221.0/255.0, blue: 234.0/255.0, alpha: 1.0), for: .normal)
        btnAll.setTitleColor(UIColor(red: 141.0/255.0, green: 221.0/255.0, blue: 234.0/255.0, alpha: 1.0), for: .normal)
    }
    
    func getHistory() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@", BASE_URL)
        let parameters: [AnyHashable: Any] = [
            "key": "value"
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageHistoryData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageHistoryData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickHistory(sender: UIButton) {
        let button: UIButton! = sender
        
        self.setDefault()
        button.setTitleColor(UIColor.white, for: .normal)
        
        if (button == btnToday) {
            viewToday.isHidden = false
        }else if (button == btnWeek) {
            viewWeek.isHidden = false
        }else if (button == btnMonth) {
            viewMonth.isHidden = false
        }else {
            viewAll.isHidden = false
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YourHistoryCell", for: indexPath) as! YourHistoryCell
        return cell
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

