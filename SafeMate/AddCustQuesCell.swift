//
//  MySitesCell.swift
//  

import UIKit

protocol AddCustQuesCellProtocol : class {
    func getQuestionDetails(index: NSInteger)
}

class AddCustQuesCell: UITableViewCell {
   
    // MARK: - Variables
    var delegate : AddCustQuesCellProtocol?
    var idx: NSInteger = 0
    
    // MARK: - UIControls
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblYesNo: UILabel!
    @IBOutlet var btnDetails: UIButton!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickDetails(_ sender: Any) {
        delegate?.getQuestionDetails(index: idx)
    }
    
}
