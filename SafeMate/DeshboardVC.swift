//
//  DeshboardVC.swift
//

import UIKit

class DeshboardVC: UIViewController {
    
    // MARK: - Variables

    // MARK: - UI Controls
    @IBOutlet var viewCheckSite: UIView!
    @IBOutlet var viewCreateSite: UIView!
    @IBOutlet var viewMyInfo: UIView!
    @IBOutlet var viewHelp: UIView!
    
    @IBOutlet var viewSepCheckSite: UIView!
    @IBOutlet var viewSepCreateSite: UIView!
    @IBOutlet var viewSepMyInfo: UIView!
    @IBOutlet var viewSepHelp: UIView!
    
    @IBOutlet var btnMenu: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnMenu.tag = 0
        btnMenu.setImage(UIImage(named: "add_small.png"), for: .normal)
        
        self.hideMenu()
    }
    
    // MARK: - UI Methods
    func showMenu() {
        viewCheckSite.isHidden = false
        viewCreateSite.isHidden = false
        viewMyInfo.isHidden = false
        viewHelp.isHidden = false
        
        viewSepCheckSite.isHidden = false
        viewSepCreateSite.isHidden = false
        viewSepMyInfo.isHidden = false
        viewSepHelp.isHidden = false
    }
    
    func hideMenu() {
        viewCheckSite.isHidden = true
        viewCreateSite.isHidden = true
        viewMyInfo.isHidden = true
        viewHelp.isHidden = true
        
        viewSepCheckSite.isHidden = true
        viewSepCreateSite.isHidden = true
        viewSepMyInfo.isHidden = true
        viewSepHelp.isHidden = true
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickMenu(_ sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setImage(UIImage(named: "close_small.png"), for: .normal)
            self.showMenu()
        }else {
            sender.tag = 0
            sender.setImage(UIImage(named: "add_small.png"), for: .normal)
            self.hideMenu()
        }
    }
    
    @IBAction func onClickCheckIntoSite(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objSiteAccessCodeVC: SiteAccessCodeVC = storyboard.instantiateViewController(withIdentifier: "SiteAccessCodeVC") as! SiteAccessCodeVC
        self.navigationController?.pushViewController(objSiteAccessCodeVC, animated: true)
    }
    
    @IBAction func onClickCreateSite(_ sender: UIButton) {
        let strUserId = (UserDefaults.standard.value(forKey: "UserID")) as! String
        
        DELEGATE.dictNewSite.setValue(strUserId, forKey: "userID")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objAddNewSiteVC: AddNewSiteVC = storyboard.instantiateViewController(withIdentifier: "AddNewSiteVC") as! AddNewSiteVC
        self.navigationController?.pushViewController(objAddNewSiteVC, animated: true)
    }
    
    @IBAction func onClickMyInfo(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: YourInformationVC = storyboard.instantiateViewController(withIdentifier: "YourInformationVC") as! YourInformationVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickHelp(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: FAQVC = storyboard.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

