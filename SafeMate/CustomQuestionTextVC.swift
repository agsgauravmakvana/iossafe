//
//  CustomQuestionTextVC.swift
//

import UIKit

protocol CustomQuestionTextVCProtocol : class {
    func reloadCustomQuestionTextList()
}

class CustomQuestionTextVC: UIViewController {
    
    // MARK: - Variables
    var strInputRequired: String = ""
    var strAllowMultiline: String = ""
    var strMinCharacters: String = ""
    var strMaxCharacters: String = ""
    var strInputType: String = ""
    weak var delegate : CustomQuestionTextVCProtocol?
    var dictPrev: NSDictionary!
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var txtQuestion: ACFloatingTextfield!
    @IBOutlet var txtHelpText: ACFloatingTextfield!
    @IBOutlet var txtMinCharacters: ACFloatingTextfield!
    @IBOutlet var txtMaxCharacters: ACFloatingTextfield!
    
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSave: UIButton!
    
    @IBOutlet var btnInputRequired: UIButton!
    
    @IBOutlet var btnSingleLine: UIButton!
    @IBOutlet var btnMultiLine: UIButton!
    
    @IBOutlet var btnAnyCharacters: UIButton!
    @IBOutlet var btnNumbersOnly: UIButton!
    @IBOutlet var btnTextNumbers: UIButton!
    @IBOutlet var btnTextOnly: UIButton!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        btnCancel.layer.cornerRadius = DELEGATE.setCorner()
        btnCancel.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnCancel.layer.borderWidth = 1.0
        
        btnSave.layer.cornerRadius = DELEGATE.setCorner()
        btnSave.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnSave.layer.borderWidth = 1.0
        
        strInputRequired = "1"
        btnInputRequired.tag = 0
        btnInputRequired.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        strMinCharacters = "0"
        strMaxCharacters = "0"
        
        strAllowMultiline = "1"
        self.onClickLineRadio(sender: btnSingleLine)
        
        strInputType = "1"
        self.onClickTextTypeRadio(sender: btnAnyCharacters)
    }
    
    // MARK: - UI Methods
    func addQuestion() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, ADD_QUESTION)
        
        let parameters: [AnyHashable: Any] = [
            "fieldType" : "5",
            "siteID" : (dictPrev.value(forKey: "siteID") as! String),
            "question" : txtQuestion.text!,
            "helpText" : txtHelpText.text!,
            "isRequired" : strInputRequired,
            "isAllowMultiline" : strAllowMultiline,
            "minChar" : strMinCharacters,
            "maxChar" : strMaxCharacters,
            "inputType" : strInputType,
            "options" : ""
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageAddQuestionData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageAddQuestionData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let dict = dictData["questionDetail"] as! NSDictionary
            DELEGATE.arrQuestions.add(dict)
            
            delegate?.reloadCustomQuestionTextList()
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCancel(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSave(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtQuestion.text = txtQuestion.text?.trimmingCharacters(in: .whitespaces)
        if txtQuestion.text == "" {
            DELEGATE.showPopup(title: Empty_Question, type: .error)
            return
        }
        
        self.addQuestion()
    }
    
    @IBAction func onClickInputRequiredCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
            
            strInputRequired = "0"
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
            
            strInputRequired = "1"
        }
    }
    
    @IBAction func onClickLineRadio(sender: UIButton) {
        btnSingleLine.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        btnMultiLine.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        
        sender.setBackgroundImage(UIImage(named: "radio_selected.png"), for: .normal)
        
        if (sender == btnSingleLine) {
            strAllowMultiline = "1"
        }else {
            strAllowMultiline = "0"
        }
    }
    
    @IBAction func onClickTextTypeRadio(sender: UIButton) {
        btnAnyCharacters.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        btnNumbersOnly.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        btnTextNumbers.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        btnTextOnly.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
        
        sender.setBackgroundImage(UIImage(named: "radio_selected.png"), for: .normal)
        
        if (sender == btnAnyCharacters) {
            strInputType = "1"
        }else if (sender == btnTextNumbers) {
            strInputType = "2"
        }else if (sender == btnNumbersOnly) {
            strInputType = "3"
        }else {
            strInputType = "4"
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtMinCharacters || textField == txtMaxCharacters) {
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                return false
            }
            return true
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == txtMinCharacters) {
            txtMinCharacters.text = txtMinCharacters.text?.trimmingCharacters(in: .whitespaces)
            if txtMinCharacters.text == "" {
                strMinCharacters = "0"
            }else {
                strMinCharacters = txtMinCharacters.text!
            }
        }
        
        if (textField == txtMaxCharacters) {
            txtMaxCharacters.text = txtMaxCharacters.text?.trimmingCharacters(in: .whitespaces)
            if txtMinCharacters.text == "" {
                strMaxCharacters = "0"
            }else {
                strMaxCharacters = txtMaxCharacters.text!
            }
        }
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

