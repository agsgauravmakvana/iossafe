//
//  PendingUsersCell.swift
//  

import UIKit

class PendingUsersCell: UITableViewCell {
   
    // MARK: - Variables
    
    // MARK: - UIControls
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblSiteName: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var btnPendingCheckbox: UIButton!
    @IBOutlet weak var imgShadow: UIImageView!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    
}
