//
//  ExistingSiteCell.swift
//  

import UIKit

protocol ExistingSiteCellProtocol : class {
    func getExistingSiteDetails(index: NSInteger)
}

class ExistingSiteCell: UITableViewCell {
   
    // MARK: - Variables
    var delegate : ExistingSiteCellProtocol?
    var idx: NSInteger = 0
    
    // MARK: - UIControls
    @IBOutlet var ViewReport: UIView!
    
    @IBOutlet weak var lblFirstAndLastName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblRoleName: UILabel!
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickDetails(_ sender: Any) {
        delegate?.getExistingSiteDetails(index: idx)
    }
    
}
