//
//  AddNewSiteVC.swift
//

import UIKit

class AddNewSiteVC: UIViewController {
    
    // MARK: - Variables
    var strPeoples: String = ""
    
    // MARK: - UI Controls
    @IBOutlet var btnCreateNew: UIButton!
    @IBOutlet var btnUpgradeSite: UIButton!
    
    @IBOutlet var btn1Site: UIButton!
    @IBOutlet var btn2Site: UIButton!
    @IBOutlet var btn3Site: UIButton!
    @IBOutlet var btn5Site: UIButton!
    @IBOutlet var btn10Site: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCreateNew.layer.cornerRadius = DELEGATE.setCorner()
        btnCreateNew.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnCreateNew.layer.borderWidth = 1.0
        
        btnUpgradeSite.layer.cornerRadius = DELEGATE.setCorner()
        btnUpgradeSite.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnUpgradeSite.layer.borderWidth = 1.0
        
        self.onClickSiteCheckbox(sender: btn1Site)
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCreateNew(sender: UIButton) {
        DELEGATE.dictNewSite.setValue(strPeoples, forKey: "peoples")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: StartNewCopySiteVC = storyboard.instantiateViewController(withIdentifier: "StartNewCopySiteVC") as! StartNewCopySiteVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickUpgradeSite(sender: UIButton) {
        DELEGATE.dictNewSite.setValue(strPeoples, forKey: "peoples")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SelectPaymentVC = storyboard.instantiateViewController(withIdentifier: "SelectPaymentVC") as! SelectPaymentVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickSiteCheckbox(sender: UIButton) {
        btn1Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        btn2Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        btn3Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        btn5Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        btn10Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
        
        if (sender == btn1Site) {
            strPeoples = "5"
        }else if (sender == btn2Site) {
            strPeoples = "10"
        }else if (sender == btn3Site) {
            strPeoples = "20"
        }else if (sender == btn5Site) {
            strPeoples = "50"
        }else {
            strPeoples = "100"
        }
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

