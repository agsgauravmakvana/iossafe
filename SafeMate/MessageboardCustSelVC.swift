//
//  MessageboardCustSelVC.swift
//

import UIKit

class MessageboardCustSelVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var strAgreement: String = ""
    var strName: String = ""
    var strOrganization: String = ""
    var strGroup: String = ""
    
    var countCheckbox: NSInteger = 0
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnAgreement: UIButton!
    @IBOutlet var btnName: UIButton!
    @IBOutlet var btnOrganization: UIButton!
    @IBOutlet var btnGroup: UIButton!
    @IBOutlet var txtMessageBoard: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
        
        strAgreement = "1"
        btnAgreement.tag = 0
        btnAgreement.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        strName = "1"
        btnName.tag = 0
        btnName.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        strOrganization = "1"
        btnOrganization.tag = 0
        btnOrganization.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        strGroup = "1"
        btnGroup.tag = 0
        btnGroup.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
    }
    
    // MARK: - UI Methods
    func createNewSite() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, CREATE_NEW_SITE)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "refSiteID" : (DELEGATE.dictNewSite.value(forKey: "refSiteID") as! String),
            "isFileCopy" : (DELEGATE.dictNewSite.value(forKey: "isFileCopy") as! String),
            "isFieldCopy" : (DELEGATE.dictNewSite.value(forKey: "isFieldCopy") as! String),
            "siteName" : (DELEGATE.dictNewSite.value(forKey: "siteName") as! String),
            "siteAddress" : (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String),
            "sitegeoFenceRange" : (DELEGATE.dictNewSite.value(forKey: "sitegeoFenceRange") as! String),
            "siteLatitude" : (DELEGATE.dictNewSite.value(forKey: "siteLatitude") as! String),
            "siteLogitude" : (DELEGATE.dictNewSite.value(forKey: "siteLogitude") as! String),
            "siteSupervisorName" : (DELEGATE.dictNewSite.value(forKey: "siteSupervisorName") as! String),
            "companyID" : (DELEGATE.dictNewSite.value(forKey: "companyID") as! String),
            "companyName" : (DELEGATE.dictNewSite.value(forKey: "companyName") as! String),
            "siteContactNumber" : (DELEGATE.dictNewSite.value(forKey: "siteContactNumber") as! String),
            "siteEmail" : (DELEGATE.dictNewSite.value(forKey: "siteEmail") as! String),
            "authUsers" : (DELEGATE.dictNewSite.value(forKey: "authUsers") as! String),
            "allowAutoApprove" : (DELEGATE.dictNewSite.value(forKey: "allowAutoApprove") as! String),
            "notifyEmail" : (DELEGATE.dictNewSite.value(forKey: "notifyEmail") as! String),
            "emailOnEntry" : (DELEGATE.dictNewSite.value(forKey: "emailOnEntry") as! String),
            "emailOnExit" : (DELEGATE.dictNewSite.value(forKey: "emailOnExit") as! String),
            "welcomeMessage" : (DELEGATE.dictNewSite.value(forKey: "welcomeMessage") as! String),
            "isRequireAgreement" : (DELEGATE.dictNewSite.value(forKey: "isRequireAgreement") as! String),
            "isRequireName" : (DELEGATE.dictNewSite.value(forKey: "isRequireName") as! String),
            "isRequireOrganization" : (DELEGATE.dictNewSite.value(forKey: "isRequireOrganization") as! String),
            "isRequireGroup" : (DELEGATE.dictNewSite.value(forKey: "isRequireGroup") as! String),
            "startTime" : (DELEGATE.dictNewSite.value(forKey: "startTime") as! String),
            "endTime" : (DELEGATE.dictNewSite.value(forKey: "endTime") as! String)
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageCreateNewSiteData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageCreateNewSiteData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "success" {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objVC: AddCustQuesVC = storyboard.instantiateViewController(withIdentifier: "AddCustQuesVC") as! AddCustQuesVC
                objVC.dictPrev = dictData["siteDetail"] as! NSDictionary
                self.navigationController?.pushViewController(objVC, animated: true)
            }else {
                
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtMessageBoard.text = txtMessageBoard.text?.trimmingCharacters(in: .whitespaces)
        
        DELEGATE.dictNewSite.setValue(txtMessageBoard.text!, forKey: "welcomeMessage")
        DELEGATE.dictNewSite.setValue(strAgreement, forKey: "isRequireAgreement")
        DELEGATE.dictNewSite.setValue(strName, forKey: "isRequireName")
        DELEGATE.dictNewSite.setValue(strOrganization, forKey: "isRequireOrganization")
        DELEGATE.dictNewSite.setValue(strGroup, forKey: "isRequireGroup")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        self.createNewSite()
    }
    
    @IBAction func onClickCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        
        if (sender == btnAgreement) {
            if (sender.tag == 0) {
                strAgreement = "1"
            }else {
                strAgreement = "0"
            }
        }else if (sender == btnName) {
            if (sender.tag == 0) {
                strName = "1"
            }else {
                strName = "0"
            }
        }else if (sender == btnOrganization) {
            if (sender.tag == 0) {
                strOrganization = "1"
            }else {
                strOrganization = "0"
            }
        }else {
            if (sender.tag == 0) {
                strGroup = "1"
            }else {      
                strGroup = "0"
            }
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

