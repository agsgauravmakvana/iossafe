//
//  BillingVC.swift
//

import UIKit

class BillingVC: UIViewController {
    
    // MARK: - Variables
    var countCheckbox: NSInteger = 0
    
    // MARK: - UI Controls
    @IBOutlet var btnPauseBilling: UIButton!
    @IBOutlet var btnCancelAccount: UIButton!
    
    @IBOutlet var btn1Site: UIButton!
    @IBOutlet var btn2Site: UIButton!
    @IBOutlet var btn3Site: UIButton!
    @IBOutlet var btn5Site: UIButton!
    @IBOutlet var btn10Site: UIButton!
    
    @IBOutlet var viewProfile: UIView!
    @IBOutlet var scrollBilling: UIScrollView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewProfile.layer.cornerRadius = viewProfile.frame.width / 2.0
        viewProfile.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        viewProfile.layer.borderWidth = 1.5
        
        btnPauseBilling.layer.cornerRadius = DELEGATE.setCorner()
        btnPauseBilling.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnPauseBilling.layer.borderWidth = 1.0
        
        btnCancelAccount.layer.cornerRadius = DELEGATE.setCorner()
        btnCancelAccount.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnCancelAccount.layer.borderWidth = 1.0
        
        btn1Site.tag = 0
        btn1Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btn2Site.tag = 0
        btn2Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btn3Site.tag = 0
        btn3Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btn5Site.tag = 0
        btn5Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btn10Site.tag = 0
        btn10Site.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            scrollBilling.contentSize = CGSize(width: self.view.frame.size.width, height: 1105.0)
        }else {
            scrollBilling.contentSize = CGSize(width: self.view.frame.size.width, height: 640.0)
        }
    }
    
    // MARK: - UI Methods
    func getBillingData() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@", BASE_URL)
        let parameters: [AnyHashable: Any] = [
            "key": "value",
            "key": "value"
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageBillingData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageBillingData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
    }

    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickPauseBilling(sender: UIButton) {
        
    }
    
    @IBAction func onClickCancelAccount(sender: UIButton) {
        if countCheckbox == 1 {
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            let objVC: RemoveVC = storyboard.instantiateViewController(withIdentifier: "RemoveVC") as! RemoveVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }else {
            DELEGATE.showPopup(title: Select_Category, type: .error)
        }
    }
    
    @IBAction func onClickBillingCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
            countCheckbox += 1
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
            countCheckbox -= 1
        }
       
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

