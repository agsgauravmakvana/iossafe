//
//  MyTeamCell.swift
//  

import UIKit

protocol MyTeamCellProtocol : class {
    func getTeamDetails(index: NSInteger)
}

class MyTeamCell: UITableViewCell {
   
    // MARK: - Variables
    var delegate : MyTeamCellProtocol?
    var idx: NSInteger = 0
    
    // MARK: - UIControls
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var btnTeamCheckbox: UIButton!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblTeamAddress: UILabel!
    @IBOutlet weak var lblTeamPosition: UILabel!
    @IBOutlet weak var lblTeamSIteName: UILabel!
    @IBOutlet weak var imgbackground: UIImageView!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickDetails(_ sender: Any) {
        delegate?.getTeamDetails(index: idx)
    }
    
}
