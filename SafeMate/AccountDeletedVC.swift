//
//  AccountDeletedVC.swift
//

import UIKit

class AccountDeletedVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnHome: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnHome.layer.cornerRadius = DELEGATE.setCorner()
        btnHome.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnHome.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickHome(sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isLogin")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objSafeMate: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
        self.navigationController?.pushViewController(objSafeMate, animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

