//
//  SignUp.swift
//

import UIKit

class SignUp: UIViewController, UITextFieldDelegate, LBZSpinnerDelegate, SearchLocationCellProtocol, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    // MARK: - Variables
    var strRoleID: String = ""
    var strRoleName: String = ""
    
    var strCompanyID: String = ""
    var strCompanyName: String = ""
    var strSearchCompany: String = ""
    
    var arrCompany: Array<Dictionary<String,String>> = []
    var arrSearchCompany: Array<Dictionary<String,String>> = []
    
    var arrRole: Array<Dictionary<String,String>> = []
    var arrRoleName: Array<String> = []
    
    // MARK: - UI Controls
    @IBOutlet var btnTerms: UIButton!
    @IBOutlet var btnCreateAccount: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var txtFirstName: ACFloatingTextfield!
    @IBOutlet var txtLastName: ACFloatingTextfield!
    @IBOutlet var txtEmail: ACFloatingTextfield!
    @IBOutlet var txtPassword: ACFloatingTextfield!
    @IBOutlet var txtContact: ACFloatingTextfield!
    @IBOutlet var txtCompanyName: ACFloatingTextfield!
    @IBOutlet var chooseRole: LBZSpinner!
    
    @IBOutlet var tblCompany: UITableView!
    @IBOutlet var viewCompany: UIView!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCreateAccount.layer.cornerRadius = DELEGATE.setCorner()
        btnCreateAccount.layer.borderColor = UIColor.white.cgColor
        btnCreateAccount.layer.borderWidth = 1.0
        
        self.txtContact.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.txtCompanyName.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        
        self.viewCompany.layer.shadowOpacity = 0.3;
        self.viewCompany.layer.shadowOffset = CGSize(width: 1, height: 1);
        self.viewCompany.layer.shadowRadius = 5;
        self.viewCompany.layer.cornerRadius = 0
        self.viewCompany.layer.masksToBounds = false
        self.viewCompany.clipsToBounds = false
        self.viewCompany.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture))
        tapGesture.delegate = self
        self.viewCompany.addGestureRecognizer(tapGesture)
        
        self.arrRoleName.insert("Choose Role", at: 0)
        self.setPopup()
        self.getRoles()
    }
    
    // MARK: - UI Methods
    func handleTapGesture() {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewCompany.isHidden = true
    }
    
    func textFieldDidChange() {
        if (txtTemp == txtContact) {
            let newString: String = txtContact.text! as String
            let updatedString: String = PartialFormatter().formatPartial(newString)
            txtContact.text = updatedString
            
            txtContact.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: UIColor.clear])
        }else if (txtTemp == txtCompanyName) {
            strSearchCompany = self.txtCompanyName.text!
        }else {
            
        }
    }
    
    func setPopup() {
        chooseRole.updateList(self.arrRoleName)
        chooseRole.delegate = self
         
        let font: UIFont = UIFont(name: txtFirstName.font!.fontName, size: txtFirstName.font!.pointSize)!
        chooseRole.dDLTextFont = font
         
        if chooseRole.selectedIndex == LBZSpinner.INDEX_NOTHING {
            chooseRole.changeSelectedIndex(0)
        }
    }
    
    func signUp(fullName: String, lastName: String, email: String, password: String, contactNumber: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, REGISTER_USER)
        let parameters: [AnyHashable: Any] = [
            "firstName": fullName,
            "lastName": lastName,
            "email": email,
            "password": password,
            "contactNumber": contactNumber,
            "companyID": strCompanyID,
            "companyName": strCompanyName,
            "roleID": strRoleID,
            "deviceType": DEVICE_TYPE,
            "deviceToken": UserDefaults.standard.value(forKey: "DeviceToken")!
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSignUpData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageSignUpData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                DELEGATE.dictUserDetails = dictData["userDetail"] as! NSDictionary
//                print("DELEGATE.dictUserDetails: \(DELEGATE.dictUserDetails)")
                
                UserDefaults.standard.setValue(DELEGATE.dictUserDetails, forKey: "UserDetails")
//                print("UserDetails: \(String(describing: UserDefaults.standard.value(forKey: "UserDetails")))")
                
                DELEGATE.strUserID = DELEGATE.dictUserDetails["userID"] as! String
//                print("DELEGATE.strUserID: \(DELEGATE.strUserID)")
                
                UserDefaults.standard.setValue(DELEGATE.strUserID, forKey: "UserID")
//                print("UserID: \(String(describing: UserDefaults.standard.value(forKey: "UserID")))")
                
                let isNewUser = DELEGATE.dictUserDetails["isNewUser"] as! String
                if (isNewUser == "1") {
                    let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                    let objVC: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
                    self.navigationController?.pushViewController(objVC, animated: true)
                }else {
                    let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                    let objVC: DeshboardVC = storyboard.instantiateViewController(withIdentifier: "DeshboardVC") as! DeshboardVC
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
            }else {
                
            }
        }
    }
    
    func getRoles() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
            
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_ROLES)
        
        manager.post(url, parameters: nil, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageRolesData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
            
            self.getCompany()
        })
    }
    
    func manageRolesData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                self.arrRole = dictData["roleList"] as! Array<Dictionary<String, String>>
                print("arrRole: \(self.arrRole)")
                
                for index in 0...self.arrRole.count - 1 {
                    var dict = self.arrRole[index]
                    self.arrRoleName.insert(dict["roleName"]! as String, at: index + 1)
                }
                chooseRole.updateList(self.arrRoleName)
            }else {
                
            }
        }
        
        self.getCompany()
    }
    
    func getCompany() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_COMPANY)
        
        manager.post(url, parameters: nil, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageCompanyData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageCompanyData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                self.arrCompany = dictData["companyList"] as! Array<Dictionary<String, String>>
                print("arrCompany: \(self.arrCompany)")
            }else {
                
            }
        }
    }
    
    func checkCompanyIDName(name: String) {
        print("name: \(name)")
        
        if name == "" {
            strCompanyID = ""
            strCompanyName = txtCompanyName.text!
            print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
            
            return
        }
        
        if self.arrCompany.count > 0 {
            for index in 0...self.arrCompany.count - 1 {
                var dict: Dictionary<String,String> = self.arrCompany[index]
                
                if (dict["companyName"]! as String) == name {
                    strCompanyID = dict["companyID"]! as String
                    strCompanyName = dict["companyName"]! as String
                    print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
                    
                    break
                }else {
                    strCompanyID = ""
                    strCompanyName = txtCompanyName.text!
                    print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
                    
                    continue
                }
            }
        }else {
            strCompanyID = ""
            strCompanyName = txtCompanyName.text!
            print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickTerms(sender: UIButton) {
        
    }
    
    @IBAction func onClickCreateAccount(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtFirstName.text = txtFirstName.text?.trimmingCharacters(in: .whitespaces)
        if txtFirstName.text == "" {
            DELEGATE.showPopup(title: Empty_First_Name, type: .error)
            return
        }else {
            if (txtFirstName.text?.characters.count)! <= 2 {
                DELEGATE.showPopup(title: Mini_Characters_First_Name, type: .error)
                return
            }
            
            if DELEGATE.isValidCharecter(strchare: txtFirstName.text!) == false {
                DELEGATE.showPopup(title: Only_Characters_First_Name, type: .error)
                return
            }
        }
        
        txtLastName.text = txtLastName.text?.trimmingCharacters(in: .whitespaces)
        if txtLastName.text == "" {
            DELEGATE.showPopup(title: Empty_Last_Name, type: .error)
            return
        }else {
            if DELEGATE.isValidCharecter(strchare: txtLastName.text!) == false {
                DELEGATE.showPopup(title: Only_Characters_Last_Name, type: .error)
                return
            }
            
            if (txtLastName.text?.characters.count)! <= 2 {
                DELEGATE.showPopup(title: Mini_Characters_Last_Name, type: .error)
                return
            }
        }
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }
        
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: .whitespaces)
        if txtPassword.text == "" {
            DELEGATE.showPopup(title: Empty_Password, type: .error)
            return
        }else {
            if (txtPassword.text?.characters.count)! >= 6 {
                if DELEGATE.isValidPassword(strPassword: txtPassword.text!) == false {
                    DELEGATE.showPopup(title: Valid_Password, type: .error)
                    return
                }
            }else {
                DELEGATE.showPopup(title: Mini_Characters_Password, type: .error)
                return
            }            
        }
        
        txtContact.text = txtContact.text?.trimmingCharacters(in: .whitespaces)
        if txtContact.text == "" {
            DELEGATE.showPopup(title: Empty_Contact_Number, type: .error)
            return
        }else {
            if (txtContact.text?.characters.count)! <= 9 {
                DELEGATE.showPopup(title: Mini_Characters_Contact, type: .error)
                return
            }
        }
        
        txtCompanyName.text = txtCompanyName.text?.trimmingCharacters(in: .whitespaces)
        if txtCompanyName.text == "" {
            DELEGATE.showPopup(title: Empty_Company_Name, type: .error)
            return
        }
        
        if strRoleName == "Choose Role" {
            DELEGATE.showPopup(title: Empty_Role, type: .error)
            return
        }
        
        self.signUp(fullName: txtFirstName.text!, lastName: txtLastName.text!, email: txtEmail.text!, password: txtPassword.text!, contactNumber: txtContact.text!)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtContact {
            let invalidCharacters = CharacterSet(charactersIn: "+0123456789").inverted
            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                return false
            }
            return true
        }else if textField == txtPassword {
            let currentString: NSString = txtPassword.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= Constant.maxLength
        }else if textField == txtFirstName {
            let currentString: NSString = txtFirstName.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= Constant.maxLength
        }else if textField == txtLastName {
            let currentString: NSString = txtLastName.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= Constant.maxLength
        }else if textField == txtCompanyName {
            if string.isEmpty {
                strSearchCompany = String(strSearchCompany.characters.dropLast())
             }else {
                strSearchCompany = textField.text!+string
             }
             
             let predicate = NSPredicate(format: "SELF.companyName CONTAINS[cd] %@", strSearchCompany)
             let array = (arrCompany as NSArray).filtered(using: predicate)
             
             if array.count > 0 {
                self.arrSearchCompany.removeAll(keepingCapacity: true)
                self.arrSearchCompany = array as! Array<Dictionary<String,String>>
                self.viewCompany.isHidden = false
             }else {
                self.arrSearchCompany.removeAll()
                self.viewCompany.isHidden = true
             }
             
             self.tblCompany.reloadData()
            return true
        }else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        txtCompanyName.text = txtCompanyName.text?.trimmingCharacters(in: .whitespaces)
        self.checkCompanyIDName(name: txtCompanyName.text!)
    }
    
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearchCompany.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell", for: indexPath) as! SearchLocationCell
        cell.delegate = self
        cell.idx = indexPath.row
        
        var dict: Dictionary<String,String> = self.arrSearchCompany[indexPath.row] 
        cell.lblLocationName.text = NSString(format:"%@", dict["companyName"]!) as String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: SearchLocationCell
    func getLocationDetails(index: NSInteger) {
        var dict: Dictionary<String,String> = self.arrSearchCompany[index]
        txtCompanyName.text = NSString(format:"%@", dict["companyName"]!) as String
        
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewCompany.isHidden = true
    }
    
    // MARK: - LBZSpinner
    func spinnerOpen(_ spinner:LBZSpinner) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        print("Open")
    }
    
    func spinnerClose(_ spinner:LBZSpinner) {
        print("Close")
    }
    
    func spinnerChoose(_ spinner:LBZSpinner, index:Int, value:String) {
        if index == 0 {
            strRoleID = ""
        }else {
            var dict: Dictionary<String,String> = self.arrRole[index - 1]
            strRoleID = dict["roleID"]! as String
        }
        
        strRoleName = value
        print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

