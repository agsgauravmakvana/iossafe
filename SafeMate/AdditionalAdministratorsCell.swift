//
//  AdditionalAdministratorsCell.swift
//

import UIKit

class AdditionalAdministratorsCell: UITableViewCell {
    
    // MARK: - Variables
    
    // MARK: - UIControls
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var btnPermission: UIButton!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    
}
