//
//  SearchLocationVC.swift
//

import UIKit

protocol SearchLocationVCProtocol : class {
    func getLocationCoordinate(address: String)
}

class SearchLocationVC: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, SearchLocationCellProtocol {

    // MARK: - Variables
    weak var delegate : SearchLocationVCProtocol?
    fileprivate var dataTask: URLSessionDataTask?
    var strSearch: String = ""
    var arrLocations = [String]()
    
    // MARK - UIControls
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblLocation: UITableView!
    @IBOutlet weak var viewSearch: UIView!
    
    var txtTemp: UITextField!
    
    // MARK: - ViewController Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSearch.layer.cornerRadius = DELEGATE.setCorner()
        viewSearch.layer.borderColor = UIColor(red: 199.0/255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 0.5).cgColor
        viewSearch.layer.borderWidth = 1.0
        
        self.txtSearch.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
    }
    
    // MARK: - UI Methods
    func textFieldDidChange() {
        strSearch = txtSearch.text!
        
        if let dataTask = self.dataTask {
            dataTask.cancel()
        }
        self.fetchAutocompletePlaces(strSearch)
    }
    
    fileprivate func fetchAutocompletePlaces(_ keyword: String) {
        let urlString = "\(GOOGLE_AUTO_FILTER_BASE_URL)?key=\(GOOGLE_AUTO_FILTER_API_KEY)&input=\(keyword)"
        let s = (CharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
        s.addCharacters(in: "+&")
        
        if let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: s as CharacterSet) {
            if let url = URL(string: encodedString) {
                
                let request = URLRequest(url: url)
                dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                    if let data = data {
                        do {
                            let result = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                            if let status = result["status"] as? String {
                                if status == "OK" {
                                    if let predictions = result["predictions"] as? NSArray {
                                        var locations = [String]()
                                        for dict in predictions as! [NSDictionary] {
                                            locations.append(dict["description"] as! String)
                                        }
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            self.arrLocations = locations
                                            self.tblLocation.reloadData()
                                        })
                                        return
                                    }
                                }
                            }
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.arrLocations.removeAll()
                                self.tblLocation.reloadData()
                            })
                        }
                        catch let error as NSError {
                            print("Error: \(error.localizedDescription)")
                        }
                    }
                })
                dataTask?.resume()
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(_ sender: AnyObject) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        strSearch = txtTemp.text!
        
        if let dataTask = self.dataTask {
            dataTask.cancel()
        }
        self.fetchAutocompletePlaces(strSearch)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrLocations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell", for: indexPath) as! SearchLocationCell
        cell.delegate = self
        cell.idx = indexPath.row
        cell.lblLocationName.text = self.arrLocations[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: SearchLocationCell
    func getLocationDetails(index: NSInteger) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.delegate?.getLocationCoordinate(address: self.arrLocations[index])
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
