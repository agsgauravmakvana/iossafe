//
//  SearchLocationCell.swift
//

import UIKit

protocol SearchLocationCellProtocol : class {
    func getLocationDetails(index: NSInteger)
}

class SearchLocationCell: UITableViewCell {

    // MARK: - Variables
    weak var delegate : SearchLocationCellProtocol?
    var idx: NSInteger = 0
    
    // MARK - UIControls
    @IBOutlet weak var lblLocationName: UILabel!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickLocationDetails(_ sender: Any) {
        delegate?.getLocationDetails(index: idx)
    }

}
