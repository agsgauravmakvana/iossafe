//
//  ContactsCell.swift
//

import UIKit

protocol ContactsCellProtocol : class {
    func getContactDetails(index: NSInteger)
}

class ContactsCell: UITableViewCell {

    // MARK: - Variables
    weak var delegate : ContactsCellProtocol?
    var idx: NSInteger = 0
    
    // MARK - UIControls
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickContactDetails(_ sender: Any) {
        delegate?.getContactDetails(index: idx)
    }

}
