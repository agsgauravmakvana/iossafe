//
//  ContactsVC.swift
//

import UIKit

protocol ContactsVCProtocol : class {
    func getContactEmail(email: String)
}

class ContactsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ContactsCellProtocol {

    // MARK: - Variables
    weak var delegate : ContactsVCProtocol?
    
    // MARK - UIControls
    @IBOutlet weak var tblContacts: UITableView!

    // MARK: - ViewController Method
    override func viewDidLoad() {
        super.viewDidLoad()

        print("DELEGATE.arrContacts : \(DELEGATE.arrContacts)")
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DELEGATE.arrContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as! ContactsCell
        cell.delegate = self
        cell.idx = indexPath.row
        
        let dict: NSDictionary = DELEGATE.arrContacts[indexPath.row] as! NSDictionary
        cell.lblName.text = (dict["displayName"] as! String)
        cell.lblEmail.text = (dict["emailAddress"] as! String)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - ContactsCellProtocol
    func getContactDetails(index: NSInteger) {
        let dict: NSDictionary = DELEGATE.arrContacts[index] as! NSDictionary
        print("\(dict)")
        
        _ = self.navigationController?.popViewController(animated: true)
        delegate?.getContactEmail(email: (dict["emailAddress"] as! String))
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
