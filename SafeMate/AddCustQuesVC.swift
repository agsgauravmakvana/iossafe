//
//  AddCustQuesVC.swift
//

import UIKit

class AddCustQuesVC: UIViewController, LBZSpinnerDelegate, UITableViewDelegate, UITableViewDataSource, AddCustQuesCellProtocol, CustomQuestionFileVCProtocol, CustomQuestionTextVCProtocol, CustomQuestionVCProtocol, UIGestureRecognizerDelegate {
    
    // MARK: - Variables
    var strQueryType: String = ""
    var dictPrev: NSDictionary!
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var typeOfQuery: LBZSpinner!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var tblQuestions: UITableView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        DELEGATE.arrQuestions.removeAllObjects()
        self.tblQuestions.isHidden = true
        
        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
        
        btnDone.layer.cornerRadius = DELEGATE.setCorner()
        btnDone.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnDone.layer.borderWidth = 1.0
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.onLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delaysTouchesBegan = true
        longPressGesture.delegate = self
        self.tblQuestions.addGestureRecognizer(longPressGesture)
        
        self.setPopup()
    }
    
    // MARK: - UI Methods
    func onLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            
            let row = longPressGestureRecognizer.location(in: self.tblQuestions)
            let indexPath = self.tblQuestions.indexPathForRow(at: row)
            
            if let index = indexPath {
                _ = self.tblQuestions.cellForRow(at: index)

                let dictData = DELEGATE.arrQuestions[index.row] as! NSDictionary
                print("dictData: \(dictData)")

                let alert = UIAlertController(title: APP_NAME, message: Delete_Query, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {
                    (action:UIAlertAction!) -> Void in
                    self.removeQuestion(dictData: dictData, idxPath: index as NSIndexPath)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            }else {
                print("Could not find index path.")
            }
        }
    }
    
    func removeQuestion(dictData: NSDictionary, idxPath: NSIndexPath) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, REMOVE_QUESTION)
        let parameters: [AnyHashable: Any] = [
            "userID": (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "fieldTypeID": (dictData.value(forKey: "fieldTypeID") as! String),
            "siteID": (dictData.value(forKey: "siteID") as! String)
        ]

        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dict: NSDictionary = response as! NSDictionary
            let status: String = dict["status"] as! String
            let msg: String = dict["msg"] as! String
            
            if status == "0" {
                DELEGATE.showPopup(title: msg, type: .error)
            }else {
                DELEGATE.arrQuestions.removeObject(at: idxPath.row)
                
                if (DELEGATE.arrQuestions.count > 0) {
                    self.tblQuestions.isHidden = false
                }else {
                    self.tblQuestions.isHidden = true
                }
                
                self.tblQuestions.reloadData()
            }
            
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func setPopup() {
        let arrSiteToCopy = ["Select","Drop Down","Text","File"]
        
        typeOfQuery.updateList(arrSiteToCopy)
        typeOfQuery.delegate = self
        
        let font: UIFont!
        if DELEGATE.isIPad() {
            font = UIFont(name: "Roboto-Light", size: 20.0)!
        }else {
            font = UIFont(name: "Roboto-Light", size: 14.0)!
        }
        
        typeOfQuery.dDLTextFont = font
        
        if typeOfQuery.selectedIndex == LBZSpinner.INDEX_NOTHING {
            typeOfQuery.changeSelectedIndex(0)
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        // _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if strQueryType == "Select" {
            DELEGATE.showPopup(title: Empty_Query_Type, type: .error)
            return
        }else {
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            
            if strQueryType == "Drop Down" {
                let objVC: CustomQuestionVC = storyboard.instantiateViewController(withIdentifier: "CustomQuestionVC") as! CustomQuestionVC
                objVC.delegate = self
                objVC.dictPrev = dictPrev
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if strQueryType == "Text" {
                let objVC: CustomQuestionTextVC = storyboard.instantiateViewController(withIdentifier: "CustomQuestionTextVC") as! CustomQuestionTextVC
                objVC.delegate = self
                objVC.dictPrev = dictPrev
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else if strQueryType == "File" {
                let objVC: CustomQuestionFileVC = storyboard.instantiateViewController(withIdentifier: "CustomQuestionFileVC") as! CustomQuestionFileVC
                objVC.delegate = self
                objVC.dictPrev = dictPrev
                self.navigationController?.pushViewController(objVC, animated: true)
            }
            else {
                
            }
        }
    }
    
    @IBAction func onClickDone(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SuccessVC = storyboard.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
        objVC.dictPrev = dictPrev
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DELEGATE.arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCustQuesCell", for: indexPath) as! AddCustQuesCell
        cell.delegate = self
        cell.idx = indexPath.row
        
        let dict = DELEGATE.arrQuestions[indexPath.row] as! NSDictionary
        cell.lblQuestion.text = (dict["question"] as! String)
        
        let strFieldType = dict["fieldType"] as! String
        if strFieldType == "1" {
            cell.lblType.text = "Drop Down"
        }else if strFieldType == "2" {
            cell.lblType.text = "Checkbox"
        }else if strFieldType == "3" {
            cell.lblType.text = "Radio Button"
        }else if strFieldType == "4" {
            cell.lblType.text = "File"
        }else if strFieldType == "5" {
            cell.lblType.text = "Text"
        }else {
            cell.lblType.text = ""
        }
        
        let strInputRequired = dict["isRequired"] as! String
        if strInputRequired == "1" {
            cell.lblYesNo.text = "No"
        }else {
            cell.lblYesNo.text = "Yes"
        }
        
        return cell
    }
    
    // MARK: ContactsVC
    func getQuestionDetails(index: NSInteger) {
        print("index: \(index)")
    }
    
    // MARK: LBZSpinner
    func spinnerOpen(_ spinner:LBZSpinner) {
        print("Open")
    }
    
    func spinnerClose(_ spinner:LBZSpinner) {
        print("Close")
    }
    
    func spinnerChoose(_ spinner:LBZSpinner, index:Int, value:String) {
        strQueryType = value
        print("Index : \(index) Value : \(value)")
    }
    
    // MARK: CustomQuestionFileVCProtocol
    func reloadCustomQuestionFileList() {
        if (DELEGATE.arrQuestions.count > 0) {
            self.tblQuestions.isHidden = false
        }else {
            self.tblQuestions.isHidden = true
        }
        
        _ = self.navigationController?.popViewController(animated: true)
        self.tblQuestions.reloadData()
    }
    
    // MARK: CustomQuestionTextVCProtocol
    func reloadCustomQuestionTextList() {
        if (DELEGATE.arrQuestions.count > 0) {
            self.tblQuestions.isHidden = false
        }else {
            self.tblQuestions.isHidden = true
        }
        
        _ = self.navigationController?.popViewController(animated: true)
        self.tblQuestions.reloadData()
    }
    
    // MARK: CustomQuestionVCProtocol
    func reloadCustomQuestionList() {
        if (DELEGATE.arrQuestions.count > 0) {
            self.tblQuestions.isHidden = false
        }else {
            self.tblQuestions.isHidden = true
        }
        
        _ = self.navigationController?.popViewController(animated: true)
        self.tblQuestions.reloadData()
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

