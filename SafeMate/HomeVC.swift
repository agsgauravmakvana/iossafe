//
//  HomeVC.swift
//

import UIKit

class HomeVC: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, GMSMapViewDelegate, KYButtonDelegate, SearchLocationCellProtocol, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    // MARK: - Variables
    var locationManager = CLLocationManager()
    var bounds = GMSCoordinateBounds()
    
    var arrSites = [String]()
    fileprivate var dataTask: URLSessionDataTask?
    var isSearch: Bool = false
    var strSearch: String = ""
    
    // MARK: - UI Controls
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var viewMap: GMSMapView!
    @IBOutlet var button: KYButton!
    @IBOutlet var tblSites: UITableView!
    @IBOutlet var viewSites: UIView!
    
    var txtTemp: UITextField!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearch.attributedPlaceholder = NSAttributedString(string: "New Port Center", attributes: [NSForegroundColorAttributeName: UIColor.black])
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        viewMap.delegate = self
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude:48.857165, longitude:2.354613)
        marker.map = self.viewMap
        
        bounds = bounds.includingCoordinate(marker.position)

        self.txtSearch.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.viewSites.isHidden = true
        
        self.viewSites.layer.shadowOpacity = 0.3;
        self.viewSites.layer.shadowOffset = CGSize(width: 1, height: 1);
        self.viewSites.layer.shadowRadius = 5;
        self.viewSites.layer.cornerRadius = 0
        self.viewSites.layer.masksToBounds = false
        self.viewSites.clipsToBounds = false
        
        // For testing
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture))
//        tapGesture.delegate = self
//        self.view.addGestureRecognizer(tapGesture)
        
        self.setExpandingMenuButton()
    }
    
    // MARK: - UI Methods
//    func handleTapGesture() {
//        if txtTemp != nil {
//            txtTemp.resignFirstResponder()
//        }
//
//        self.viewSites.isHidden = true
//    }
    
    func textFieldDidChange() {
        strSearch = self.txtSearch.text!
        
        if let dataTask = self.dataTask {
            dataTask.cancel()
        }
        self.fetchAutocompletePlaces(strSearch)
    }
    
    fileprivate func fetchAutocompletePlaces(_ keyword: String) {
        let urlString = "\(GOOGLE_AUTO_FILTER_BASE_URL)?key=\(GOOGLE_AUTO_FILTER_API_KEY)&input=\(keyword)"
        let s = (CharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
        s.addCharacters(in: "+&")
        
        if let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: s as CharacterSet) {
            if let url = URL(string: encodedString) {
                
                let request = URLRequest(url: url)
                dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                    if let data = data {
                        do {
                            let result = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                            if let status = result["status"] as? String {
                                if status == "OK" {
                                    if let predictions = result["predictions"] as? NSArray {
                                        var locations = [String]()
                                        for dict in predictions as! [NSDictionary] {
                                            locations.append(dict["description"] as! String)
                                        }
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            self.arrSites = locations
                                            self.viewSites.isHidden = false
                                            self.tblSites.reloadData()
                                        })
                                        return
                                    }
                                }
                            }
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.arrSites.removeAll()
                                self.viewSites.isHidden = true
                                self.tblSites.reloadData()
                            })
                        }
                        catch let error as NSError {
                            print("Error: \(error.localizedDescription)")
                        }
                    }
                })
                dataTask?.resume()
            }
        }
    }
    
    fileprivate func setExpandingMenuButton() {
        button.kyDelegate = self
        button.openType = .popUp
        button.plusColor = UIColor.white
        button.fabTitleColor = UIColor.white
        
        button.add(color: UIColor.clear, title: "Check in to New Site", image: UIImage(named: "current_location.png")!) { (item) in
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            let objVC: SiteAccessCodeVC = storyboard.instantiateViewController(withIdentifier: "SiteAccessCodeVC") as! SiteAccessCodeVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
        button.add(color: UIColor.clear, title: "Monitor a New Site", image: UIImage(named: "monitor_new_site.png")!) { (item) in
            let strUserId = (UserDefaults.standard.value(forKey: "UserID")) as! String
            
            DELEGATE.dictNewSite.setValue(strUserId, forKey: "userID")
            print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
            
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            let objAddNewSiteVC: AddNewSiteVC = storyboard.instantiateViewController(withIdentifier: "AddNewSiteVC") as! AddNewSiteVC
            self.navigationController?.pushViewController(objAddNewSiteVC, animated: true)
        }
    }
    
    // MARK: - IBAction Methods
        
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtSearch) {
            strSearch = txtTemp.text!
            
            if let dataTask = self.dataTask {
                dataTask.cancel()
            }
            self.fetchAutocompletePlaces(strSearch)
            return true
        }else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell", for: indexPath) as! SearchLocationCell
        cell.delegate = self
        cell.idx = indexPath.row
        cell.lblLocationName.text = self.arrSites[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: SearchLocationCell
    func getLocationDetails(index: NSInteger) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewSites.isHidden = true
        self.txtSearch.text = self.arrSites[index]
    }
    
    // MARK: CLLocationManager
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }

    // MARK: GMSMapView
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    // MARK: KYButtonDelegate
    func openKYButton(_ button: KYButton) {
        
    }
    
    func closeKYButton(_ button: KYButton) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

