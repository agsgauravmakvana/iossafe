//
//  SendReportVC.swift
//

import UIKit

class SendReportVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var txtEmail: ACFloatingTextfield!
    
    @IBOutlet var btnSendNow: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnClose: UIButton!

    @IBOutlet var viewAlert: UIView!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSendNow.layer.cornerRadius = DELEGATE.setCorner()
        btnSendNow.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnSendNow.layer.borderWidth = 1.0
        
        btnDone.layer.cornerRadius = DELEGATE.setCorner()
        btnDone.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnDone.layer.borderWidth = 1.0
        
        viewAlert.isHidden = true
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSendNow(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }
        
        viewAlert.isHidden = false
    }
    
    @IBAction func onClickDone(sender: UIButton) {
        viewAlert.isHidden = true
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
    }
    
    @IBAction func onClickClose(sender: UIButton) {
        viewAlert.isHidden = true
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

