//
//  MySitesCell.swift
//  

import UIKit

protocol MySitesCellProtocol : class {
    func getSitesDetails(index: NSInteger)
}

class MySitesCell: UITableViewCell {
   
    // MARK: - Variables
    var delegate : MySitesCellProtocol?
    var idx: NSInteger = 0
    
    // MARK: - UIControls
    @IBOutlet weak var lblSiteName: UILabel!
    @IBOutlet weak var lblSiteAddress: UILabel!
    @IBOutlet weak var lblSiteUsers: UILabel!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgbackground: UIImageView!
    @IBOutlet weak var btnBackView: UIButton!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickDetails(_ sender: Any) {
        delegate?.getSitesDetails(index: idx)
    }
    
}
