//
//  ThanksVC.swift
//

import UIKit

class ThanksVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnHome: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnHome.layer.cornerRadius = DELEGATE.setCorner()
        btnHome.layer.borderColor = UIColor.white.cgColor
        btnHome.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickHome(sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isLogin")
        
        let dict = UserDefaults.standard.value(forKey: "UserDetails") as! NSDictionary
        let dictUserDetails: NSMutableDictionary = NSMutableDictionary(dictionary: dict)
        dictUserDetails.setValue("1", forKey: "isNewUser")
        
        DELEGATE.dictUserDetails = dictUserDetails as NSDictionary
        UserDefaults.standard.setValue(DELEGATE.dictUserDetails, forKey: "UserDetails")
        
        print("UserID: \(String(describing: UserDefaults.standard.value(forKey: "UserID")))")
        print("UserDetails: \(String(describing: UserDefaults.standard.value(forKey: "UserDetails")))")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objSafeMate: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
        self.navigationController?.pushViewController(objSafeMate, animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

