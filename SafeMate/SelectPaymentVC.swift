//
//  SelectPaymentVC.swift
//

import UIKit

class SelectPaymentVC: UIViewController, PayPalPaymentDelegate {
    
    // MARK: - Variables
    var payPalConfig = PayPalConfiguration()
    var resultText = ""
    
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var acceptCreditCards: Bool = true {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    
    // MARK: - UI Controls
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnPayPal: UIButton!
    @IBOutlet var btnCreditCard: UIButton!
    @IBOutlet var lblPeoples: UILabel!
    @IBOutlet var lblMonth: UILabel!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        let strPeoples = DELEGATE.dictNewSite.value(forKey: "peoples") as! String
        if strPeoples == "5" {
            lblPeoples.text = "1Site / 5Peoples"
            lblMonth.text = "FREE"
        }else if strPeoples == "10" {
            lblPeoples.text = "2Site / 10Peoples"
            lblMonth.text = "$5 / Month"
        }else if strPeoples == "20" {
            lblPeoples.text = "3Site / 20Peoples"
            lblMonth.text = "$10 / Month"
        }else if strPeoples == "50" {
            lblPeoples.text = "5Site / 50Peoples"
            lblMonth.text = "$25 / Month"
        }else {
            lblPeoples.text = "10Site / 100Peoples"
            lblMonth.text = "$50 / Month"
        }
        
        btnPayPal.layer.cornerRadius = DELEGATE.setCorner()
        btnPayPal.layer.borderColor = UIColor(red: 1.0/255.0, green: 0.0/255.0, blue: 122.0/255.0, alpha: 1.0).cgColor
        btnPayPal.layer.borderWidth = 1.0
        
        btnCreditCard.layer.cornerRadius = DELEGATE.setCorner()
        btnCreditCard.layer.borderColor = UIColor(red: 28.0/255.0, green: 184.0/255.0, blue: 211.0/255.0, alpha: 1.0).cgColor
        btnCreditCard.layer.borderWidth = 1.0
        
        self.configPayPal()
    }
    
    // MARK: - UI Methods
    func configPayPal() {
        payPalConfig.acceptCreditCards = acceptCreditCards
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .payPal;
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    func pay() {
        resultText = ""

        // Optional: include multiple items
        let item1 = PayPalItem(name: "Old jeans with holes", withQuantity: 2, withPrice: NSDecimalNumber(string: "84.99"), withCurrency: "USD", withSku: "Hip-0037")
        let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
        let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1, item2, item3]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "5.99")
        let tax = NSDecimalNumber(string: "2.50")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Hipster Clothing", intent: .sale)
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }else {
            DELEGATE.showPopup(title: "Payment Not Processalbe: \(payment)", type: .error)
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickPayPal(sender: UIButton) {
        self.pay()
    }
    
    @IBAction func onClickCreditCard(sender: UIButton) {
        self.pay()
    }
    
    // MARK: - Delegate Methods
    // MARK: PayPalPayment
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        // DELEGATE.showPopup(title: Payment_Cancelled, type: .error)
        
        resultText = ""
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        DELEGATE.showPopup(title: Payment_Success, type: .success)
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            print("\(completedPayment.confirmation)")
            self.resultText = completedPayment.description
            
            let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
            let objVC: PaymentSuccessVC = storyboard.instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
            self.navigationController?.pushViewController(objVC, animated: true)
        })
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

