//
//  PendingUsersVC.swift
//

import UIKit

class PendingUsersVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Variables
    var currentHeight: CGFloat!
    var diff: CGFloat!
    var widthTitle: CGFloat!
    var hightTable: CGFloat!
    
    var arrPendingUsers: NSMutableArray = []
    var arrPermissionUsers: NSMutableArray = []
    var arrSelPendingUsers: NSMutableArray = []
    
    // MARK: - UI Controls
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnApprove: UIButton!
    @IBOutlet var btnReject: UIButton!
    @IBOutlet var tblPendingUsers: UITableView!
    @IBOutlet var viewBottom: UIView!
    @IBOutlet var lblNotFound: UILabel!
    @IBOutlet var lblSelectAll: UILabel!
    @IBOutlet var btnSelectAll: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblNotFound.isHidden = true
        lblSelectAll.isHidden = true
        
        btnApprove.layer.cornerRadius = DELEGATE.setCorner()
        btnApprove.layer.borderColor = UIColor(red: 27.0/255.0, green: 176.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        btnApprove.layer.borderWidth = 1.0
        
        btnReject.layer.cornerRadius = DELEGATE.setCorner()
        btnReject.layer.borderColor = UIColor(red: 255.0/255.0, green: 4.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnReject.layer.borderWidth = 1.0
        
        btnSelectAll.isHidden = true
        btnSelectAll.tag = 0
        btnSelectAll.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        if DELEGATE.isIPad() {
            currentHeight = 145.0
            diff = 3.0
            hightTable = 797.0
        }else {
            currentHeight = 80.0
            diff = -2.0
            hightTable = 422.0
        }
        
        tblPendingUsers.estimatedRowHeight = currentHeight
        tblPendingUsers.rowHeight = UITableViewAutomaticDimension
        
        self.close(view: viewBottom)
        self.getPendingUsers()
    }
    
    // MARK: - UI Methods
    func getPendingUsers() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_PENDING_USERS)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.managePendingUsersData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func managePendingUsersData(dictData: NSDictionary) {
        print("dictData: \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let keyExists = dictData["userList"] != nil
            if keyExists == false {
                lblNotFound.isHidden = false
                lblNotFound.text = "You don't have pending users"
                
                lblSelectAll.isHidden = true
                btnSelectAll.isHidden = true
            }else {
                self.arrPendingUsers = NSMutableArray(array: dictData["userList"] as! Array<Dictionary<String,AnyObject>>)
                
                if (self.arrPendingUsers.count > 0) {
                    self.arrPermissionUsers.removeAllObjects()
                    for i in 0...self.arrPendingUsers.count - 1 {
                        let dict = self.arrPendingUsers[i] as! NSDictionary
                        let dictSiteDetail = dict["siteDetail"] as! NSDictionary
                        if (dictSiteDetail["permission"] as! String) == "1" {
                            self.arrPermissionUsers.add(dict)
                        }
                    }
                }

                if (self.arrPendingUsers.count > 0) {
                    lblNotFound.isHidden = true
                    lblNotFound.text = ""
                    
                    lblSelectAll.isHidden = false
                    btnSelectAll.isHidden = false
                }else {
                    lblNotFound.isHidden = false
                    lblNotFound.text = "You don't have pending users"
                    
                    lblSelectAll.isHidden = true
                    btnSelectAll.isHidden = true
                }
                
                self.tblPendingUsers.reloadData()
            }
        }
    }
    
    func approveUser(strTeamIDs: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, APPROVE_USERS)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "teamID" : strTeamIDs
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageApproveUsersData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageApproveUsersData(dictData: NSDictionary) {
        print("dictData: \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            self.arrSelPendingUsers.removeAllObjects()
            
            btnSelectAll.tag = 5
            self.onClickSelectAllCheckbox(sender: btnSelectAll)
            
            self.getPendingUsers()
        }
    }
    
    func removeUsers(strTeamIDs: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, REMOVE_USERS)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "teamID" : strTeamIDs
        ]

        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageRemoveUsersData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageRemoveUsersData(dictData: NSDictionary) {
        print("dictData: \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            self.arrSelPendingUsers.removeAllObjects()
            
            btnSelectAll.tag = 5
            self.onClickSelectAllCheckbox(sender: btnSelectAll)
            
            self.getPendingUsers()
        }
    }
    
    func onClickSelectedSite(_ sender: UIButton!) {
        let value = sender.tag;
        
        if self.arrSelPendingUsers.contains(self.arrPendingUsers.object(at: value)) {
            self.arrSelPendingUsers.remove(self.arrPendingUsers.object(at: value))
        }else {
            self.arrSelPendingUsers.add(self.arrPendingUsers.object(at: value))
        }
        
        if (self.arrSelPendingUsers.count == self.arrPermissionUsers.count) {
            btnSelectAll.tag = 5
            btnSelectAll.setBackgroundImage(UIImage(named: "checked_filled.png"), for: .normal)
        }else {
            btnSelectAll.tag = 0
            btnSelectAll.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        
        if (self.arrSelPendingUsers.count > 0) {
            self.open(view: viewBottom)
        }else {
            self.close(view: viewBottom)
        }
        
        self.tblPendingUsers.reloadData()
        
        print("self.arrSelPendingUsers: \(self.arrSelPendingUsers)")
    }
    
    func open(view: UIView) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationCurve(.easeInOut)
        view.frame = CGRect(x: 0.0, y: self.view.frame.size.height - view.frame.size.height, width: view.frame.size.width, height: view.frame.size.height)
        self.tblPendingUsers.frame.size.height = hightTable
        UIView.commitAnimations()
    }
    
    func close(view: UIView) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationCurve(.easeInOut)
        view.frame = CGRect(x: 0.0, y: self.view.frame.origin.y + self.view.frame.size.height, width: view.frame.size.width, height: view.frame.size.height)
        self.tblPendingUsers.frame.size.height = hightTable + view.frame.size.height
        UIView.commitAnimations()
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickApprove(sender: UIButton) {
        var strTeamIDs: String = ""
        
        for i in 0...self.arrSelPendingUsers.count - 1 {
            let dict = self.arrSelPendingUsers[i] as! NSDictionary
            strTeamIDs = strTeamIDs.appending(",\(dict["teamID"] as! String)")
        }
        
        strTeamIDs = String(strTeamIDs.characters.dropFirst())
//        print("strTeamIDs: \(strTeamIDs)")
        
        self.approveUser(strTeamIDs: strTeamIDs)
    }
    
    @IBAction func onClickReject(sender: UIButton) {
        var strTeamIDs: String = ""
        
        for i in 0...self.arrSelPendingUsers.count - 1 {
            let dict = self.arrSelPendingUsers[i] as! NSDictionary
            strTeamIDs = strTeamIDs.appending(",\(dict["teamID"] as! String)")
        }
        
        strTeamIDs = String(strTeamIDs.characters.dropFirst())
//        print("strTeamIDs: \(strTeamIDs)")
        
        self.removeUsers(strTeamIDs: strTeamIDs)
    }
    
    @IBAction func onClickSelectAllCheckbox(sender: UIButton) {
        self.arrSelPendingUsers.removeAllObjects()
        
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled.png"), for: .normal)
            
            if (self.arrPermissionUsers.count > 0) {
                for i in 0...self.arrPermissionUsers.count - 1 {
                    let dict = self.arrPermissionUsers[i] as! NSDictionary
                    self.arrSelPendingUsers.add(dict)
                }
            }
            
//            self.arrSelPendingUsers = self.arrPendingUsers.mutableCopy() as! NSMutableArray
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        
        if (self.arrSelPendingUsers.count > 0) {
            self.open(view: viewBottom)
        }else {
            self.close(view: viewBottom)
        }
        
        self.tblPendingUsers.reloadData()
        
        print("self.arrSelPendingUsers: \(self.arrSelPendingUsers)")
    }
    
    // MARK: - Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPendingUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingUsersCell", for: indexPath) as! PendingUsersCell
        
        cell.btnPendingCheckbox.addTarget(self, action:#selector(self.onClickSelectedSite(_:)), for: .touchUpInside)
        cell.btnPendingCheckbox.tag = indexPath.row
        
        cell.imgShadow.isHidden = true
        cell.viewBackground.layer.shadowOpacity = 0.3;
        cell.viewBackground.layer.shadowOffset = CGSize(width: 1, height: 1);
        cell.viewBackground.layer.shadowRadius = 3;
        cell.viewBackground.layer.cornerRadius = 0
        cell.viewBackground.layer.masksToBounds = false
        cell.viewBackground.clipsToBounds = false
        
        let dict = self.arrPendingUsers[indexPath.row] as! NSDictionary
        cell.lblTitle.text = "\(dict["firstname"] as! String) \(dict["lastname"] as! String) (\(dict["companyName"] as! String))"
        cell.lblRole.text = "\(dict["roleName"] as! String)"
        
        let dictSiteDetail = dict["siteDetail"] as! NSDictionary
        cell.lblSiteName.text = "\(dictSiteDetail["siteName"] as! String)"
        
        if self.arrSelPendingUsers.contains(self.arrPendingUsers.object(at: indexPath.row)) {
            cell.btnPendingCheckbox.setBackgroundImage(UIImage(named:"checked_filled.png"), for: UIControlState())
            cell.viewBackground.backgroundColor = UIColor(red: 228.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        }else {
            cell.btnPendingCheckbox.setBackgroundImage(UIImage(named:"unchecked.png"), for: UIControlState())
            cell.viewBackground.backgroundColor = UIColor.white
        }
        
        if (dictSiteDetail["permission"] as! String) == "1" {
            if DELEGATE.isIPad() {
                widthTitle = 545.0
            }else {
                widthTitle = 245.0
            }
            
            cell.btnPendingCheckbox.isHidden = false
        }else {
            if DELEGATE.isIPad() {
                widthTitle = 588.0
            }else {
                widthTitle = 270.0
            }
            
            cell.btnPendingCheckbox.isHidden = true
        }
        
        cell.lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblTitle.sizeToFit()
        
        cell.lblTitle.frame.size.width = widthTitle
        cell.lblRole.frame.origin.y = cell.lblTitle.frame.origin.y + cell.lblTitle.frame.size.height + diff
        cell.lblSiteName.frame.origin.y = cell.lblRole.frame.origin.y + cell.lblRole.frame.size.height + diff
        if DELEGATE.isIPad() {
            cell.viewBackground.frame.size.height = cell.lblSiteName.frame.origin.y + cell.lblSiteName.frame.size.height + (diff * 2.0)
        }else {
            cell.viewBackground.frame.size.height = cell.lblSiteName.frame.origin.y + cell.lblSiteName.frame.size.height - diff
        }
        
        currentHeight = cell.viewBackground.frame.origin.y + cell.viewBackground.frame.size.height + diff
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return currentHeight + 10.0
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

