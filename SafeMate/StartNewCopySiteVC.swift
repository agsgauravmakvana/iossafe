//
//  StartNewCopySiteVC.swift
//

import UIKit

class StartNewCopySiteVC: UIViewController, LBZSpinnerDelegate {
    
    // MARK: - Variables
    var strSiteID: String = ""
    var strSiteName: String = ""
    var strCopyFiles: String = ""
    var strCopyCustomQues: String = ""
    var arrSites: Array<Dictionary<String,String>> = []
    var arrSiteNames: Array<String> = []
    
    // MARK: - UI Controls
    @IBOutlet var btnStartFresh: UIButton!
    @IBOutlet var btnCopySite: UIButton!
    @IBOutlet var btnCopyFile: UIButton!
    @IBOutlet var btnCopyCustQues: UIButton!
    
    @IBOutlet var siteToCopy: LBZSpinner!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnStartFresh.layer.cornerRadius = DELEGATE.setCorner()
        btnStartFresh.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnStartFresh.layer.borderWidth = 1.0
        
        btnCopySite.layer.cornerRadius = DELEGATE.setCorner()
        btnCopySite.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnCopySite.layer.borderWidth = 1.0
        
        strCopyFiles = "1"
        btnCopyFile.tag = 0
        btnCopyFile.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        strCopyCustomQues = "1"
        btnCopyCustQues.tag = 0
        btnCopyCustQues.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        self.arrSiteNames.insert("Site to Copy", at: 0)
        self.setPopup()
        self.getSiteList()
    }
    
    // MARK: - UI Methods
    func setPopup() {
        siteToCopy.updateList(self.arrSiteNames)
        siteToCopy.delegate = self
        
        let font: UIFont!
        if DELEGATE.isIPad() {
            font = UIFont(name: "Roboto-Light", size: 20.0)!
        }else {
            font = UIFont(name: "Roboto-Light", size: 14.0)!
        }
        
        siteToCopy.dDLTextFont = font
        
        if siteToCopy.selectedIndex == LBZSpinner.INDEX_NOTHING {
            siteToCopy.changeSelectedIndex(0)
        }
    }
    
    func getSiteList() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_SITE_LIST)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String
        ]

        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSiteListData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageSiteListData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "site not found" {
                DELEGATE.showPopup(title: msg, type: .error)
            }else {
                let dictSiteListInfo = dictData["siteListInfo"] as! NSDictionary
                
                self.arrSites = dictSiteListInfo["siteList"] as! Array<Dictionary<String, String>>
                print("arrSites: \(self.arrSites)")
                
                for index in 0...self.arrSites.count - 1 {
                    var dict = self.arrSites[index]
                    self.arrSiteNames.insert(dict["siteName"]! as String, at: index + 1)
                }
                siteToCopy.updateList(self.arrSiteNames)
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickStartFresh(sender: UIButton) {
        DELEGATE.dictNewSite.setValue("", forKey: "refSiteID")
        DELEGATE.dictNewSite.setValue("", forKey: "refSiteName")
        DELEGATE.dictNewSite.setValue("1", forKey: "isFileCopy")
        DELEGATE.dictNewSite.setValue("1", forKey: "isFieldCopy")
        
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        DELEGATE.isNewSite = true
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: NewSite = storyboard.instantiateViewController(withIdentifier: "NewSite") as! NewSite
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickCopySite(sender: UIButton) {
        if strSiteName == "Site to Copy" {
            DELEGATE.showPopup(title: Empty_Copy_Site, type: .error)
            return
        }
        
        DELEGATE.dictNewSite.setValue(strSiteID, forKey: "refSiteID")
        DELEGATE.dictNewSite.setValue(strSiteName, forKey: "refSiteName")
        DELEGATE.dictNewSite.setValue(strCopyFiles, forKey: "isFileCopy")
        DELEGATE.dictNewSite.setValue(strCopyCustomQues, forKey: "isFieldCopy")
        
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: NewSite = storyboard.instantiateViewController(withIdentifier: "NewSite") as! NewSite
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickCopyCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        
        if (sender == btnCopyFile) {
            if (sender.tag == 0) {
                strCopyFiles = "1"
            }else {
                strCopyFiles = "0"
            }
        }else {
            if (sender.tag == 0) {
                strCopyCustomQues = "1"
            }else {
                strCopyCustomQues = "0"
            }
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: - LBZSpinner
    func spinnerOpen(_ spinner:LBZSpinner) {
        print("Open")
    }
    
    func spinnerClose(_ spinner:LBZSpinner) {
        print("Close")
    }
    
    func spinnerChoose(_ spinner:LBZSpinner, index:Int, value:String) {
        if index == 0 {
            strSiteID = ""
        }else {
            var dict: Dictionary<String,String> = self.arrSites[index - 1]
            strSiteID = dict["siteID"]! as String
        }
        
        strSiteName = value
        print("strSiteID : \(strSiteID) strSiteName : \(strSiteName)")
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

