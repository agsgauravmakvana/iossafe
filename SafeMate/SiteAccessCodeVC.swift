//
//  SiteAccessCodeVC.swift
//

import UIKit

class SiteAccessCodeVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    // MARK: - UI Controls
    @IBOutlet var txtSiteAccessCode: ACFloatingTextfield!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    func checkValidateAccessCode(strAccessCode: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, Validate_Access_Code)

        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "accessCode": strAccessCode
        ]

        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageValidAccessCodeData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageValidAccessCodeData(dictData: NSDictionary) {
        print("dictData: \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "site not found" {
                DELEGATE.showPopup(title: msg, type: .error)
            }else {
                let dictSiteDetail = dictData["siteDetail"] as! NSDictionary
                
                let geofenceRange = Double(dictSiteDetail["sitegeoFenceRange"] as! String)!
                print("geofenceRange: \(geofenceRange)")
                
                latitude = Double(dictSiteDetail["siteLatitude"] as! String)!
                longitude = Double(dictSiteDetail["siteLogitude"] as! String)!
                print("latitude: \(latitude) longitude: \(longitude)")
                
                let serverLocation = CLLocation(latitude: latitude, longitude: longitude)
                let distance: CLLocationDistance = DELEGATE.currentlocation.distance(from: serverLocation)
                print("distance: \(distance)")
                
                if geofenceRange >= distance {
                    let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                    let objSiteKeyVC: FillYourDetailsVC = storyboard.instantiateViewController(withIdentifier: "FillYourDetailsVC") as! FillYourDetailsVC
                    objSiteKeyVC.dictPrev = dictSiteDetail
                    self.navigationController?.pushViewController(objSiteKeyVC, animated: true)
                }else {
                    let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                    let objSiteKeyVC: SiteKeyVC = storyboard.instantiateViewController(withIdentifier: "SiteKeyVC") as! SiteKeyVC
                    objSiteKeyVC.dictPrev = dictSiteDetail
                    self.navigationController?.pushViewController(objSiteKeyVC, animated: true)
                }
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtSiteAccessCode.text = txtSiteAccessCode.text?.trimmingCharacters(in: .whitespaces)
        if txtSiteAccessCode.text == "" {
            DELEGATE.showPopup(title: Empty_Site_AccessCode, type: .error)
            return
        }
        
        DELEGATE.dictCheckIntoSite.setValue(txtSiteAccessCode.text!, forKey: "accessCode")
        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        self.checkValidateAccessCode(strAccessCode: txtSiteAccessCode.text!)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtSiteAccessCode) {
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
        }else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

