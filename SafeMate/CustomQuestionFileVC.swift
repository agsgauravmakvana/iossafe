//
//  CustomQuestionFileVC.swift
//

import UIKit

protocol CustomQuestionFileVCProtocol : class {
    func reloadCustomQuestionFileList()
}

class CustomQuestionFileVC: UIViewController {
    
    // MARK: - Variables
    var strInputRequired: String = ""
    weak var delegate : CustomQuestionFileVCProtocol?
    var dictPrev: NSDictionary!
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnInputRequired: UIButton!
    
    @IBOutlet var txtQuestion: ACFloatingTextfield!
    @IBOutlet var txtHelpText: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
        
        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        btnCancel.layer.cornerRadius = DELEGATE.setCorner()
        btnCancel.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnCancel.layer.borderWidth = 1.0
        
        btnSave.layer.cornerRadius = DELEGATE.setCorner()
        btnSave.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnSave.layer.borderWidth = 1.0
        
        strInputRequired = "1"
        btnInputRequired.tag = 0
        btnInputRequired.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
    }
    
    // MARK: - UI Methods
    func addQuestion() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, ADD_QUESTION)
        
        let parameters: [AnyHashable: Any] = [
            "fieldType" : "4",
            "siteID" : (dictPrev.value(forKey: "siteID") as! String),
            "question" : txtQuestion.text!,
            "helpText" : txtHelpText.text!,
            "isRequired" : strInputRequired,
            "isAllowMultiline" : "1",
            "minChar" : "0",
            "maxChar" : "0",
            "inputType" : "1",
            "options" : ""
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageAddQuestionData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageAddQuestionData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            let dict = dictData["questionDetail"] as! NSDictionary
            DELEGATE.arrQuestions.add(dict)
            
            delegate?.reloadCustomQuestionFileList()
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCancel(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSave(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtQuestion.text = txtQuestion.text?.trimmingCharacters(in: .whitespaces)
        if txtQuestion.text == "" {
            DELEGATE.showPopup(title: Empty_Question, type: .error)
            return
        }
        
        self.addQuestion()
    }
    
    @IBAction func onClickInputRequiredCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
            
            strInputRequired = "0"
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
            
            strInputRequired = "1"
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

