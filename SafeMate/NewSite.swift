//
//  NewSite.swift
//

import UIKit
import IQKeyboardManagerSwift

enum TravelModes: Int {
    case driving
    case walking
    case bicycling
}

class NewSite: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, GMSMapViewDelegate, SearchLocationCellProtocol, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    // MARK: - Variables
    var locationManager = CLLocationManager()
    var mapTasks = MapTasks()
    var locationMarker: GMSMarker!
    let geoFenceCircle = GMSCircle()
    var radius: Double = 0.0
    var currentCoradinate: CLLocationCoordinate2D!
    fileprivate var dataTask: URLSessionDataTask?
    var strSearch: String = ""
    var arrLocations = [String]()
    
    var strCompanyID: String = ""
    var strCompanyName: String = ""
    var strSearchCompany: String = ""
    
    var arrCompany: Array<Dictionary<String,String>> = []
    var arrSearchCompany: Array<Dictionary<String,String>> = []
    
    // MARK: - UI Controls
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var txtSiteName: ACFloatingTextfield!
    @IBOutlet var txtAddress: ACFloatingTextfield!
    @IBOutlet var txtTrackingRange: ACFloatingTextfield!
    @IBOutlet var txtSupervisor: ACFloatingTextfield!
    @IBOutlet var txtOrganization: ACFloatingTextfield!
    @IBOutlet var txtPhone: ACFloatingTextfield!
    @IBOutlet var txtEmail: ACFloatingTextfield!
    @IBOutlet var txtStartTime: ACFloatingTextfield!
    @IBOutlet var txtEndTime: ACFloatingTextfield!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var viewMap: GMSMapView!
    @IBOutlet var tblLocation: UITableView!
    @IBOutlet var viewLocation: UIView!
    
    @IBOutlet var tblCompany: UITableView!
    @IBOutlet var viewCompany: UIView!
    
    var popStatrTimePicker: PopDatePicker?
    var popEndTimePicker: PopDatePicker?
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (DELEGATE.isIPad() == false) {
            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 575.0)
        }
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        viewMap.delegate = self
        
        self.txtAddress.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        self.txtOrganization.addTarget(self, action: #selector(NewSite.textFieldDidChange), for: .editingChanged)
       
        self.txtStartTime.text = "09:00"
        self.txtEndTime.text = "17:00"
        
        self.setShadow(view: viewLocation)
        self.setShadow(view: viewCompany)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTapLocationView))
        tapGesture.delegate = self
        self.viewLocation.addGestureRecognizer(tapGesture)
        
        let tapCompanyView = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCompanyView))
        tapCompanyView.delegate = self
        self.viewCompany.addGestureRecognizer(tapCompanyView)
        
        popStatrTimePicker = PopDatePicker(forTextField: self.txtStartTime)
        popEndTimePicker = PopDatePicker(forTextField: self.txtEndTime)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.getCompany()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    // MARK: - UI Methods
    func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func setShadow(view: UIView) {
        view.layer.shadowOpacity = 0.3;
        view.layer.shadowOffset = CGSize(width: 1, height: 1);
        view.layer.shadowRadius = 5;
        view.layer.cornerRadius = 0
        view.layer.masksToBounds = false
        view.clipsToBounds = false
        view.isHidden = true
    }
    
    func handleTapLocationView() {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewLocation.isHidden = true
    }
    
    func handleTapCompanyView() {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewCompany.isHidden = true
    }
    
    func textFieldDidChange() {
        if (txtTemp == txtAddress) {
            strSearch = self.txtAddress.text!
            
            if let dataTask = self.dataTask {
                dataTask.cancel()
            }
            self.fetchAutocompletePlaces(strSearch)
        }else if (txtTemp == txtOrganization) {
            strSearchCompany = self.txtOrganization.text!
        }else {
            
        }
    }
    
    fileprivate func fetchAutocompletePlaces(_ keyword: String) {
        let urlString = "\(GOOGLE_AUTO_FILTER_BASE_URL)?key=\(GOOGLE_AUTO_FILTER_API_KEY)&input=\(keyword)"
        let s = (CharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
        s.addCharacters(in: "+&")
        
        if let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: s as CharacterSet) {
            if let url = URL(string: encodedString) {
                
                let request = URLRequest(url: url)
                dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                    if let data = data {
                        do {
                            let result = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                            if let status = result["status"] as? String {
                                if status == "OK" {
                                    if let predictions = result["predictions"] as? NSArray {
                                        var locations = [String]()
                                        for dict in predictions as! [NSDictionary] {
                                            locations.append(dict["description"] as! String)
                                        }
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            self.arrLocations = locations
                                            self.viewLocation.isHidden = false
                                            self.tblLocation.reloadData()
                                        })
                                        return
                                    }
                                }
                            }
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.arrLocations.removeAll()
                                self.viewLocation.isHidden = true
                                self.tblLocation.reloadData()
                            })
                        }
                        catch let error as NSError {
                            print("Error: \(error.localizedDescription)")
                        }
                    }
                })
                dataTask?.resume()
            }
        }
    }
    
    func setupLocationMarker(_ coordinate: CLLocationCoordinate2D) {
        if locationMarker != nil {
            locationMarker.map = nil
        }
        
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = viewMap
        
        locationMarker.title = mapTasks.fetchedFormattedAddress
        locationMarker.appearAnimation = kGMSMarkerAnimationPop
        locationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        locationMarker.opacity = 1.0
        locationMarker.isFlat = true
        
        geoFenceCircle.radius = radius
        geoFenceCircle.position = coordinate
        geoFenceCircle.fillColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 0.2)
        geoFenceCircle.strokeWidth = 1.0
        geoFenceCircle.strokeColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        geoFenceCircle.map = viewMap
    }
    
    func getCompany() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_COMPANY)
        
        manager.post(url, parameters: nil, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageCompanyData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageCompanyData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                self.arrCompany = dictData["companyList"] as! Array<Dictionary<String, String>>
                print("arrCompany: \(self.arrCompany)")
            }else {
                
            }
        }
    }
    
    func checkCompanyIDName(name: String) {
        print("name: \(name)")
        
        if name == "" {
            strCompanyID = ""
            strCompanyName = txtOrganization.text!
            print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
            
            return
        }
        
        if self.arrCompany.count > 0 {
            for index in 0...self.arrCompany.count - 1 {
                var dict: Dictionary<String,String> = self.arrCompany[index]
                
                if (dict["companyName"]! as String) == name {
                    strCompanyID = dict["companyID"]! as String
                    strCompanyName = dict["companyName"]! as String
                    print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
                    
                    break
                }else {
                    strCompanyID = ""
                    strCompanyName = txtOrganization.text!
                    print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
                    
                    continue
                }
            }
        }else {
            strCompanyID = ""
            strCompanyName = txtOrganization.text!
            print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }

        txtSiteName.text = txtSiteName.text?.trimmingCharacters(in: .whitespaces)
        if txtSiteName.text == "" {
            DELEGATE.showPopup(title: Empty_Site_Name, type: .error)
            return
        }
        
        txtAddress.text = txtAddress.text?.trimmingCharacters(in: .whitespaces)
        if txtAddress.text == "" {
            DELEGATE.showPopup(title: Empty_Address, type: .error)
            return
        }
        
        txtTrackingRange.text = txtTrackingRange.text?.trimmingCharacters(in: .whitespaces)
        if txtTrackingRange.text == "" {
            DELEGATE.showPopup(title: Empty_Tracking_Rang, type: .error)
            return
        }
        
        let range: Double = Double(txtTrackingRange.text!)!
        if (range < 50 || range > 2000) {
            DELEGATE.showPopup(title: Mini_Tracking_Rang, type: .error)
            return
        }
        
        radius = range
        geoFenceCircle.radius = radius
        geoFenceCircle.position = self.currentCoradinate
        geoFenceCircle.map = viewMap
        
        txtSupervisor.text = txtSupervisor.text?.trimmingCharacters(in: .whitespaces)
        if txtSupervisor.text == "" {
            DELEGATE.showPopup(title: Empty_Supervisor, type: .error)
            return
        }
        
        txtOrganization.text = txtOrganization.text?.trimmingCharacters(in: .whitespaces)
        if txtOrganization.text == "" {
            DELEGATE.showPopup(title: Empty_Organization, type: .error)
            return
        }
        
        txtPhone.text = txtPhone.text?.trimmingCharacters(in: .whitespaces)
        if txtPhone.text == "" {
            DELEGATE.showPopup(title: Empty_Contact_Number, type: .error)
            return
        }else {
            if (txtPhone.text?.characters.count)! <= 9 {
                DELEGATE.showPopup(title: Mini_Characters_Contact, type: .error)
                return
            }
        }
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        DELEGATE.dictNewSite.setValue(txtSiteName.text!, forKey: "siteName")
        DELEGATE.dictNewSite.setValue(txtAddress.text!, forKey: "siteAddress")
        DELEGATE.dictNewSite.setValue(txtTrackingRange.text!, forKey: "sitegeoFenceRange")
        DELEGATE.dictNewSite.setValue("\(self.currentCoradinate.latitude)", forKey: "siteLatitude")
        DELEGATE.dictNewSite.setValue("\(self.currentCoradinate.longitude)", forKey: "siteLogitude")
        DELEGATE.dictNewSite.setValue(txtSupervisor.text!, forKey: "siteSupervisorName")
        DELEGATE.dictNewSite.setValue(strCompanyID, forKey: "companyID")
        DELEGATE.dictNewSite.setValue(strCompanyName, forKey: "companyName")
        DELEGATE.dictNewSite.setValue(txtPhone.text!, forKey: "siteContactNumber")
        DELEGATE.dictNewSite.setValue(txtEmail.text!, forKey: "siteEmail")
        DELEGATE.dictNewSite.setValue(txtStartTime.text!, forKey: "startTime")
        DELEGATE.dictNewSite.setValue(txtEndTime.text!, forKey: "endTime")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: AdditionalAdministratorsVC = storyboard.instantiateViewController(withIdentifier: "AdditionalAdministratorsVC") as! AdditionalAdministratorsVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == txtStartTime && textField.isFirstResponder) {
            txtStartTime.resignFirstResponder()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            let initDate = dateFormatter.date(from: self.txtStartTime.text!)

            let dataChangedCallback: PopDatePicker.PopDatePickerCallback = { (newDate: Date, forTextField: UITextField) -> () in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                
                let time: String = dateFormatter.string(from: newDate)
                self.txtStartTime.text = time
            }
            
            popStatrTimePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            return false
        }else if (textField == txtEndTime && textField.isFirstResponder) {
            txtEndTime.resignFirstResponder()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            let initDate = dateFormatter.date(from: self.txtEndTime.text!)
            
            let dataChangedCallback: PopDatePicker.PopDatePickerCallback = { (newDate: Date, forTextField: UITextField) -> () in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                
                let time: String = dateFormatter.string(from: newDate)
                self.txtEndTime.text = time
            }
            
            popEndTimePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            return false
        }else {
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtPhone) {
            let invalidCharacters = CharacterSet(charactersIn: "+0123456789").inverted
            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                return false
            }else {
                let currentString: NSString = txtPhone.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= Constant.maxLength
            }
        }else if (textField == txtTrackingRange) {
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
        }else if (textField == txtAddress) {
            strSearch = txtTemp.text!
            
            if let dataTask = self.dataTask {
                dataTask.cancel()
            }
            self.fetchAutocompletePlaces(strSearch)
            return true
        }else if (textField == txtOrganization) {
            if string.isEmpty {
                strSearchCompany = String(strSearchCompany.characters.dropLast())
            }else {
                strSearchCompany = textField.text!+string
            }
            
            let predicate = NSPredicate(format: "SELF.companyName CONTAINS[cd] %@", strSearchCompany)
            let array = (arrCompany as NSArray).filtered(using: predicate)
            
            if array.count > 0 {
                self.arrSearchCompany.removeAll(keepingCapacity: true)
                self.arrSearchCompany = array as! Array<Dictionary<String,String>>
                self.viewCompany.isHidden = false
            }else {
                self.arrSearchCompany.removeAll()
                self.viewCompany.isHidden = true
            }
            
            self.tblCompany.reloadData()
            return true
        }else {
           return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.txtSiteName) {
            self.txtAddress.becomeFirstResponder()
        }else if (textField == self.txtAddress) {
            self.txtTrackingRange.becomeFirstResponder()
        }else if (textField == self.txtTrackingRange) {
            self.txtSupervisor.becomeFirstResponder()
        }else if (textField == self.txtSupervisor) {
            self.txtOrganization.becomeFirstResponder()
        }else if (textField == self.txtOrganization) {
            self.txtPhone.becomeFirstResponder()
        }else if (textField == self.txtPhone) {
            self.txtEmail.becomeFirstResponder()
        }else {
            self.txtEmail.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == txtAddress || textField == txtTrackingRange) {
            
            txtAddress.text = txtAddress.text?.trimmingCharacters(in: .whitespaces)
            if txtAddress.text != "" {
                self.mapTasks.geocodeAddress(txtAddress.text, withCompletionHandler: { (status, success) -> Void in
                    if !success {
                        if status == "ZERO_RESULTS" {
                            self.txtAddress.text = ""
                            DELEGATE.showPopup(title: SEARCH_LOCATION_ERROR, type: .error)
                        }
                    }else {
                        let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
                        self.currentCoradinate = coordinate
                        
                        self.viewMap.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 14.0)
                        self.setupLocationMarker(coordinate)
                        
                        self.txtTrackingRange.text = self.txtTrackingRange.text?.trimmingCharacters(in: .whitespaces)
                        if self.txtTrackingRange.text != "" {
                            let range: Double = Double(self.txtTrackingRange.text!)!
                            if (range < 50 || range > 2000) {
                                DELEGATE.showPopup(title: Mini_Tracking_Rang, type: .error)
                            }else {
                                self.radius = range
                                self.geoFenceCircle.position = self.currentCoradinate
                                self.geoFenceCircle.map = self.viewMap
                            }
                        }else {
                            self.radius = 0.0
                        }
                        
                        self.geoFenceCircle.radius = self.radius
                    }
                })
            }
        }else if (textField == txtOrganization) {
            txtOrganization.text = txtOrganization.text?.trimmingCharacters(in: .whitespaces)
            self.checkCompanyIDName(name: txtOrganization.text!)
        }else {
            
        }
    }
    
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == tblLocation) {
            return self.arrLocations.count
        }else {
            return self.arrSearchCompany.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == tblLocation) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell", for: indexPath) as! SearchLocationCell
            cell.delegate = self
            cell.idx = indexPath.row
            cell.lblLocationName.text = self.arrLocations[indexPath.row]
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell", for: indexPath) as! SearchLocationCell
            cell.delegate = self
            cell.idx = indexPath.row
            
            var dict: Dictionary<String,String> = self.arrSearchCompany[indexPath.row]
            cell.lblLocationName.text = NSString(format:"%@", dict["companyName"]!) as String
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: SearchLocationCell
    func getLocationDetails(index: NSInteger) {
        if (txtTemp == txtAddress) {
            if txtTemp != nil {
                txtTemp.resignFirstResponder()
            }
            
            self.viewLocation.isHidden = true
            self.txtAddress.text = self.arrLocations[index]
            
            self.mapTasks.geocodeAddress(self.arrLocations[index], withCompletionHandler: { (status, success) -> Void in
                if !success {
                    if status == "ZERO_RESULTS" {
                        self.txtAddress.text = ""
                        DELEGATE.showPopup(title: "The location could not be found.", type: .error)
                    }
                }else {
                    let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
                    self.currentCoradinate = coordinate
                    
                    self.viewMap.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 14.0)
                    self.setupLocationMarker(coordinate)
                }
            })
        }else {
            var dict: Dictionary<String,String> = self.arrSearchCompany[index]
            txtOrganization.text = NSString(format:"%@", dict["companyName"]!) as String
            
            if txtTemp != nil {
                txtTemp.resignFirstResponder()
            }
            
            self.viewCompany.isHidden = true
        }
    }
    
    // MARK: CLLocationManager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations: \(locations)")
        self.locationManager.stopUpdatingLocation()
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    // MARK: GMSMapView
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

