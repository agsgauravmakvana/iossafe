//
//  SettingsVC.swift
//

import UIKit

class SettingsVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet weak var switchTracking: UISwitch!
    @IBOutlet weak var switchAutoLogin: UISwitch!
    @IBOutlet weak var switchInactive: UISwitch!
    @IBOutlet weak var switchMultiCheckIns: UISwitch!
    
    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var txtTime: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogout.layer.cornerRadius = DELEGATE.setCorner()
        btnLogout.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnLogout.layer.borderWidth = 1.0
        
        if DELEGATE.isIPad() == false {
            switchTracking.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchAutoLogin.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchInactive.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchMultiCheckIns.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
        }
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickLogout(sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isLogin")
        
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
          _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtTime) {
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
        }else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
