//
//  Questions.swift
//

import UIKit

class Questions: NSObject {

    var QUESTIONS_ROW_ID: String = String()
    var QUESTIONS_ID: String = String()
    var SITE_ID: String = String()
    var FIELD_TYPE_ID: String = String()
    var FIELD_TYPE: String = String()
    var QUESTION: String = String()
    var HELP_TEXT: String = String()
    var IS_REQUIRED: String = String()
    var IS_ALLOW_MULTILINE: String = String()
    var MIN_CHAR: String = String()
    var MAX_CHAR: String = String()
    var INPUT_TYPE: String = String()
}
