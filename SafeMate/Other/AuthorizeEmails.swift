//
//  AuthorizeEmails.swift
//

import UIKit

class AuthorizeEmails: NSObject {

    var AUTHORIZE_ROW_ID: String = String()
    var AUTHORIZE_EMAIL_ID: String = String()
    var AUTHORIZE_EMAIL: String = String()
    var AUTHORIZE_PERMISSION: String = String()
    var SITE_ID: String = String()

}
