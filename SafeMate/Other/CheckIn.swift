//
//  CheckIn.swift
//

import UIKit

class CheckIn: NSObject {

    var CHECKIN_ROW_ID: String = String()
    var CHECKIN_ID: String = String()
    var ATTENDANCE_ID: String = String()
    var CHECK_IN_TIME: String = String()
    var CHECK_OUT_TIME: String = String()
    var TOTAL_WORK_HOURS: String = String()
    var TOTAL_BREAK_HOURS: String = String()

}
