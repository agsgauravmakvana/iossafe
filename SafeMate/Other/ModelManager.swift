//
//  ModelManager.swift
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager {
        if (sharedInstance.database == nil) {
            sharedInstance.database = FMDatabase(path: Util.getPath("\(DATABASE_NAME)"))
        }
        return sharedInstance
    }

    // List of site information
    func listSitesInfo() -> NSMutableArray {
        sharedInstance.database!.open()
        
        let arrSiteInfo: NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM CREATE_SITE", withArgumentsIn: nil)
        if (resultSet != nil) {
            while resultSet.next() {
                let siteInfo: CreateSite = CreateSite()
                siteInfo.SITE_ROW_ID = resultSet.string(forColumn: "SITE_ROW_ID")
                siteInfo.SITE_ID = resultSet.string(forColumn: "SITE_ID")
                siteInfo.SITE_ACCESS_CODE = resultSet.string(forColumn: "SITE_ACCESS_CODE")
                siteInfo.SITE_KEY = resultSet.string(forColumn: "SITE_KEY")
                siteInfo.SITE_REF_ID = resultSet.string(forColumn: "SITE_REF_ID")
                siteInfo.SITE_IS_FILE_COPY = resultSet.string(forColumn: "SITE_IS_FILE_COPY")
                siteInfo.SITE_IS_FIELD_COPY = resultSet.string(forColumn: "SITE_IS_FIELD_COPY")
                siteInfo.SITE_NAME = resultSet.string(forColumn: "SITE_NAME")
                siteInfo.SITE_ADDRESS = resultSet.string(forColumn: "SITE_ADDRESS")
                siteInfo.SITE_GEO_FENCE_RANGE = resultSet.string(forColumn: "SITE_GEO_FENCE_RANGE")
                siteInfo.SITE_LATITUDE = resultSet.string(forColumn: "SITE_LATITUDE")
                siteInfo.SITE_LONGITUDE = resultSet.string(forColumn: "SITE_LONGITUDE")
                siteInfo.SITE_SUPERVISOR_NAME = resultSet.string(forColumn: "SITE_SUPERVISOR_NAME")
                siteInfo.SITE_COMPANY_ID = resultSet.string(forColumn: "SITE_COMPANY_ID")
                siteInfo.SITE_COMPANY_NAME = resultSet.string(forColumn: "SITE_COMPANY_NAME")
                siteInfo.SITE_CONTACT_NUMBER = resultSet.string(forColumn: "SITE_CONTACT_NUMBER")
                siteInfo.SITE_EMAIL = resultSet.string(forColumn: "SITE_EMAIL")
                siteInfo.SITE_AUTH_USERS = resultSet.string(forColumn: "SITE_AUTH_USERS")
                siteInfo.SITE_ALLOW_AUTO_APPROVE = resultSet.string(forColumn: "SITE_ALLOW_AUTO_APPROVE")
                siteInfo.SITE_NOTIFY_EMAIL = resultSet.string(forColumn: "SITE_NOTIFY_EMAIL")
                siteInfo.SITE_EMAIL_ON_ENTRY = resultSet.string(forColumn: "SITE_EMAIL_ON_ENTRY")
                siteInfo.SITE_EMAIL_ON_EXIT = resultSet.string(forColumn: "SITE_EMAIL_ON_EXIT")
                siteInfo.SITE_WELCOME_MSG = resultSet.string(forColumn: "SITE_WELCOME_MSG")
                siteInfo.SITE_IS_REQUIRED_AGREEMENT = resultSet.string(forColumn: "SITE_IS_REQUIRED_AGREEMENT")
                siteInfo.SITE_IS_REQUIRED_NAME = resultSet.string(forColumn: "SITE_IS_REQUIRED_NAME")
                siteInfo.SITE_IS_REQUIRED_ORG = resultSet.string(forColumn: "SITE_IS_REQUIRED_ORG")
                siteInfo.SITE_IS_REQUIRED_GROUP = resultSet.string(forColumn: "SITE_IS_REQUIRED_GROUP")
                siteInfo.SITE_START_TIME = resultSet.string(forColumn: "SITE_START_TIME")
                siteInfo.SITE_END_TIME = resultSet.string(forColumn: "SITE_END_TIME")
                
                arrSiteInfo.add(siteInfo)
            }
        }
        sharedInstance.database!.close()
        
        return arrSiteInfo
    }
    
    // Insert site information
    func insertSiteInfo(_ siteInfo: CreateSite) -> Bool {
        sharedInstance.database!.open()
        
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO CREATE_SITE (SITE_ID, SITE_ACCESS_CODE, SITE_KEY, SITE_REF_ID, SITE_IS_FILE_COPY, SITE_IS_FIELD_COPY, SITE_NAME, SITE_ADDRESS, SITE_GEO_FENCE_RANGE, SITE_LATITUDE, SITE_LONGITUDE, SITE_SUPERVISOR_NAME, SITE_COMPANY_ID, SITE_COMPANY_NAME, SITE_CONTACT_NUMBER, SITE_EMAIL, SITE_AUTH_USERS, SITE_ALLOW_AUTO_APPROVE, SITE_NOTIFY_EMAIL, SITE_EMAIL_ON_ENTRY, SITE_EMAIL_ON_EXIT, SITE_WELCOME_MSG, SITE_IS_REQUIRED_AGREEMENT, SITE_IS_REQUIRED_NAME, SITE_IS_REQUIRED_ORG, SITE_IS_REQUIRED_GROUP, SITE_START_TIME, SITE_END_TIME) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", withArgumentsIn: [siteInfo.SITE_ID, siteInfo.SITE_ACCESS_CODE, siteInfo.SITE_KEY, siteInfo.SITE_REF_ID, siteInfo.SITE_IS_FILE_COPY, siteInfo.SITE_IS_FIELD_COPY, siteInfo.SITE_NAME, siteInfo.SITE_ADDRESS, siteInfo.SITE_GEO_FENCE_RANGE, siteInfo.SITE_LATITUDE, siteInfo.SITE_LONGITUDE, siteInfo.SITE_SUPERVISOR_NAME, siteInfo.SITE_COMPANY_ID, siteInfo.SITE_COMPANY_NAME, siteInfo.SITE_CONTACT_NUMBER, siteInfo.SITE_EMAIL, siteInfo.SITE_AUTH_USERS, siteInfo.SITE_ALLOW_AUTO_APPROVE, siteInfo.SITE_NOTIFY_EMAIL, siteInfo.SITE_EMAIL_ON_ENTRY, siteInfo.SITE_EMAIL_ON_EXIT, siteInfo.SITE_WELCOME_MSG, siteInfo.SITE_IS_REQUIRED_AGREEMENT, siteInfo.SITE_IS_REQUIRED_NAME, siteInfo.SITE_IS_REQUIRED_ORG, siteInfo.SITE_IS_REQUIRED_GROUP, siteInfo.SITE_START_TIME, siteInfo.SITE_END_TIME])
        
        sharedInstance.database!.close()
        
        return isInserted
    }
    
    // Update site information
    func updateSiteInfo(_ siteInfo: CreateSite) -> Bool {
        sharedInstance.database!.open()
        
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE CREATE_SITE SET SITE_ACCESS_CODE=?, SITE_KEY=?, SITE_REF_ID=?, SITE_IS_FILE_COPY=?, SITE_IS_FIELD_COPY=?, SITE_NAME=?, SITE_ADDRESS=?, SITE_GEO_FENCE_RANGE=?, SITE_LATITUDE=?, SITE_LONGITUDE=?, SITE_SUPERVISOR_NAME=?, SITE_COMPANY_ID=?, SITE_COMPANY_NAME=?, SITE_CONTACT_NUMBER=?, SITE_EMAIL=?, SITE_AUTH_USERS=?, SITE_ALLOW_AUTO_APPROVE=?, SITE_NOTIFY_EMAIL=?, SITE_EMAIL_ON_ENTRY=?, SITE_EMAIL_ON_EXIT=?, SITE_WELCOME_MSG=?, SITE_IS_REQUIRED_AGREEMENT=?, SITE_IS_REQUIRED_NAME=?, SITE_IS_REQUIRED_ORG=?, SITE_IS_REQUIRED_GROUP=?, SITE_START_TIME=?, SITE_END_TIME=? WHERE SITE_ID=?", withArgumentsIn: [siteInfo.SITE_ACCESS_CODE, siteInfo.SITE_KEY, siteInfo.SITE_REF_ID, siteInfo.SITE_IS_FILE_COPY, siteInfo.SITE_IS_FIELD_COPY, siteInfo.SITE_NAME, siteInfo.SITE_ADDRESS, siteInfo.SITE_GEO_FENCE_RANGE, siteInfo.SITE_LATITUDE, siteInfo.SITE_LONGITUDE, siteInfo.SITE_SUPERVISOR_NAME, siteInfo.SITE_COMPANY_ID, siteInfo.SITE_COMPANY_NAME, siteInfo.SITE_CONTACT_NUMBER, siteInfo.SITE_EMAIL, siteInfo.SITE_AUTH_USERS, siteInfo.SITE_ALLOW_AUTO_APPROVE, siteInfo.SITE_NOTIFY_EMAIL, siteInfo.SITE_EMAIL_ON_ENTRY, siteInfo.SITE_EMAIL_ON_EXIT, siteInfo.SITE_WELCOME_MSG, siteInfo.SITE_IS_REQUIRED_AGREEMENT, siteInfo.SITE_IS_REQUIRED_NAME, siteInfo.SITE_IS_REQUIRED_ORG, siteInfo.SITE_IS_REQUIRED_GROUP, siteInfo.SITE_START_TIME, siteInfo.SITE_END_TIME, siteInfo.SITE_ID])
        
        sharedInstance.database!.close()
        
        return isUpdated
    }
    
    // List of authorize email information
    func listAuthorizeEmailsInfo() -> NSMutableArray {
        sharedInstance.database!.open()
        
        let arrAuthorizeEmailInfo: NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM AUTHORIZE_EMAILS", withArgumentsIn: nil)
        if (resultSet != nil) {
            while resultSet.next() {
                let authorizeEmailInfo: AuthorizeEmails = AuthorizeEmails()
                authorizeEmailInfo.AUTHORIZE_ROW_ID = resultSet.string(forColumn: "AUTHORIZE_ROW_ID")
                authorizeEmailInfo.AUTHORIZE_EMAIL_ID = resultSet.string(forColumn: "AUTHORIZE_EMAIL_ID")
                authorizeEmailInfo.AUTHORIZE_EMAIL = resultSet.string(forColumn: "AUTHORIZE_EMAIL")
                authorizeEmailInfo.AUTHORIZE_PERMISSION = resultSet.string(forColumn: "AUTHORIZE_PERMISSION")
                authorizeEmailInfo.SITE_ID = resultSet.string(forColumn: "SITE_ID")
                
                arrAuthorizeEmailInfo.add(authorizeEmailInfo)
            }
        }
        sharedInstance.database!.close()
        
        return arrAuthorizeEmailInfo
    }
    
    // Insert authorize email information
    func insertAuthorizeEmailInfo(_ authorizeEmailInfo: AuthorizeEmails) -> Bool {
        sharedInstance.database!.open()
        
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO AUTHORIZE_EMAILS (AUTHORIZE_EMAIL_ID, AUTHORIZE_EMAIL, AUTHORIZE_PERMISSION, SITE_ID) VALUES (?, ?, ?, ?)", withArgumentsIn: [authorizeEmailInfo.AUTHORIZE_EMAIL_ID, authorizeEmailInfo.AUTHORIZE_EMAIL, authorizeEmailInfo.AUTHORIZE_PERMISSION, authorizeEmailInfo.SITE_ID])
        
        sharedInstance.database!.close()
        
        return isInserted
    }
    
    // Update authorize email information
    func updateAuthorizeEmailsInfo(_ authorizeEmailInfo: AuthorizeEmails) -> Bool {
        sharedInstance.database!.open()
        
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE AUTHORIZE_EMAILS SET AUTHORIZE_EMAIL_ID=? AUTHORIZE_EMAIL=?, AUTHORIZE_PERMISSION=? WHERE SITE_ID=?", withArgumentsIn: [authorizeEmailInfo.AUTHORIZE_EMAIL_ID, authorizeEmailInfo.AUTHORIZE_EMAIL, authorizeEmailInfo.AUTHORIZE_PERMISSION, authorizeEmailInfo.SITE_ID])
        
        sharedInstance.database!.close()
        
        return isUpdated
    }
    
    // List of question information
    func listQuestionsInfo() -> NSMutableArray {
        sharedInstance.database!.open()
        
        let arrQuestionInfo: NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM QUESTIONS", withArgumentsIn: nil)
        if (resultSet != nil) {
            while resultSet.next() {
                let questionInfo: Questions = Questions()
                questionInfo.QUESTIONS_ROW_ID = resultSet.string(forColumn: "QUESTIONS_ROW_ID")
                questionInfo.QUESTIONS_ID = resultSet.string(forColumn: "QUESTIONS_ID")
                questionInfo.SITE_ID = resultSet.string(forColumn: "SITE_ID")
                questionInfo.FIELD_TYPE_ID = resultSet.string(forColumn: "FIELD_TYPE_ID")
                questionInfo.FIELD_TYPE = resultSet.string(forColumn: "FIELD_TYPE")
                questionInfo.QUESTION = resultSet.string(forColumn: "QUESTION")
                questionInfo.HELP_TEXT = resultSet.string(forColumn: "HELP_TEXT")
                questionInfo.IS_REQUIRED = resultSet.string(forColumn: "IS_REQUIRED")
                questionInfo.IS_ALLOW_MULTILINE = resultSet.string(forColumn: "IS_ALLOW_MULTILINE")
                questionInfo.MIN_CHAR = resultSet.string(forColumn: "MIN_CHAR")
                questionInfo.MAX_CHAR = resultSet.string(forColumn: "MAX_CHAR")
                questionInfo.INPUT_TYPE = resultSet.string(forColumn: "INPUT_TYPE")
                
                arrQuestionInfo.add(questionInfo)
            }
        }
        sharedInstance.database!.close()
        
        return arrQuestionInfo
    }
    
    // Insert question information
    func insertQuestionInfo(_ questionInfo: Questions) -> Bool {
        sharedInstance.database!.open()
        
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO QUESTIONS (QUESTIONS_ID, SITE_ID, FIELD_TYPE_ID, FIELD_TYPE, QUESTION, HELP_TEXT, IS_REQUIRED, IS_ALLOW_MULTILINE, MIN_CHAR, MAX_CHAR, INPUT_TYPE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", withArgumentsIn: [questionInfo.QUESTIONS_ID, questionInfo.SITE_ID, questionInfo.FIELD_TYPE_ID, questionInfo.FIELD_TYPE, questionInfo.QUESTION, questionInfo.HELP_TEXT, questionInfo.IS_REQUIRED, questionInfo.IS_ALLOW_MULTILINE, questionInfo.MIN_CHAR, questionInfo.MAX_CHAR, questionInfo.INPUT_TYPE])
        
        sharedInstance.database!.close()
        
        return isInserted
    }
    
    // Update question information
    func updateQuestionInfo(_ questionInfo: Questions) -> Bool {
        sharedInstance.database!.open()
        
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE QUESTIONS SET QUESTIONS_ID=? FIELD_TYPE_ID=?, FIELD_TYPE=?, QUESTION=?, HELP_TEXT=?, IS_REQUIRED=?, IS_ALLOW_MULTILINE=?, MIN_CHAR=?, MAX_CHAR=?, INPUT_TYPE=? WHERE SITE_ID=?", withArgumentsIn: [questionInfo.QUESTIONS_ID, questionInfo.FIELD_TYPE_ID, questionInfo.FIELD_TYPE, questionInfo.QUESTION, questionInfo.HELP_TEXT, questionInfo.IS_REQUIRED, questionInfo.IS_ALLOW_MULTILINE, questionInfo.MIN_CHAR, questionInfo.MAX_CHAR, questionInfo.INPUT_TYPE, questionInfo.SITE_ID])
        
        sharedInstance.database!.close()
        
        return isUpdated
    }
    
    // List of option information
    func listOptionsInfo() -> NSMutableArray {
        sharedInstance.database!.open()
        
        let arrOptionInfo: NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM OPTIONS", withArgumentsIn: nil)
        if (resultSet != nil) {
            while resultSet.next() {
                let optionInfo: Options = Options()
                optionInfo.OPTIONS_ROW_ID = resultSet.string(forColumn: "OPTIONS_ROW_ID")
                optionInfo.OPTIONS_ID = resultSet.string(forColumn: "OPTIONS_ID")
                optionInfo.OPTIONS_NAME = resultSet.string(forColumn: "OPTIONS_NAME")
                optionInfo.QUESTIONS_ID = resultSet.string(forColumn: "QUESTIONS_ID")
                
                arrOptionInfo.add(optionInfo)
            }
        }
        sharedInstance.database!.close()
        
        return arrOptionInfo
    }
    
    // Insert option information
    func insertOptionInfo(_ optionInfo: Options) -> Bool {
        sharedInstance.database!.open()
        
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO OPTIONS (OPTIONS_ID, OPTIONS_NAME, QUESTIONS_ID) VALUES (?, ?, ?)", withArgumentsIn: [optionInfo.OPTIONS_ID, optionInfo.OPTIONS_NAME, optionInfo.QUESTIONS_ID])
        
        sharedInstance.database!.close()
        
        return isInserted
    }
    
    // Update option information
    func updateOptionInfo(_ optionInfo: Options) -> Bool {
        sharedInstance.database!.open()
        
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE OPTIONS SET OPTIONS_ID=? OPTIONS_NAME=? WHERE QUESTIONS_ID=?", withArgumentsIn: [optionInfo.OPTIONS_ID, optionInfo.OPTIONS_NAME, optionInfo.QUESTIONS_ID])
        
        sharedInstance.database!.close()
        
        return isUpdated
    }
}
