//
//  CreateSite.swift
//

import UIKit

class CreateSite: NSObject {
    
    var SITE_ROW_ID: String = String()
    var SITE_ID: String = String()
    var SITE_ACCESS_CODE: String = String()
    var SITE_KEY: String = String()
    var SITE_REF_ID: String = String()
    var SITE_IS_FILE_COPY: String = String()
    var SITE_IS_FIELD_COPY: String = String()
    var SITE_NAME: String = String()
    var SITE_ADDRESS: String = String()
    var SITE_GEO_FENCE_RANGE: String = String()
    var SITE_LATITUDE: String = String()
    var SITE_LONGITUDE: String = String()
    var SITE_SUPERVISOR_NAME: String = String()
    var SITE_COMPANY_ID: String = String()
    var SITE_COMPANY_NAME: String = String()
    var SITE_CONTACT_NUMBER: String = String()
    var SITE_EMAIL: String = String()
    var SITE_AUTH_USERS: String = String()
    var SITE_ALLOW_AUTO_APPROVE: String = String()
    var SITE_NOTIFY_EMAIL: String = String()
    var SITE_EMAIL_ON_ENTRY: String = String()
    var SITE_EMAIL_ON_EXIT: String = String()
    var SITE_WELCOME_MSG: String = String()
    var SITE_IS_REQUIRED_AGREEMENT: String = String()
    var SITE_IS_REQUIRED_NAME: String = String()
    var SITE_IS_REQUIRED_ORG: String = String()
    var SITE_IS_REQUIRED_GROUP: String = String()
    var SITE_START_TIME: String = String()
    var SITE_END_TIME: String = String()

}
