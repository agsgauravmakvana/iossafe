//
//  AuthorizeVC.swift
//

import UIKit

class AuthorizeVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var strNewUser: String = ""
    var strExit: String = ""
    var strEntry: String = ""
    
    // MARK: - UI Controls
    @IBOutlet var lblSiteName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet weak var switchNewUser: UISwitch!
    @IBOutlet weak var switchExit: UISwitch!
    @IBOutlet weak var switchEntry: UISwitch!
    
    @IBOutlet var txtNotifyEmail: ACFloatingTextfield!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        lblSiteName.text = (DELEGATE.dictNewSite.value(forKey: "siteName") as! String)
        lblLocation.text = (DELEGATE.dictNewSite.value(forKey: "siteAddress") as! String)
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0

        if DELEGATE.isIPad() == false {
            switchNewUser.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchExit.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
            switchEntry.transform = CGAffineTransform(scaleX: 0.73, y: 0.68)
        }
        
        switchNewUser.isOn = true
        strNewUser = "0"
        
        switchExit.isOn = false
        strExit = "1"
        
        switchEntry.isOn = false
        strEntry = "1"
    }

    // MARK: - UI Methods

    // MARK: - IBAction Methods
    @IBAction func isSwitchNewUserOn(sender: UISwitch) {
        if sender.isOn {
            strNewUser = "0"
        }else {
            strNewUser = "1"
        }
    }
    
    @IBAction func isSwitchExitOn(sender: UISwitch) {
        if sender.isOn {
            strExit = "0"
        }else {
            strExit = "1"
        }
    }
    
    @IBAction func isSwitchEntry(sender: UISwitch) {
        if sender.isOn {
            strEntry = "0"
        }else {
            strEntry = "1"
        }
    }
    
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtNotifyEmail.text = txtNotifyEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtNotifyEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtNotifyEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }
        
        DELEGATE.dictNewSite.setValue(strNewUser, forKey: "allowAutoApprove")
        DELEGATE.dictNewSite.setValue(txtNotifyEmail.text!, forKey: "notifyEmail")
        DELEGATE.dictNewSite.setValue(strEntry, forKey: "emailOnEntry")
        DELEGATE.dictNewSite.setValue(strExit, forKey: "emailOnExit")
        print("DELEGATE.dictNewSite: \(DELEGATE.dictNewSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: MessageboardCustSelVC = storyboard.instantiateViewController(withIdentifier: "MessageboardCustSelVC") as! MessageboardCustSelVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
