//
//  TimerVC.swift
//

import UIKit

class TimerVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnPlayPause: UIButton!
    @IBOutlet var btnStop: UIButton!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblWorkStarted: UILabel!
    @IBOutlet var imgWorkStarted: UIImageView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPlayPause.tag = 0
        lblTitle.text = "Welcome! Start Your Time"
        btnPlayPause.setBackgroundImage(UIImage(named: "start.png"), for: .normal)
        lblWorkStarted.isHidden = true
        imgWorkStarted.isHidden = true
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickPlayPause(sender: UIButton) {
        let button: UIButton! = sender
        
        if (button.tag == 0) {
            button.tag = 5
            lblTitle.text = "You'ar on Break"
            btnPlayPause.setBackgroundImage(UIImage(named: "pause.png"), for: .normal)
            lblWorkStarted.isHidden = false
            imgWorkStarted.isHidden = false
        }else {
            button.tag = 0
            lblTitle.text = "Wow! - You are on the Fied"
            btnPlayPause.setBackgroundImage(UIImage(named: "start.png"), for: .normal)
            lblWorkStarted.isHidden = true
            imgWorkStarted.isHidden = true
        }
    }
    
    @IBAction func onClickStop(sender: UIButton) {
        btnPlayPause.tag = 0
        lblTitle.text = "Welcome! Start Your Time"
        btnPlayPause.setBackgroundImage(UIImage(named: "start.png"), for: .normal)
        lblWorkStarted.isHidden = true
        imgWorkStarted.isHidden = true
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

