//
//  FillYourDetailsVC.swift
//

import UIKit
import CoreLocation
import MapKit

class FillYourDetailsVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, LBZSpinnerDelegate, CLLocationManagerDelegate, UIScrollViewDelegate, SearchLocationCellProtocol, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextViewDelegate {
    
    // MARK: - Variables
    var x: CGFloat = 0.0
    var h: CGFloat = 0.0
    var w: CGFloat = 0.0
    var d: CGFloat = 0.0
    var defaultHeight: CGFloat = 0.0
    var queSpace: CGFloat = 0.0
    var dictPrev = NSDictionary()
    
    var arrQuestions: NSMutableArray = []
    var arrCustomField = NSMutableArray()
    var arrDropDown = NSMutableArray()
    var arrTextField = NSMutableArray()
    var arrCheckbox = NSMutableArray()
    var arrRadioButton = NSMutableArray()
    
    var strRoleID: String = ""
    var strRoleName: String = ""
    var strCompanyID: String = ""
    var strCompanyName: String = ""
    var strSearchCompany: String = ""
    var arrCompany: Array<Dictionary<String,String>> = []
    var arrSearchCompany: Array<Dictionary<String,String>> = []
    var arrRole: Array<Dictionary<String,String>> = []
    var arrRoleName: Array<String> = []
    
    var strImageData: String = ""
    var imageData: NSData?
    
    // MARK: - UI Controls
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnUpdateImage: UIButton!
    
    @IBOutlet var txtFirstName: ACFloatingTextfield!
    @IBOutlet var txtLastName: ACFloatingTextfield!
    @IBOutlet var txtOrganization: ACFloatingTextfield!
    @IBOutlet var txtPhoneNumber: ACFloatingTextfield!
    @IBOutlet var txtEmail: ACFloatingTextfield!
    @IBOutlet var group: LBZSpinner!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var imgPlaceholder: UIImageView!
    @IBOutlet var viewCustomField: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var tblCompany: UITableView!
    @IBOutlet var viewCompany: UIView!
    
    var txtViewTemp: UITextView!
    var txtFieldTemp: UITextField!
    var txtTemp: ACFloatingTextfield!
    var picker: UIImagePickerController! = UIImagePickerController()
    var popover: UIPopoverController! = nil
    var font: UIFont!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictPrev: \(dictPrev)")
//        print("DELEGATE.dictUserDetails: \(DELEGATE.dictUserDetails)")
        
        DELEGATE.dictCheckIntoSite.setValue(dictPrev, forKey: "siteDetail")
//        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        imgPlaceholder.isHidden = false
        btnCamera.isHidden = false
        btnUpdateImage.isHidden = true

        let dictUserDetails = (UserDefaults.standard.value(forKey: "UserDetails")) as! NSDictionary
        let image: String = (dictUserDetails["profilePic"] as! String)
        if image != "" {
            let url = String(format: "%@%@", USER_PIC_URL, image)
            self.imageFromServerURL(urlString: url)
        }
        
        txtFirstName.text = (dictUserDetails["firstName"] as! String)
        txtLastName.text = (dictUserDetails["lastName"] as! String)
        txtPhoneNumber.text = (dictUserDetails["contactNumber"] as! String)
        txtEmail.text = (dictUserDetails["email"] as! String)
        
        strCompanyID = (dictUserDetails["companyID"] as! String)
        strCompanyName = (dictUserDetails["companyName"] as! String)
        print("strCompanyID: \(strCompanyID) strCompanyName: \(strCompanyName)")

        txtOrganization.text = strCompanyName
        
        strRoleID = (dictUserDetails["roleID"] as! String)
        strRoleName = (dictUserDetails["roleName"] as! String)
        print("strRoleID: \(strRoleID) strRoleName: \(strRoleName)")
        
        let dict: Dictionary<String,String> = [
            "roleID" : strRoleID,
            "roleName" : strRoleName
        ]
        self.arrRole.insert(dict, at: 0)
        
        self.arrQuestions = NSMutableArray(array: dictPrev["questionList"] as! Array<Dictionary<String,AnyObject>>)
//        print("arrQuestions: \(self.arrQuestions)")
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
        
        picker.delegate = self
        picker.allowsEditing = true
        
        txtOrganization.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        
        self.viewCompany.layer.shadowOpacity = 0.3;
        self.viewCompany.layer.shadowOffset = CGSize(width: 1, height: 1);
        self.viewCompany.layer.shadowRadius = 5;
        self.viewCompany.layer.cornerRadius = 0
        self.viewCompany.layer.masksToBounds = false
        self.viewCompany.clipsToBounds = false
        self.viewCompany.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture))
        tapGesture.delegate = self
        self.viewCompany.addGestureRecognizer(tapGesture)
        
        if DELEGATE.isIPad() {
            d = 8.0
            x = 100.0
            h = 65.0
            w = 568.0
            font = UIFont(name: "Roboto-Regular", size: 20.0)!
            defaultHeight = 10.0
            queSpace = 0.0
        }else {
            d = 8.0
            x = 20.0
            h = 38.0
            w = 280.0
            font = UIFont(name: "Roboto-Regular", size: 14.0)!
            queSpace = 0.0
        }
        
        self.arrRoleName.insert("Group", at: 0)
        self.arrRoleName.insert(strRoleName, at: 1)

        self.setPopup()
        self.getRoles()
        self.setCustomField()
    }
    
    // MARK: - UI Methods
    func handleTapGesture() {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewCompany.isHidden = true
    }
    
    func textFieldDidChange() {
        strSearchCompany = txtOrganization.text!
    }
    
    func getRoles() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_ROLES)
        
        manager.post(url, parameters: nil, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageRolesData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
            
            self.getCompany()
        })
    }
    
    func manageRolesData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                self.arrRole = dictData["roleList"] as! Array<Dictionary<String, String>>
                print("arrRole: \(self.arrRole)")
                
                self.arrRoleName.remove(at: 1)
                for index in 0...self.arrRole.count - 1 {
                    var dict = self.arrRole[index]
                    self.arrRoleName.insert(dict["roleName"]! as String, at: index + 1)
                }
                group.updateList(self.arrRoleName)
            }else {
                
            }
        }
        
        self.getCompany()
    }
    
    func checkRoleIDName(name: String) {
        print("name: \(name)")
        
        if name == "" {
            strRoleID = ""
            strRoleName = "Group"
            print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
            
            return
        }
        
        if self.arrRole.count > 0 {
            for index in 0...self.arrRole.count - 1 {
                var dict: Dictionary<String,String> = self.arrRole[index]
                
                if (dict["roleName"]! as String) == name {
                    strRoleID = dict["roleID"]! as String
                    strRoleName = dict["roleName"]! as String
                    print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
                    
                    break
                }else {
                    strRoleID = ""
                    strRoleName = "Group"
                    print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
                    
                    continue
                }
            }
        }else {
            strRoleID = ""
            strRoleName = "Group"
            print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
        }
    }
    
    func getCompany() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_COMPANY)
        
        manager.post(url, parameters: nil, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageCompanyData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageCompanyData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                self.arrCompany = dictData["companyList"] as! Array<Dictionary<String, String>>
                print("arrCompany: \(self.arrCompany)")
            }else {
                
            }
        }
    }
    
    func checkCompanyIDName(name: String) {
        print("name: \(name)")
        
        if name == "" {
            strCompanyID = ""
            strCompanyName = txtOrganization.text!
            print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
            
            return
        }
        
        if self.arrCompany.count > 0 {
            for index in 0...self.arrCompany.count - 1 {
                var dict: Dictionary<String,String> = self.arrCompany[index]
                
                if (dict["companyName"]! as String) == name {
                    strCompanyID = dict["companyID"]! as String
                    strCompanyName = dict["companyName"]! as String
                    print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
                    
                    break
                }else {
                    strCompanyID = ""
                    strCompanyName = txtOrganization.text!
                    print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
                    
                    continue
                }
            }
        }else {
            strCompanyID = ""
            strCompanyName = txtOrganization.text!
            print("strCompanyID : \(strCompanyID) strCompanyName : \(strCompanyName)")
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = UIImagePickerControllerSourceType.camera
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.present(picker, animated: true, completion: nil)
            }else {
                popover = UIPopoverController(contentViewController: picker)
                popover.present(from: btnCamera.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            }
        }else {
            DELEGATE.showPopup(title: No_Camera, type: .error)
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.present(picker, animated: true, completion: nil)
            }else {
                popover = UIPopoverController(contentViewController: picker)
                popover.present(from: btnCamera.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            }
        }
    }
    
    func setPopup() {
        group.updateList(self.arrRoleName)
        group.delegate = self
            
        if DELEGATE.isIPad() {
            group.dDLTextFont = UIFont(name: "Roboto-Regular", size: 20.0)!
        }else {
            group.dDLTextFont = UIFont(name: "Roboto-Regular", size: 14.0)!
        }
            
        if group.selectedIndex == LBZSpinner.INDEX_NOTHING {
            group.changeSelectedIndex(1)
        }
    }
    
    func imageFromServerURL(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                DELEGATE.showPopup(title: "\(String(describing: error?.localizedDescription))", type: .error)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.imgView.image = image
                
                self.btnUpdateImage.isHidden = false
                self.btnCamera.isHidden = true
                self.imgPlaceholder.isHidden = true
                
                self.imageData = data as NSData?
                self.strImageData = (self.imageData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) as String!)!
            })
            
        }).resume()
    }
    
    func setCustomField() {
        // Set custom fields programmatically
        if (self.arrQuestions.count > 0) {
            for index in 0...self.arrQuestions.count - 1 {
                let dict = self.arrQuestions[index] as! Dictionary<String, AnyObject>
//                print("dict: \(dict)")
                
                let strFieldType = dict["fieldType"] as! String
                if strFieldType == "1" {
                    // Deopdown
                    self.questionAndDropdown(queData: dict, queNo: "\(index + 1)")
                }else if strFieldType == "2" {
                    // Checkbox
                    self.questionAndCheckbox(queData: dict, queNo: "\(index + 1)")
                }else if strFieldType == "3" {
                    // Radio Button
                    self.questionAndRadioButton(queData: dict, queNo: "\(index + 1)")
                }else if strFieldType == "4" {
                    // File
                    
                }else if strFieldType == "5" {
                    // Text
                    self.questionAndTextField(queData: dict, queNo: "\(index + 1)")
                }else {
                    
                }
            }
        }

        viewCustomField.frame = CGRect(x: x, y: group.frame.origin.y + group.frame.size.height + d, width: w, height: defaultHeight)
 
        var diff: CGFloat = 0.0 // Set difference between content view & next button
        if (DELEGATE.isIPad()) {
            diff = 43.0
        }else {
            diff = 28.0
        }
        
        btnNext.frame = CGRect(x: btnNext.frame.origin.x ,y :viewCustomField.frame.origin.y + viewCustomField.frame.height + diff - 15.0, width: btnNext.frame.size.width, height: btnNext.frame.size.height)
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: (btnNext.frame.origin.y + btnNext.frame.size.height + diff + 15.0))
    }
    
    func questionAndTextField(queData: Dictionary<String, AnyObject>, queNo: String) {  // Add question with textfield programmatically
        let dictTextFieldData = NSMutableDictionary()
        dictTextFieldData.setValue(queData, forKey: "questionData")  // Set each question data (Note: Comming from serrver side)
        dictTextFieldData.setValue(queNo, forKey: "questionNo")  // Set each question no
        
        let lblQuestion = UILabel(frame: CGRect(x: 0.0, y: defaultHeight, width: w, height: h))
        lblQuestion.numberOfLines = 0;
        lblQuestion.font = font
        lblQuestion.text = "Q.\(queNo) \(queData["question"] as! String)"
        lblQuestion.textColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        lblQuestion.lineBreakMode = NSLineBreakMode.byWordWrapping;
        lblQuestion.sizeToFit()
        lblQuestion.frame.size.width = w
        viewCustomField.addSubview(lblQuestion)

        let strInputType = (queData["inputType"] as! String)
        dictTextFieldData.setValue(strInputType, forKey: "inputType")  // Set input type
        
        var frame: CGRect
        if (queData["isAllowMultiline"] as! String) == "1" {
            // Code for single line
            frame = CGRect(x: 0.0, y: (lblQuestion.frame.origin.y + lblQuestion.frame.size.height + d), width: w, height: h)
            
            let txtField = UITextField(frame: frame)
            txtField.tintColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0)
            txtField.textColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0)
            txtField.tag = Int(queNo)!
            txtField.font = font
            txtField.autocorrectionType = .no
            txtField.delegate = self
            
            if (queData["helpText"] as! String) == "" {
                txtField.placeholder = "Enter Text"
            }else {
                txtField.placeholder = ("\(queData["helpText"] as! String)")
            }
            
            if strInputType == "1" {
                txtField.keyboardType = .default
            }else if strInputType == "2" {
                txtField.keyboardType = .default
            }else if strInputType == "3" {
                txtField.keyboardType = .numberPad
            }else if strInputType == "4" {
                txtField.keyboardType = .alphabet
            }else {
                
            }
            
            viewCustomField.addSubview(txtField)
        }else {
            // Code for multi line
            frame = CGRect(x: 0.0, y: (lblQuestion.frame.origin.y + lblQuestion.frame.size.height + d), width: w, height: h * 2.0)
            
            let txtView = UITextView(frame: frame)
            txtView.tintColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0)
            txtView.textColor = UIColor.lightGray
            txtView.tag = Int(queNo)!
            txtView.font = font
            txtView.autocorrectionType = .no
            txtView.delegate = self
            
            if (queData["helpText"] as! String) == "" {
                txtView.text = "Enter Text"
            }else {
                txtView.text = ("\(queData["helpText"] as! String)")
            }
            
            if strInputType == "1" {
                txtView.keyboardType = .default
            }else if strInputType == "2" {
                txtView.keyboardType = .default
            }else if strInputType == "3" {
                txtView.keyboardType = .numberPad
            }else if strInputType == "4" {
                txtView.keyboardType = .alphabet
            }else {
                
            }
            
            viewCustomField.addSubview(txtView)
        }
        
        let viewSep = UIView(frame: CGRect(x: 0.0, y: frame.origin.y + frame.size.height - 1.0, width: w, height: 1.0))
        viewSep.backgroundColor = UIColor.black
        viewCustomField.addSubview(viewSep)
        
        dictTextFieldData.setValue((queData["maxChar"] as! String), forKey: "maxChar")  // Set maximum char
        dictTextFieldData.setValue((queData["minChar"] as! String), forKey: "minChar")  // Set minimum char
        dictTextFieldData.setValue(queNo, forKey: "tag")  // Set tag
        dictTextFieldData.setValue("", forKey: "textfieldText") // Set textfield value (Default: "")
        
        self.arrTextField.add(dictTextFieldData)
//        print("self.arrTextField: \(self.arrTextField)")
        
        defaultHeight = frame.origin.y + frame.size.height + d + queSpace
    }
    
    func questionAndRadioButton(queData: Dictionary<String, AnyObject>, queNo: String) {    // Add question with radio buttons programmatically
        let dictRadioButtonData = NSMutableDictionary()
        dictRadioButtonData.setValue(queData, forKey: "questionData")  // Set each question data (Note: Comming from serrver side)
        dictRadioButtonData.setValue(queNo, forKey: "questionNo")  // Set each question no
        
        let lblQuestion = UILabel(frame: CGRect(x: 0.0, y: defaultHeight, width: w, height: h))
        lblQuestion.numberOfLines = 0;
        lblQuestion.font = font
        
        if (queData["helpText"] as! String) == "" {
            lblQuestion.text = "Q.\(queNo) \(queData["question"] as! String)"
        }else {
            lblQuestion.text = "Q.\(queNo) \(queData["question"] as! String) (\(queData["helpText"] as! String))"
        }
        
        lblQuestion.textColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        lblQuestion.lineBreakMode = NSLineBreakMode.byWordWrapping;
        lblQuestion.sizeToFit()
        lblQuestion.frame.size.width = w
        viewCustomField.addSubview(lblQuestion)
        
        var arrOptions: Array<Dictionary<String,String>> = []
        arrOptions = queData["options"] as! Array<Dictionary<String, String>>
//        print("arrOptions: \(arrOptions)")
        
        dictRadioButtonData.setValue(arrOptions, forKey: "options") // Array of optionID & optionName
        dictRadioButtonData.setValue(queNo, forKey: "tag")  // Set tag
        dictRadioButtonData.setValue((queData["inputType"] as! String), forKey: "inputType")  // Set input type
        dictRadioButtonData.setValue("", forKey: "radioButtonText") // Set selected radio button optionID (Default: "")
        
        var temp: CGFloat = 0.0
        
        var rbw: CGFloat = 0.0
        var rbh: CGFloat = 0.0
        var th: CGFloat = 0.0
        var rbd: CGFloat = 0.0
        
        var fontTitle: UIFont!
        
        if (DELEGATE.isIPad()) {
            rbw = 25.0
            rbh = 25.0
            th = 45.0
            rbd = 8.0
            fontTitle = UIFont(name: "Roboto-Regular", size: 20.0)!
        }else {
            rbw = 18.0
            rbh = 18.0
            th = 22.0
            rbd = 8.0
            fontTitle = UIFont(name: "Roboto-Regular", size: 13.0)!
        }

        temp = (lblQuestion.frame.origin.y + lblQuestion.frame.size.height + d)
        
        if arrOptions.count > 0 {
            var arrRadioButtons:Array<UIButton> = []
            
            for i in 0...arrOptions.count - 1 {
                var dict = arrOptions[i]
                
                let btnRadio = UIButton(frame: CGRect(x: 0.0, y:(temp - ((th - rbh) / 2.0)), width: rbw, height: rbh))
                btnRadio.tag = 0
                btnRadio.setTitle("\(dict["optionID"]! as String) \(queNo)", for: .normal)
                btnRadio.setBackgroundImage(UIImage(named: "radio_normal.png"), for: .normal)
                btnRadio.setBackgroundImage(UIImage(named: "radio_selected.png"), for: .selected)
                btnRadio.setTitleColor(UIColor.clear, for: .normal)
                btnRadio.addTarget(self, action: #selector(self.onClickRadioButton), for: .touchUpInside)
                viewCustomField.addSubview(btnRadio)
                
                arrRadioButtons.insert(btnRadio, at: i) // Add each and every radio buttons in single array
                
                let lblTitle = UILabel(frame: CGRect(x :btnRadio.frame.origin.x + rbw + rbd, y :temp, width: (viewCustomField.frame.size.width - (btnRadio.frame.origin.x + rbw + rbd)), height: th))
                lblTitle.numberOfLines = 0
                lblTitle.font = fontTitle
                lblTitle.text = "\(dict["optionName"]! as String)"
                lblTitle.textColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.sizeToFit()
                
                lblTitle.frame.size.width = (viewCustomField.frame.size.width - (btnRadio.frame.origin.x + rbw + rbd))
                viewCustomField.addSubview(lblTitle)
                
                btnRadio.frame.origin.y = lblTitle.frame.origin.y
                temp = temp + lblTitle.frame.size.height + d
            }
            
            dictRadioButtonData.setValue(arrRadioButtons, forKey: "radioButtons")  // Set array of radio buttons
        }
        
        self.arrRadioButton.add(dictRadioButtonData)
//        print("self.arrRadioButton: \(self.arrRadioButton)")
        
        defaultHeight = temp + d + queSpace
    }
    
    func onClickRadioButton(sender: UIButton) { // Click event of radio buttons
        let queNo = sender.titleLabel?.text?.characters.split{$0 == " "}.map(String.init)
//        print("queNo: \(queNo?[1])")
//        print("optionID: \(sender.titleLabel?.text!)")
        
        if self.arrRadioButton.count > 0 {
//            print("self.arrRadioButton: \(self.arrRadioButton)")
            
            for i in 0...self.arrRadioButton.count - 1 {
                let dict = self.arrRadioButton[i] as! NSMutableDictionary
                let questionNo = (dict["questionNo"] as! String)
                
                if queNo?[1] == questionNo {
                    let arrRadioButtons = (dict["radioButtons"] as! Array<UIButton>)
                    for button: UIView in (arrRadioButtons) {
                        if (button is UIButton) {
                            (button as? UIButton)?.isSelected = false
                        }
                    }
                    sender.isSelected = true

                    dict.setValue((NSString(format:"%@", (queNo?[0])!) as String), forKey: "radioButtonText")   // Update value of selected radio button (Like: "<optionID>")
                    break
                }else {
                    continue
                }
            }
            
//            print("self.arrRadioButton: \(self.arrRadioButton)")
        }
    }
    
    func questionAndCheckbox(queData: Dictionary<String, AnyObject>, queNo: String) {   // Add question with checkbox programmatically
        let dictCheckboxData = NSMutableDictionary()
        dictCheckboxData.setValue(queData, forKey: "questionData")  // Set each question data (Note: Comming from serrver side)
        dictCheckboxData.setValue(queNo, forKey: "questionNo")  // Set each question no
        
        let lblQuestion = UILabel(frame: CGRect(x: 0.0, y: defaultHeight, width: w, height: h))
        lblQuestion.numberOfLines = 0;
        lblQuestion.font = font
        
        if (queData["helpText"] as! String) == "" {
            lblQuestion.text = "Q.\(queNo) \(queData["question"] as! String)"
        }else {
            lblQuestion.text = "Q.\(queNo) \(queData["question"] as! String) (\(queData["helpText"] as! String))"
        }
        
        lblQuestion.textColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        lblQuestion.lineBreakMode = NSLineBreakMode.byWordWrapping;
        lblQuestion.sizeToFit()
        lblQuestion.frame.size.width = w
        viewCustomField.addSubview(lblQuestion)
        
        var arrOptions: Array<Dictionary<String,String>> = []
        arrOptions = queData["options"] as! Array<Dictionary<String, String>>
//        print("arrOptions: \(arrOptions)")
        
        dictCheckboxData.setValue(arrOptions, forKey: "options") // Array of optionID & optionName
        dictCheckboxData.setValue(queNo, forKey: "tag")  // Set tag
        dictCheckboxData.setValue((queData["inputType"] as! String), forKey: "inputType")  // Set input type
        dictCheckboxData.setValue("", forKey: "checkboxText") // Set selected checkbox optionID (Default: "")
        
        var temp: CGFloat = 0.0
        
        var cbw: CGFloat = 0.0
        var cbh: CGFloat = 0.0
        var th: CGFloat = 0.0
        var cbd: CGFloat = 0.0
        
        var fontTitle: UIFont!
        
        if (DELEGATE.isIPad()) {
            cbw = 25.0
            cbh = 25.0
            th = 45.0
            cbd = 8.0
            fontTitle = UIFont(name: "Roboto-Regular", size: 20.0)!
        }else {
            cbw = 18.0
            cbh = 18.0
            th = 22.0
            cbd = 8.0
            fontTitle = UIFont(name: "Roboto-Regular", size: 13.0)!
        }
        
        temp = (lblQuestion.frame.origin.y + lblQuestion.frame.size.height + d)

        if arrOptions.count > 0 {
            for i in 0...arrOptions.count - 1 {
                var dict = arrOptions[i]

                let btnCheckbox = UIButton(frame: CGRect(x: 0.0, y:(temp - ((th - cbh) / 2.0)), width: cbw, height: cbh))
                btnCheckbox.tag = 0
                btnCheckbox.setTitle("\(dict["optionID"]! as String) \(queNo)", for: .normal)
                btnCheckbox.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
                btnCheckbox.addTarget(self, action: #selector(self.onClickCheckBox), for: .touchUpInside)
                btnCheckbox.setTitleColor(UIColor.clear, for: .normal)
                viewCustomField.addSubview(btnCheckbox)
                
                let lblTitle = UILabel(frame: CGRect(x :btnCheckbox.frame.origin.x + cbw + cbd, y :temp, width: (viewCustomField.frame.size.width - (btnCheckbox.frame.origin.x + cbw + cbd)), height: th))
                lblTitle.numberOfLines = 0
                lblTitle.font = fontTitle
                lblTitle.text = "\(dict["optionName"]! as String)"
                lblTitle.textColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.sizeToFit()
                
                lblTitle.frame.size.width = (viewCustomField.frame.size.width - (btnCheckbox.frame.origin.x + cbw + cbd))
                viewCustomField.addSubview(lblTitle)
                
                btnCheckbox.frame.origin.y = lblTitle.frame.origin.y
                temp = temp + lblTitle.frame.size.height + d
            }
        }
        
        self.arrCheckbox.add(dictCheckboxData)
//        print("self.arrCheckbox: \(self.arrCheckbox)")
        
        defaultHeight = temp + d + queSpace
    }
    
    func onClickCheckBox(sender: UIButton) {    // Click even of check box
        let queNo = sender.titleLabel?.text?.characters.split{$0 == " "}.map(String.init)
//        print("queNo: \(queNo?[1])")
//        print("optionID: \(sender.titleLabel?.text!)")

        if self.arrCheckbox.count > 0 {
//            print("self.arrCheckbox: \(self.arrCheckbox)")
            
            for i in 0...self.arrCheckbox.count - 1 {
                let dict = self.arrCheckbox[i] as! NSMutableDictionary
                let questionNo = (dict["questionNo"] as! String)

                if queNo?[1] == questionNo {
                    var checkboxText = (dict["checkboxText"] as! String)
                    
                    if (sender.tag == 0) {
                        sender.tag = 5
                        sender.setBackgroundImage(UIImage(named: "checked_filled"), for: .normal)
                        
                        checkboxText = checkboxText.appending(NSString(format:"#$;%@", (queNo?[0])!) as String)
                    }else {
                        sender.tag = 0
                        sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
                        
                        checkboxText = checkboxText.replacingOccurrences(of: (NSString(format:"#$;%@", (queNo?[0])!) as String), with: "", options: NSString.CompareOptions.literal, range:nil)
                    }
                    
//                    print("checkboxText: \(checkboxText)")
                    dict.setValue(checkboxText, forKey: "checkboxText") // Update value of selected checkbox (Like: "<optionID>"#$"<optionID>"#$"<optionID>")
                    break
                }else {
                    continue
                }
            }
            
//            print("self.arrCheckbox: \(self.arrCheckbox)")
        }
    }
    
    func questionAndDropdown(queData: Dictionary<String, AnyObject>, queNo: String) {   // Add question with dropdown programmatically
        let dictDropDownData = NSMutableDictionary()
        dictDropDownData.setValue(queData, forKey: "questionData")  // Set each question data (Note: Comming from serrver side)
        dictDropDownData.setValue(queNo, forKey: "questionNo")  // Set each question no
        
        let lblQuestion = UILabel(frame: CGRect(x: 0.0, y: defaultHeight, width: w, height: h))
        lblQuestion.numberOfLines = 0
        lblQuestion.font = font
        lblQuestion.text = "Q.\(queNo) \(queData["question"] as! String)"
        lblQuestion.textColor = UIColor(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        lblQuestion.lineBreakMode = NSLineBreakMode.byWordWrapping;
        lblQuestion.sizeToFit()
        lblQuestion.frame.size.width = w
        viewCustomField.addSubview(lblQuestion)
        
        // Get list of options
        var arrOptions: Array<Dictionary<String,String>> = []
        arrOptions = queData["options"] as! Array<Dictionary<String, String>>
//        print("arrOptions: \(arrOptions)")
        
        dictDropDownData.setValue(arrOptions, forKey: "options") // Set array of optionID & optionName
        
        var arrOptionName: Array<String> = []
        if (queData["helpText"] as! String) == "" {
            arrOptionName.insert("Select", at: 0)
        }else {
            arrOptionName.insert("\(queData["helpText"] as! String)", at: 0)
        }
        
        for index in 0...arrOptions.count - 1 {
            var dict = arrOptions[index]
            arrOptionName.insert(dict["optionName"]! as String, at: index + 1)
        }
        
        dictDropDownData.setValue(arrOptionName, forKey: "optionName")   // Set array of optionName
        
        let dropDown = LBZSpinner(frame: CGRect(x: 0.0, y: (lblQuestion.frame.origin.y + lblQuestion.frame.size.height + d), width: w, height: h))
        dropDown.tag = Int(queNo)!
        dropDown.textColor = UIColor.black
        dropDown.lineColor = UIColor.black
        
        dictDropDownData.setValue(queNo, forKey: "tag")  // Set tag
        
        self.arrDropDown.add(dictDropDownData)
//        print("self.arrDropDown: \(self.arrDropDown)")
        
        if DELEGATE.isIPad() {
            dropDown.dDLTextFont = UIFont(name: "Roboto-Regular", size: 20.0)!
        }else {
            dropDown.dDLTextFont = UIFont(name: "Roboto-Regular", size: 14.0)!
        }

        dropDown.updateList(arrOptionName)
        dropDown.delegate = self

        if dropDown.selectedIndex == LBZSpinner.INDEX_NOTHING {
            dropDown.changeSelectedIndex(0)
        }
        
        viewCustomField.addSubview(dropDown)
        
        defaultHeight = dropDown.frame.origin.y + dropDown.frame.size.height + d + queSpace
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCamera(sender: UIButton) {
        let alert: UIAlertController = UIAlertController(title: APP_NAME, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let photosAction = UIAlertAction(title: "Photos", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openPhotoLibrary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(photosAction)
        alert.addAction(cancelAction)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(alert, animated: true, completion: nil)
        }else {
            popover = UIPopoverController(contentViewController: alert)
            popover.present(from: btnCamera.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        let dictSiteDetails = (DELEGATE.dictCheckIntoSite["siteDetail"] as! NSDictionary)
        
        txtFirstName.text = txtFirstName.text?.trimmingCharacters(in: .whitespaces)
        if (dictSiteDetails["isRequireName"] as! String) == "0" {
            // First name must be required
            if txtFirstName.text == "" {
                DELEGATE.showPopup(title: Empty_First_Name, type: .error)
                return
            }else {
                if DELEGATE.isValidCharecter(strchare: txtFirstName.text!) == false {
                    DELEGATE.showPopup(title: Only_Characters_Name, type: .error)
                    return
                }
            }
        }else {
            // First name not required
            if txtFirstName.text != "" {
                if DELEGATE.isValidCharecter(strchare: txtFirstName.text!) == false {
                    DELEGATE.showPopup(title: Only_Characters_Name, type: .error)
                    return
                }
            }
        }
        
        txtLastName.text = txtLastName.text?.trimmingCharacters(in: .whitespaces)
        if txtLastName.text != "" {
            if DELEGATE.isValidCharecter(strchare: txtLastName.text!) == false {
                DELEGATE.showPopup(title: Only_Characters_Name, type: .error)
                return
            }
        }

        txtOrganization.text = txtOrganization.text?.trimmingCharacters(in: .whitespaces)
        if (dictSiteDetails["isRequireOrganization"] as! String) == "0" {
            // Organization must be required
            if txtOrganization.text == "" {
                DELEGATE.showPopup(title: Empty_Organization, type: .error)
                return
            }
        }
        
        txtPhoneNumber.text = txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces)
        if txtPhoneNumber.text == "" {
            DELEGATE.showPopup(title: Empty_Phone_Number, type: .error)
            return
        }else {
            if (txtPhoneNumber.text?.characters.count)! <= 9 {
                DELEGATE.showPopup(title: Mini_Characters_Contact, type: .error)
                return
            }
        }
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
            return
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
                return
            }
        }
        
        group.text = group.text.trimmingCharacters(in: .whitespaces)
        if (dictSiteDetails["isRequireGroup"] as! String) == "0" {
            // Group must be required
            if group.text == "Group" {
                DELEGATE.showPopup(title: Empty_Group, type: .error)
                return
            }
        }
        
        // Remove all objects from arrCustomField
        self.arrCustomField.removeAllObjects()

        if (self.arrQuestions.count > 0) {
            // Add dropdown data
            if self.arrDropDown.count > 0 {
                for i in 0...self.arrDropDown.count - 1 {
                    let dict = self.arrDropDown[i] as! NSMutableDictionary
                    self.arrCustomField.add(dict)
                }
            }
            
            // Add textfield data
            if self.arrTextField.count > 0 {
                for i in 0...self.arrTextField.count - 1 {
                    let dict = self.arrTextField[i] as! NSMutableDictionary
                    self.arrCustomField.add(dict)
                }
            }
            
            // Add checkbox data
            if self.arrCheckbox.count > 0 {
                for i in 0...self.arrCheckbox.count - 1 {
                    let dict = self.arrCheckbox[i] as! NSMutableDictionary
                    self.arrCustomField.add(dict)
                }
            }
            
            // Add radio button data
            if self.arrRadioButton.count > 0 {
                for i in 0...self.arrRadioButton.count - 1 {
                    let dict = self.arrRadioButton[i] as! NSMutableDictionary
                    self.arrCustomField.add(dict)
                }
            }
            
            let sortedArray = self.arrCustomField.sortedArray(using: [NSSortDescriptor(key: "questionNo", ascending: true)])
            
            self.arrCustomField.removeAllObjects()
            self.arrCustomField = NSMutableArray(array: sortedArray)
//            print("self.arrCustomField: \(self.arrCustomField)")
        }
        
        var strAnswer: String = ""
        
        if (self.arrCustomField.count > 0) {
            for index in 0...self.arrCustomField.count - 1 {
                let dict = self.arrCustomField[index] as! Dictionary<String, AnyObject>
                print("dict: \(dict)")
                
                let dictQueData = dict["questionData"] as! Dictionary<String, AnyObject>
//                print("dictQueData: \(dictQueData)")
                
                let strFieldType = dictQueData["fieldType"] as! String
                let isRequired = dictQueData["isRequired"] as! String
                
                if strFieldType == "1" {
                    // Dropdown
                    if isRequired == "0" {
                        if (dict["selectedOptionID"] as! String) == "" {
                            DELEGATE.showPopup(title: "Please answer the question number \(dict["questionNo"] as! String)", type: .error)
                            return
                        }
                    }
                }else if strFieldType == "2" {
                    // Checkbox
                    if isRequired == "0" {
                        if (dict["checkboxText"] as! String) == "" {
                            DELEGATE.showPopup(title: "Please answer the question number \(dict["questionNo"] as! String)", type: .error)
                            return
                        }
                    }
                }else if strFieldType == "3" {
                    // Radio Button
                    if isRequired == "0" {
                        if (dict["radioButtonText"] as! String) == "" {
                            DELEGATE.showPopup(title: "Please answer the question number \(dict["questionNo"] as! String)", type: .error)
                            return
                        }
                    }
                }else if strFieldType == "4" {
                    // File
                    
                }else if strFieldType == "5" {
                    // Text
                    if isRequired == "0" {
                        if (dict["textfieldText"] as! String) == "" {
                            DELEGATE.showPopup(title: "Please answer the question number \(dict["questionNo"] as! String)", type: .error)
                            return
                        }
                    }
                    
                    let minChar = Int((dictQueData["minChar"] as! String))!
                    let length = (dict["textfieldText"] as! String).characters.count
                    if length > 0 {
                        if minChar > 0 {
                            if minChar > length {
                                DELEGATE.showPopup(title: "Please enter minimum character \(minChar) to question number \(dict["questionNo"] as! String)", type: .error)
                                return
                            }
                        }
                    }
                }else {
                    
                }

                var string: String = ""
                
                if strFieldType == "1" {
                    // Dropdown
                    if (dict["selectedOptionID"] as! String) != "" {
                        string = ("\(strFieldType)#;")
                        string = string.appending("\(dict["selectedOptionID"] as! String)")
                        string = string.appending("#;\("")")
                    }
                }else if strFieldType == "2" {
                    // Checkbox
                    if (dict["checkboxText"] as! String) != "" {
                        string = ("\(strFieldType)#;")
                        
                        let str = dict["checkboxText"] as! String
                        let result = String(str.characters.dropFirst(3))

                        string = string.appending("\(result)")
                        string = string.appending("#;\("")")
                    }
                }else if strFieldType == "3" {
                    // Radio Button
                    if (dict["radioButtonText"] as! String) != "" {
                        string = ("\(strFieldType)#;")
                        string = string.appending("\(dict["radioButtonText"] as! String)")
                        string = string.appending("#;\("")")
                    }
                }else if strFieldType == "4" {
                    // File
                    
                }else if strFieldType == "5" {
                    // Textfield
                    if (dict["textfieldText"] as! String) != "" {
                        string = ("\(strFieldType)#;")
                        string = string.appending("#;\(dict["textfieldText"] as! String)")
                    }
                }else {
                    
                }
                
//                print("string: \(string)")
                if string != "" {
                    strAnswer = strAnswer.appending(",\(string)")
                }
            }
            
            strAnswer = String(strAnswer.characters.dropFirst())
        }
        
//        print("strAnswer: \(strAnswer)")

        DELEGATE.dictCheckIntoSite.setValue(txtFirstName.text!, forKey: "firstName")
        DELEGATE.dictCheckIntoSite.setValue(txtLastName.text!, forKey: "lastName")
        DELEGATE.dictCheckIntoSite.setValue(strCompanyID, forKey: "companyID")
        DELEGATE.dictCheckIntoSite.setValue(strCompanyName, forKey: "companyName")
        DELEGATE.dictCheckIntoSite.setValue(txtPhoneNumber.text!, forKey: "contactNumber")
        DELEGATE.dictCheckIntoSite.setValue(txtEmail.text!, forKey: "email")
        DELEGATE.dictCheckIntoSite.setValue(strRoleID, forKey: "roleID")
        DELEGATE.dictCheckIntoSite.setValue(strRoleName, forKey: "roleName")
        DELEGATE.dictCheckIntoSite.setValue(strAnswer, forKey: "customAnswers")
        DELEGATE.dictCheckIntoSite.setValue(strImageData, forKey: "image")
        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: AuthorizeYourSelfVC = storyboard.instantiateViewController(withIdentifier: "AuthorizeYourSelfVC") as! AuthorizeYourSelfVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextView
    func textViewDidBeginEditing(_ textView: UITextView) {
        txtViewTemp = textView
        
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if self.arrTextField.count > 0 {
            for i in 0...self.arrTextField.count - 1 {
                let dict = self.arrTextField[i] as! NSMutableDictionary
                let tag = (dict["tag"] as! String)
                let maxChar = Int((dict["maxChar"] as! String))!
                
                if Int(tag)! == txtViewTemp.tag {
                    let strInputType = (dict["inputType"] as! String)
                    if strInputType == "1" {    // Any characters
                        if maxChar > 0 {
                            let currentString: NSString = txtViewTemp.text! as NSString
                            let newString: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
                            return newString.length <= maxChar
                        }else {
                            return true
                        }
                    }else if strInputType == "2" {  // Text and number
                        let invalidCharacters = CharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
                        if text.rangeOfCharacter(from: invalidCharacters, options: [], range: text.startIndex ..< text.endIndex) != nil {
                            return false
                        }else {
                            if maxChar > 0 {
                                let currentString: NSString = txtViewTemp.text! as NSString
                                let newString: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
                                return newString.length <= maxChar
                            }else {
                                return true
                            }
                        }
                    }else if strInputType == "3" {  // Number only
                        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                        if text.rangeOfCharacter(from: invalidCharacters, options: [], range: text.startIndex ..< text.endIndex) != nil {
                            return false
                        }else {
                            if maxChar > 0 {
                                let currentString: NSString = txtViewTemp.text! as NSString
                                let newString: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
                                return newString.length <= maxChar
                            }else {
                                return true
                            }
                        }
                    }else if strInputType == "4" {  // Text only
                        let invalidCharacters = CharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
                        if text.rangeOfCharacter(from: invalidCharacters, options: [], range: text.startIndex ..< text.endIndex) != nil {
                            return false
                        }else {
                            if maxChar > 0 {
                                let currentString: NSString = txtViewTemp.text! as NSString
                                let newString: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
                                return newString.length <= maxChar
                            }else {
                                return true
                            }
                        }
                    }else { // No input type (Other)
                        return true
                    }
                }else {
                    continue
                }
            }
            
//            print("self.arrTextField: \(self.arrTextField)")
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.arrTextField.count > 0 {
//            print("self.arrTextField: \(self.arrTextField)")
            
            for i in 0...self.arrTextField.count - 1 {
                let dict = self.arrTextField[i] as! NSMutableDictionary
                let tag = (dict["tag"] as! String)
                let minChar = Int((dict["minChar"] as! String))!
                
                if Int(tag)! == txtViewTemp.tag {
                    txtViewTemp.text = txtViewTemp.text?.trimmingCharacters(in: .whitespaces)
                    
                    if txtViewTemp.text == "" {
                        let dictQueData = dict["questionData"] as! NSDictionary
                        if (dictQueData["helpText"] as! String) == "" {
                            txtViewTemp.text = "Enter Text"
                        }else {
                            txtViewTemp.text = ("\(dictQueData["helpText"] as! String)")
                        }
                        txtViewTemp.textColor = UIColor.lightGray
                    }
                    
                    textView.text = textView.text?.trimmingCharacters(in: .whitespaces)
                    let length = textView.text!.characters.count
                    if length > 0 {
                        if minChar > 0 {
                            if minChar > length {
                                DELEGATE.showPopup(title: "Please enter minimum character \(minChar) to question number \(dict["questionNo"] as! String)", type: .error)
                            }
                        }
                    }
                    
                    dict.setValue(txtViewTemp.text!, forKey: "textfieldText")   // Update value of textfield
                    break
                }else {
                    continue
                }
            }
            
//            print("self.arrTextField: \(self.arrTextField)")
        }
    }
    
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == txtFirstName || textField == txtLastName || textField == txtEmail || textField == txtPhoneNumber || textField == txtOrganization) {
            txtTemp = textField as! ACFloatingTextfield
        }else {
            txtFieldTemp = textField
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == txtFirstName || textField == txtLastName || textField == txtEmail) {
            return true
        }else if (textField == txtPhoneNumber) {
            let invalidCharacters = CharacterSet(charactersIn: "+0123456789").inverted
            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                return false
            }else {
                let currentString: NSString = txtPhoneNumber.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= Constant.maxLength
            }
        }else if (textField == txtOrganization) {
            if string.isEmpty {
                strSearchCompany = String(strSearchCompany.characters.dropLast())
            }else {
                strSearchCompany = textField.text!+string
            }
            
            let predicate = NSPredicate(format: "SELF.companyName CONTAINS[cd] %@", strSearchCompany)
            let array = (arrCompany as NSArray).filtered(using: predicate)
            
            if array.count > 0 {
                self.arrSearchCompany.removeAll(keepingCapacity: true)
                self.arrSearchCompany = array as! Array<Dictionary<String,String>>
                self.viewCompany.isHidden = false
            }else {
                self.arrSearchCompany.removeAll()
                self.viewCompany.isHidden = true
            }
            
            self.tblCompany.reloadData()
            return true
        }else {
            if self.arrTextField.count > 0 {
                for i in 0...self.arrTextField.count - 1 {
                    let dict = self.arrTextField[i] as! NSMutableDictionary
                    let tag = (dict["tag"] as! String)
                    let maxChar = Int((dict["maxChar"] as! String))!
                    
                    if Int(tag)! == txtFieldTemp.tag {
                        let strInputType = (dict["inputType"] as! String)
                        
                        if strInputType == "1" {    // Any characters
                            if maxChar > 0 {
                                let currentString: NSString = txtFieldTemp.text! as NSString
                                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                                return newString.length <= maxChar
                            }else {
                                return true
                            }
                        }else if strInputType == "2" {  // Text and number
                            let invalidCharacters = CharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
                            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                                return false
                            }else {
                                if maxChar > 0 {
                                    let currentString: NSString = txtFieldTemp.text! as NSString
                                    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                                    return newString.length <= maxChar
                                }else {
                                    return true
                                }
                            }
                        }else if strInputType == "3" {  // Number only
                            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
                            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                                return false
                            }else {
                                if maxChar > 0 {
                                    let currentString: NSString = txtFieldTemp.text! as NSString
                                    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                                    return newString.length <= maxChar
                                }else {
                                    return true
                                }
                            }
                        }else if strInputType == "4" {  // Text only
                            let invalidCharacters = CharacterSet(charactersIn: " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
                            if string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) != nil {
                                return false
                            }else {
                                if maxChar > 0 {
                                    let currentString: NSString = txtFieldTemp.text! as NSString
                                    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                                    return newString.length <= maxChar
                                }else {
                                    return true
                                }
                            }
                        }else { // No input type (Other)
                            return true
                        }
                    }else {
                        continue
                    }
                }
                
                print("self.arrTextField: \(self.arrTextField)")
            }
            
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == txtFirstName || textField == txtLastName || textField == txtEmail || textField == txtPhoneNumber || textField == txtOrganization) {
            if (textField == txtOrganization) {
                txtOrganization.text = txtOrganization.text?.trimmingCharacters(in: .whitespaces)
                self.checkCompanyIDName(name: txtOrganization.text!)
            }
        }else {
            if self.arrTextField.count > 0 {
                print("self.arrTextField: \(self.arrTextField)")
                
                for i in 0...self.arrTextField.count - 1 {
                    let dict = self.arrTextField[i] as! NSMutableDictionary
                    let tag = (dict["tag"] as! String)
                    let minChar = Int((dict["minChar"] as! String))!
                    
                    if Int(tag)! == txtFieldTemp.tag {
                        txtFieldTemp.text = txtFieldTemp.text?.trimmingCharacters(in: .whitespaces)
                        
                        let length = txtFieldTemp.text!.characters.count
                        if length > 0 {
                            if minChar > 0 {
                                if minChar > length {
                                    DELEGATE.showPopup(title: "Please enter minimum character \(minChar) to question number \(dict["questionNo"] as! String)", type: .error)
                                }
                            }
                        }
                        
                        dict.setValue(txtFieldTemp.text!, forKey: "textfieldText")   // Update value of textfield
                        break
                    }else {
                        continue
                    }
                }
                
                print("self.arrTextField: \(self.arrTextField)")
            }
        }
    }
    
    // MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearchCompany.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationCell", for: indexPath) as! SearchLocationCell
        cell.delegate = self
        cell.idx = indexPath.row
        
        var dict: Dictionary<String,String> = self.arrSearchCompany[indexPath.row]
        cell.lblLocationName.text = NSString(format:"%@", dict["companyName"]!) as String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: SearchLocationCell
    func getLocationDetails(index: NSInteger) {
        var dict: Dictionary<String,String> = self.arrSearchCompany[index]
        txtOrganization.text = NSString(format:"%@", dict["companyName"]!) as String
        
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        self.viewCompany.isHidden = true
    }
    
    // MARK: - LBZSpinner
    func spinnerOpen(_ spinner:LBZSpinner) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        print("Open")
    }
    
    func spinnerClose(_ spinner:LBZSpinner) {
        print("Close")
    }
    
    func spinnerChoose(_ spinner:LBZSpinner, index:Int, value:String) {
        print("Tag: \(spinner.tag) Index: \(index) Value: \(value)")
        
        if (spinner == group) {
            if index == 0 {
                strRoleID = ""
            }else {
                var dict: Dictionary<String,String> = self.arrRole[index - 1]
                strRoleID = dict["roleID"]! as String
            }
            
            strRoleName = value
            print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
        }
        
        if self.arrDropDown.count > 0 {
//            print("self.arrDropDown: \(self.arrDropDown)")
            
            for i in 0...self.arrDropDown.count - 1 {
                let dict = self.arrDropDown[i] as! NSMutableDictionary
                let tag = (dict["tag"] as! String)
                
                if Int(tag)! == spinner.tag {
                    if index == 0 {
                        dict.setValue("", forKey: "selectedOptionID")   // Update value of selected dropdown menu (optionID)
                        dict.setValue("", forKey: "selectedOptionName") // Update value of selected dropdown menu (optionName)
                    }else {
                        let options = (dict["options"] as! Array<Dictionary<String, String>>)
                        
                        var dictOption: Dictionary<String,String> = options[index - 1]
                        dict.setValue((dictOption["optionID"]! as String), forKey: "selectedOptionID")  // Update value of selected dropdown menu (optionID)
                        dict.setValue((dictOption["optionName"]! as String), forKey: "selectedOptionName")  // Update value of selected dropdown menu (optionName)
                    }
                    break
                }else {
                    continue
                }
            }
            
//            print("self.arrDropDown: \(self.arrDropDown)")
        }
    }
    
    // MARK: UIImagePickerController
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var chosenImage = info[UIImagePickerControllerEditedImage ] as! UIImage
        chosenImage = DELEGATE.resizeImage(image: chosenImage, newWidth: 500.0)
        
        imgView.image = chosenImage
        imgPlaceholder.isHidden = true
        
        imageData = UIImageJPEGRepresentation(chosenImage, 1) as NSData?
        strImageData = (imageData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) as String!)!
        
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

