//
//  WelcomeVC.swift
//

import UIKit

class WelcomeVC: UIViewController {
    
    // MARK: - Variables
    var isTerms: Bool = false
    
    // MARK: - UI Controls
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnCheckbox: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        let dictSiteDetails = (DELEGATE.dictCheckIntoSite["siteDetail"] as! NSDictionary)
        lblTitle.text = "Welcome to \(dictSiteDetails["siteName"] as! String)!"
        
        btnDone.layer.cornerRadius = DELEGATE.setCorner()
        btnDone.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnDone.layer.borderWidth = 1.0
        
        btnCheckbox.tag = 0
        btnCheckbox.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
    }
    
    // MARK: - UI Methods
    func checkIntoSite() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, CHECK_INTO_SITE)

        let dictSiteDetails = (DELEGATE.dictCheckIntoSite["siteDetail"] as! NSDictionary)
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String,
            "siteID" : (dictSiteDetails["siteID"] as! String),
            "firstName" : (DELEGATE.dictCheckIntoSite.value(forKey: "firstName") as! String),
            "lastName" : (DELEGATE.dictCheckIntoSite.value(forKey: "lastName") as! String),
            "companyID" : (DELEGATE.dictCheckIntoSite.value(forKey: "companyID") as! String),
            "companyName" : (DELEGATE.dictCheckIntoSite.value(forKey: "companyName") as! String),
            "contactNumber" : (DELEGATE.dictCheckIntoSite.value(forKey: "contactNumber") as! String),
            "email" : (DELEGATE.dictCheckIntoSite.value(forKey: "email") as! String),
            "roleID" : (DELEGATE.dictCheckIntoSite.value(forKey: "roleID") as! String),
            "image" : (DELEGATE.dictCheckIntoSite.value(forKey: "image") as! String),
            "customAnswers" : (DELEGATE.dictCheckIntoSite.value(forKey: "customAnswers") as! String),
            "notifyEmail" : (DELEGATE.dictCheckIntoSite.value(forKey: "notifyEmail") as! String),
            "isEmailOnEntry" : (DELEGATE.dictCheckIntoSite.value(forKey: "isEmailOnEntry") as! String),
            "isEmailOnExit" : (DELEGATE.dictCheckIntoSite.value(forKey: "isEmailOnExit") as! String),
            "isAutoCheckIn" : (DELEGATE.dictCheckIntoSite.value(forKey: "isAutoCheckIn") as! String),
            "isSingleCheckIn" : (DELEGATE.dictCheckIntoSite.value(forKey: "isSingleCheckIn") as! String),
            "isFullTraking" : (DELEGATE.dictCheckIntoSite.value(forKey: "isFullTraking") as! String)
        ]
        
        print("Parameters: \(parameters)")
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageCreateNewSiteData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageCreateNewSiteData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "success" {
                let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
                let objThanksVC: ThanksVC = storyboard.instantiateViewController(withIdentifier: "ThanksVC") as! ThanksVC
                self.navigationController?.pushViewController(objThanksVC, animated: true)
            }else {
                
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDone(sender: UIButton) {
        if isTerms == false {
            DELEGATE.showPopup(title: Empty_CheckBox, type: .error)
            return
        }
        
        self.checkIntoSite()
    }
    
    @IBAction func onClickCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled.png"), for: .normal)
            
            isTerms = true
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
            
            isTerms = false
        }
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

