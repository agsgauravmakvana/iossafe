//
//  ForgotPassword.swift
//

import UIKit

class ForgotPassword: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var txtEmail: ACFloatingTextfield!
    @IBOutlet var btnForgotPass: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnForgotPass.layer.cornerRadius = DELEGATE.setCorner()
        btnForgotPass.layer.borderColor = UIColor.white.cgColor
        btnForgotPass.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    func forgotPassword(email: String) {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, FORGOT_PASSWORD)
        let parameters: [AnyHashable: Any] = [
            "email": email
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageForgotPasswordData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageForgotPasswordData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                _ = self.navigationController?.popViewController(animated: true)
            }else {
                
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onClickForgotPass(sender: UIButton) {
        if txtTemp != nil {
            txtTemp.resignFirstResponder()
        }
        
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        if txtEmail.text == "" {
            DELEGATE.showPopup(title: Empty_Email, type: .error)
        }else {
            if DELEGATE.isValidEmail(strEmailId: txtEmail.text!) == false {
                DELEGATE.showPopup(title: Valid_Email, type: .error)
            }else {
                self.forgotPassword(email: txtEmail.text!)
            }
        }
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }

    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

