//
//  MoreVC.swift
//

import UIKit

class MoreVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnLogout: UIButton!
    
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogout.layer.cornerRadius = DELEGATE.setCorner()
        btnLogout.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnLogout.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickMyInfo(_ sender: Any) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: YourInformationVC = storyboard.instantiateViewController(withIdentifier: "YourInformationVC") as! YourInformationVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickFAQ(_ sender: Any) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: FAQVC = storyboard.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickSetting(_ sender: Any) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SettingVC = storyboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickLogout(sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isLogin")
        
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
