//
//  SitesAdminCell.swift
//

import UIKit

protocol CustomQuestionCellProtocol : class {
    func deleteRow(index: NSInteger)
}

class CustomQuestionCell: UITableViewCell, UITextFieldDelegate {
    
    // MARK: - Variables
    var delegate : CustomQuestionCellProtocol?
    var idx: NSInteger = 0
    
    // MARK: - UIControls
    @IBOutlet var txtOption: ACFloatingTextfield!
    @IBOutlet var btnDelete: UIButton!
    
    var txtTemp: ACFloatingTextfield!
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickDelete(_ sender: Any) {
        delegate?.deleteRow(index: idx)
    }
    
    // MARK: - Delegate Methods
    // MARK: UITextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtTemp = textField as! ACFloatingTextfield
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}
