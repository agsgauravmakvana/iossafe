//
//  PaymentSuccessVC.swift
//

import UIKit

class PaymentSuccessVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnSuccess: UIButton!
    @IBOutlet var btnAllSites: UIButton!
    @IBOutlet var btnAddSite: UIButton!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        btnSuccess.layer.cornerRadius = DELEGATE.setCorner()
        btnSuccess.layer.borderColor = UIColor(red: 27.0/255.0, green: 176.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        btnSuccess.layer.borderWidth = 1.0
        
        btnAllSites.layer.cornerRadius = DELEGATE.setCorner()
        btnAllSites.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnAllSites.layer.borderWidth = 1.0
        
        btnAddSite.layer.cornerRadius = DELEGATE.setCorner()
        btnAddSite.layer.borderColor = UIColor(red: 28.0/255.0, green: 184.0/255.0, blue: 211.0/255.0, alpha: 1.0).cgColor
        btnAddSite.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddSite(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: AddNewSiteVC = storyboard.instantiateViewController(withIdentifier: "AddNewSiteVC") as! AddNewSiteVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickAllSites(sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isLogin")
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: SafeMate = storyboard.instantiateViewController(withIdentifier: "SafeMate") as! SafeMate
        objVC.selectedIndex = 1
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

