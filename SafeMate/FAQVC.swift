//
//  FAQVC.swift
//

import UIKit

class FAQVC: UIViewController, UITableViewDataSource, UITableViewDelegate, FAQCellProtocol {

    // MARK: - Variables
    var arrFAQ: NSMutableArray = []
    var currentQueHeight: CGFloat!
    var currentAnsHeight: CGFloat!
    var widthQue: CGFloat!
    var widthAns: CGFloat!
    
    // MARK: - UIControls
    @IBOutlet weak var tblFAQ: UITableView!

    // MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentQueHeight = 40.0
        currentAnsHeight = 0.0
        
        if DELEGATE.isIPad() {
            widthQue = 698.0
            widthAns = 728.0
        }else {
            widthQue = 277.0
            widthAns = 300.0
        }
        
        // For testing
        arrFAQ.add(FAQData(question: "What does safemate do does safemate do does safemate do?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu."))
        arrFAQ.add(FAQData(question: "What is Lorem Ipsum?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))
        arrFAQ.add(FAQData(question: "What do we use it?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))
        arrFAQ.add(FAQData(question: "Where does it come from does it come from does it come from does it come from?", answer: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))
        arrFAQ.add(FAQData(question: "Where can I get some?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))
        arrFAQ.add(FAQData(question: "What does safemate do does safemate do does safemate do?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu."))
        arrFAQ.add(FAQData(question: "What is Lorem Ipsum?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))
        arrFAQ.add(FAQData(question: "What do we use it?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))
        arrFAQ.add(FAQData(question: "Where does it come from does it come from does it come from does it come from?", answer: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))
        arrFAQ.add(FAQData(question: "Where can I get some?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))
        arrFAQ.add(FAQData(question: "What does safemate do does safemate do does safemate do?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu."))
        arrFAQ.add(FAQData(question: "What is Lorem Ipsum?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))
        arrFAQ.add(FAQData(question: "What do we use it?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))
        arrFAQ.add(FAQData(question: "Where does it come from does it come from does it come from does it come from?", answer: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))
        arrFAQ.add(FAQData(question: "Where can I get some?", answer: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))
        
        tblFAQ.estimatedRowHeight = 100
        tblFAQ.rowHeight = UITableViewAutomaticDimension
    }

    // MARK: - UI Methods
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.tblFAQ.numberOfSections
            let numberOfRows = self.tblFAQ.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tblFAQ.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
        }
    }
    
    func getFAQList() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@", BASE_URL)
        let parameters: [AnyHashable: Any] = [
            "key": "value",
            "key": "value"
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageFAQData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageFAQData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Delegate Methods
    //MARK: UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFAQ.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath) as! FAQCell
        
        cell.setValues(arrFAQ[(indexPath as NSIndexPath).row] as! FAQData)
        
        cell.delegate = self
        cell.idxPath = indexPath
        
        cell.lblQuestion.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblQuestion.sizeToFit()
        
        cell.lblAnswer.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.lblAnswer.sizeToFit()
        
        cell.lblQuestion.frame.size.width = widthQue
        cell.btnDetails.frame = cell.lblQuestion.frame
        cell.lblAnswer.frame.origin.y = cell.lblQuestion.frame.origin.y + cell.lblQuestion.frame.size.height + 10.0
        cell.lblAnswer.frame.size.width = widthAns
        
        currentQueHeight = cell.lblQuestion.frame.height + 22.0
        currentAnsHeight = cell.lblAnswer.frame.height
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let faqData = arrFAQ[(indexPath as NSIndexPath).row] as! FAQData
        
        if faqData.faqShown == true {
            return currentQueHeight + currentAnsHeight
        } else {
            return currentQueHeight
        }
    }
    
    // MARK: FAQCellProtocol
    func getFAQDetails(indexPath: IndexPath) {
        let faqData = arrFAQ[(indexPath as NSIndexPath).row] as! FAQData
        let faqShown = faqData.faqShown
        faqData.faqShown = !faqShown
        
        let cell = tblFAQ.cellForRow(at: indexPath) as! FAQCell
        
        cell.lblQuestion.frame.size.width = widthQue
        cell.btnDetails.frame = cell.lblQuestion.frame
        cell.lblAnswer.frame.origin.y = cell.lblQuestion.frame.origin.y + cell.lblQuestion.frame.size.height + 10.0
        cell.lblAnswer.frame.size.width = widthAns
        
        currentQueHeight = cell.lblQuestion.frame.height
        currentAnsHeight = cell.lblAnswer.frame.height
        
        tblFAQ.reloadData()
    }
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
