//
//  RemoveVC.swift
//

import UIKit

class RemoveVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnRemove: UIButton!
    @IBOutlet var btnPause: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var viewProfile: UIView!
    @IBOutlet var scrollBilling: UIScrollView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewProfile.layer.cornerRadius = viewProfile.frame.width / 2.0
        viewProfile.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        viewProfile.layer.borderWidth = 1.5
        
        btnRemove.layer.cornerRadius = DELEGATE.setCorner()
        btnRemove.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnRemove.layer.borderWidth = 1.0
        
        btnPause.layer.cornerRadius = DELEGATE.setCorner()
        btnPause.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnPause.layer.borderWidth = 1.0
        
        btnDelete.layer.cornerRadius = DELEGATE.setCorner()
        btnDelete.layer.borderColor = UIColor(red: 245.0/255.0, green: 91.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        btnDelete.layer.borderWidth = 1.0
        
        if DELEGATE.isIPad() {
            scrollBilling.contentSize = CGSize(width: self.view.frame.size.width, height: 1125.0)
        }else {
            scrollBilling.contentSize = CGSize(width: self.view.frame.size.width, height: 655.0)
        }
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickRemove(sender: UIButton) {
        
    }
    
    @IBAction func onClickPause(sender: UIButton) {
        
    }
    
    @IBAction func onClickDelete(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: AccountDeletedVC = storyboard.instantiateViewController(withIdentifier: "AccountDeletedVC") as! AccountDeletedVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

