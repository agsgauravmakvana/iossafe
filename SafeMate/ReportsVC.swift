//
//  ReportsVC.swift
//

import UIKit

class ReportsVC: UIViewController , LBZSpinnerDelegate {
    
    // MARK: - Variables
    var font: UIFont! = nil
    var strSiteID: String = ""
    var strSiteName: String = ""
    var arrSites: Array<Dictionary<String,String>> = []
    var arrSiteNames: Array<String> = []
    
    var strTime: String = ""
    var arrTime: Array<String> = []
    
    var strRoleID: String = ""
    var strRoleName: String = ""
    var arrRole: Array<Dictionary<String,String>> = []
    var arrRoleName: Array<String> = []
    
    var strPeopleID: String = ""
    var strPeopleName: String = ""
    var arrPeoples: Array<Dictionary<String,String>> = []
    var arrPeopleNames: Array<String> = []
    
    // MARK: - UI Controls
    @IBOutlet var btnRun: UIButton!
    
    @IBOutlet var selectSite: LBZSpinner!
    @IBOutlet var selectTime: LBZSpinner!
    @IBOutlet var selectRole: LBZSpinner!
    @IBOutlet var selectPeople: LBZSpinner!
   
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnRun.layer.cornerRadius = DELEGATE.setCorner()
        btnRun.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnRun.layer.borderWidth = 1.0
        
        if DELEGATE.isIPad() {
            font = UIFont(name: "Roboto-Regular", size: 20.0)!
        }else {
            font = UIFont(name: "Roboto-Regular", size: 14.0)!
        }
        
        self.arrSiteNames.insert("Select", at: 0)
        self.setPopupSelectSite()
        self.getSiteList()
        
        self.setPopupSelectTime()
        
        self.arrRoleName.insert("Select", at: 0)
        self.setPopupSelectRole()
        
        self.arrPeopleNames.insert("Select", at: 0)
        self.setPopupSelectPepople()
    }
    
    // MARK: - UI Methods
    func setPopupSelectSite() {
        selectSite.updateList(self.arrSiteNames)
        selectSite.delegate = self
        selectSite.dDLTextFont = font
        
        if selectSite.selectedIndex == LBZSpinner.INDEX_NOTHING {
            selectSite.changeSelectedIndex(0)
        }
    }
    
    func getSiteList() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_SITE_LIST)
        
        let parameters: [AnyHashable: Any] = [
            "userID" : (UserDefaults.standard.value(forKey: "UserID")) as! String
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSiteListData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
            
            self.getRoles()
        })
    }
    
    func manageSiteListData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "site not found" {
                DELEGATE.showPopup(title: msg, type: .error)
            }else {
                let dictSiteListInfo = dictData["siteListInfo"] as! NSDictionary
                
                self.arrSites = dictSiteListInfo["siteList"] as! Array<Dictionary<String, String>>
                print("arrSites: \(self.arrSites)")
                
                for index in 0...self.arrSites.count - 1 {
                    var dict = self.arrSites[index]
                    self.arrSiteNames.insert(dict["siteName"]! as String, at: index + 1)
                }
                selectSite.updateList(self.arrSiteNames)
            }
        }
        
        self.getRoles()
    }
    
    func setPopupSelectTime() {
        let arrTime = ["Select","Today","Yesterday","This Week","This Month","Last Month","This Year","All Time"]
        
        selectTime.updateList(arrTime)
        selectTime.delegate = self
        selectTime.dDLTextFont = font
        
        if selectTime.selectedIndex == LBZSpinner.INDEX_NOTHING {
            selectTime.changeSelectedIndex(0)
        }
    }
    
    func setPopupSelectRole() {
        selectRole.updateList(self.arrRoleName)
        selectRole.delegate = self
        selectRole.dDLTextFont = font
        
        if selectRole.selectedIndex == LBZSpinner.INDEX_NOTHING {
            selectRole.changeSelectedIndex(0)
        }
    }
    
    func getRoles() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_ROLES)
        
        manager.post(url, parameters: nil, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageRolesData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageRolesData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
        }else {
            if msg == "Success" {
                self.arrRole = dictData["roleList"] as! Array<Dictionary<String, String>>
                print("arrRole: \(self.arrRole)")
                
                for index in 0...self.arrRole.count - 1 {
                    var dict = self.arrRole[index]
                    self.arrRoleName.insert(dict["roleName"]! as String, at: index + 1)
                }
                selectRole.updateList(self.arrRoleName)
            }else {
                
            }
        }
    }
    
    func setPopupSelectPepople() {
        selectPeople.updateList(self.arrPeopleNames)
        selectPeople.delegate = self
        selectPeople.dDLTextFont = font
        
        if selectPeople.selectedIndex == LBZSpinner.INDEX_NOTHING {
            selectPeople.changeSelectedIndex(0)
        }
    }
    
    func getSitePeople() {
        if !DELEGATE.checkInternetConnection() {
            return
        }
        
        DELEGATE.showLoading(view: self.view)
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        let url = String(format: "%@%@", BASE_URL, GET_SITE_PEOPLE)
        let parameters: [AnyHashable: Any] = [
            "siteID": strSiteID,
            "roleID": strRoleID,
            "userID": (UserDefaults.standard.value(forKey: "UserID")) as! String
        ]
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            requestOperation, response in
            
            DELEGATE.hideLoading(view: self.view)
            
            let dictData: NSDictionary = response as! NSDictionary
            self.performSelector(onMainThread: #selector(self.manageSitePeopleData(dictData:)), with: dictData, waitUntilDone: false)
        }, failure: {
            requestOperation, error in
            
            DELEGATE.hideLoading(view: self.view)
            DELEGATE.showPopup(title: "\(error.localizedDescription)", type: .error)
        })
    }
    
    func manageSitePeopleData(dictData: NSDictionary) {
        print("dictData : \(dictData)")
        
        let status: String = dictData["status"] as! String
        let msg: String = dictData["msg"] as! String
        
        if status == "0" {
            DELEGATE.showPopup(title: msg, type: .error)
            
            self.arrPeopleNames.removeAll()
            self.arrPeopleNames.insert("Select", at: 0)
            
            self.arrPeoples.removeAll()
            selectPeople.updateList(self.arrPeopleNames)
        }else {
            if msg == "success" {
                self.arrPeoples = dictData["userList"] as! Array<Dictionary<String, String>>
                print("arrPeoples: \(self.arrPeoples)")
                
                if self.arrPeoples.count > 0 {
                    for index in 0...self.arrPeoples.count - 1 {
                        var dict = self.arrPeoples[index]
                        self.arrPeopleNames.insert("\(dict["firstName"]! as String) \(dict["lastName"]! as String) (\(dict["companyName"]! as String))", at: index + 1)
                    }
                    selectPeople.updateList(self.arrPeopleNames)
                }
            }else {
                
            }
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func onClickRun(sender: UIButton) {
        if strSiteName == "Select" {
            DELEGATE.showPopup(title: Empty_Copy_Site, type: .error)
            return
        }
        
        if strTime == "Select" {
            DELEGATE.showPopup(title: Empty_Time, type: .error)
            return
        }
        
        if strRoleName == "Select" {
            DELEGATE.showPopup(title: Empty_Reoprt_Role, type: .error)
            return
        }
        
        if strPeopleName == "Select" {
            DELEGATE.showPopup(title: Empty_People, type: .error)
            return
        }
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: ViewReportVC = storyboard.instantiateViewController(withIdentifier: "ViewReportVC") as! ViewReportVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    // MARK: - LBZSpinner
    func spinnerOpen(_ spinner:LBZSpinner) {
        print("Open")
    }
    
    func spinnerClose(_ spinner:LBZSpinner) {
        print("Close")
    }
    
    func spinnerChoose(_ spinner:LBZSpinner, index:Int, value:String) {
        if (spinner == selectSite) {
            if index == 0 {
                strSiteID = ""
            }else {
                var dict: Dictionary<String,String> = self.arrSites[index - 1]
                strSiteID = dict["siteID"]! as String
            }
            
            strSiteName = value
            print("strSiteID : \(strSiteID) strSiteName : \(strSiteName)")
            
            if (strSiteID != "" && strRoleID != "") {
                self.getSitePeople()
            }
        }else if (spinner == selectTime) {
            strTime = value
            print("strTime : \(strTime)")
        }else if (spinner == selectRole) {
            if index == 0 {
                strRoleID = ""
            }else {
                var dict: Dictionary<String,String> = self.arrRole[index - 1]
                strRoleID = dict["roleID"]! as String
            }
            
            strRoleName = value
            print("strRoleID : \(strRoleID) strRoleName : \(strRoleName)")
            
            if (strSiteID != "" && strRoleID != "") {
                self.getSitePeople()
            }
        }else {
            if index == 0 {
                strPeopleID = ""
            }else {
                var dict: Dictionary<String,String> = self.arrPeoples[index - 1]
                strPeopleID = dict["userID"]! as String
            }
            
            strPeopleName = value
            print("strPeopleID : \(strPeopleID) strPeopleName : \(strPeopleName)")
        }
    }

    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

