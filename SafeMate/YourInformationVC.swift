//
//  YourInformationVC.swift
//

import UIKit

class YourInformationVC: UIViewController {
    
    // MARK: - Variables
    
    // MARK: - UI Controls
    @IBOutlet var btnViewHistory: UIButton!
    @IBOutlet var btnViewBilling: UIButton!
    @IBOutlet var viewProfile: UIView!
    @IBOutlet var scrollBilling: UIScrollView!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewProfile.layer.cornerRadius = viewProfile.frame.width / 2.0
        viewProfile.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        viewProfile.layer.borderWidth = 1.5
        
        btnViewHistory.layer.cornerRadius = DELEGATE.setCorner()
        btnViewHistory.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnViewHistory.layer.borderWidth = 1.0
        
        btnViewBilling.layer.cornerRadius = DELEGATE.setCorner()
        btnViewBilling.layer.borderColor = UIColor(red: 158.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        btnViewBilling.layer.borderWidth = 1.0
        
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            scrollBilling.contentSize = CGSize(width: self.view.frame.size.width, height: 1230.0)
        }else {
            scrollBilling.contentSize = CGSize(width: self.view.frame.size.width, height: 685.0)
        }
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickViewHistory(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: YourHistoryVC = storyboard.instantiateViewController(withIdentifier: "YourHistoryVC") as! YourHistoryVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func onClickViewBilling(sender: UIButton) {
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objVC: BillingVC = storyboard.instantiateViewController(withIdentifier: "BillingVC") as! BillingVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

