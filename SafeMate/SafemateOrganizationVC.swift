//
//  SafemateOrganizationVC.swift
//

import UIKit

class SafemateOrganizationVC: UIViewController {
    
    // MARK: - Variables
    var isTerms: Bool = false
    
    // MARK: - UI Controls
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnCheckbox: UIButton!
    @IBOutlet var viewProfile: UIView!
    @IBOutlet var btnCall: UIButton!
    @IBOutlet var lblSupervisorName: UILabel!
    @IBOutlet var lblWelcomeMsg: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblRang: UILabel!
    @IBOutlet var lblSiteHours: UILabel!
    
    // MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("DELEGATE.dictCheckIntoSite: \(DELEGATE.dictCheckIntoSite)")
        
        let dictSiteDetails = (DELEGATE.dictCheckIntoSite["siteDetail"] as! NSDictionary)
        lblSupervisorName.text = (dictSiteDetails["siteSupervisorName"] as! String)
        lblWelcomeMsg.text = (dictSiteDetails["welcomeMessage"] as! String)
        lblAddress.text = (dictSiteDetails["siteAddress"] as! String)
        lblRang.text = "\(dictSiteDetails["sitegeoFenceRange"] as! String) m"
        lblSiteHours.text = "\(dictSiteDetails["siteOpeningTime"] as! String) to \(dictSiteDetails["siteClosingTime"] as! String)"
        
        viewProfile.layer.cornerRadius = viewProfile.frame.size.width / 2.0
        viewProfile.layer.borderColor = UIColor.white.cgColor
        viewProfile.layer.borderWidth = 1.5
        
        btnCheckbox.tag = 0
        btnCheckbox.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        
        btnNext.layer.cornerRadius = DELEGATE.setCorner()
        btnNext.layer.borderColor = UIColor(red: 30.0/255.0, green: 188.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        btnNext.layer.borderWidth = 1.0
    }
    
    // MARK: - UI Methods
    
    // MARK: - IBAction Methods
    @IBAction func onClickBack(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCall(sender: UIButton) {
        let dictSiteDetails = (DELEGATE.dictCheckIntoSite["siteDetail"] as! NSDictionary)
        UIApplication.shared.openURL(NSURL(string: "tel://\(dictSiteDetails["siteContactNumber"] as! String)")! as URL)
    }
    
    @IBAction func onClickNext(sender: UIButton) {
        if isTerms == false {
            DELEGATE.showPopup(title: Empty_CheckBox, type: .error)
            return
        }
        
        let storyboard = UIStoryboard(name: DELEGATE.checkDevice(), bundle: Bundle.main)
        let objWelcomeVC: WelcomeVC = storyboard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        self.navigationController?.pushViewController(objWelcomeVC, animated: true)
    }
    
    @IBAction func onClickCheckbox(sender: UIButton) {
        if (sender.tag == 0) {
            sender.tag = 5
            sender.setBackgroundImage(UIImage(named: "checked_filled.png"), for: .normal)
            
            isTerms = true
        }else {
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
            
            isTerms = false
        }
    }
    
    // MARK: - Delegate Methods
    
    // MARK: - Memory Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

